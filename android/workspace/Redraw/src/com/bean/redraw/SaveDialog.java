package com.bean.redraw;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;

public class SaveDialog extends Dialog {

public interface OnFileNameSelectedListener {
    void nameSelected(String name);
}

private OnFileNameSelectedListener mListener;
private String initname;

private static class SaveView extends RelativeLayout {
	
    private OnFileNameSelectedListener mListener;
    private EditText text;
    private Button save;

    SaveView(Context c, OnFileNameSelectedListener l, String name) {
        super(c);
        mListener = l;
        
        
        
        
        	
        text = new EditText(getContext());
        text.setHint("Joey enter name!");
        
        if(name != null && name.length() > 0){
        	text.setText(name);
        }
        
        text.setLines(1);
        
        RelativeLayout.LayoutParams lay = new RelativeLayout.LayoutParams(300, LayoutParams.WRAP_CONTENT);
    	lay.setMargins(10, 10, 0, 0);
        
        text.setLayoutParams(lay);
        
        this.addView(text);
        
        save = new Button(getContext());
        save.setText("Save");
        
        lay = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
    	lay.setMargins(0, 10, 10, 0);
    	lay.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
    	
    	save.setLayoutParams(lay);    	
    	this.addView(save);
        
    	save.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View v) {
				
				if(text.getText().toString().length() > 0){
					mListener.nameSelected(text.getText().toString());
				}
				
			}
    	});
        
        
    }
    
  
    


}



public SaveDialog(Context context, OnFileNameSelectedListener listener, String name) {
    super(context);
    
    mListener = listener;
    initname = name;
}

@Override
protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    OnFileNameSelectedListener l = new OnFileNameSelectedListener() {
        public void nameSelected(String name) {
            mListener.nameSelected(name);
            dismiss();
        }
    };

    setContentView(new SaveView(getContext(), l, initname));
    setTitle("Enter name of file to save");
 }
}
