package com.bean.redraw;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.util.Collections;
import java.util.LinkedList;
import java.util.Timer;
import java.util.TimerTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.bean.redraw.ImportDialog.OnFileNameImportSelectedListener;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Path.FillType;
import android.graphics.PathMeasure;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.Display;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.view.WindowManager.LayoutParams;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.NumberPicker;
import android.widget.RelativeLayout;


public class RedrawActivity extends Activity implements NumberPicker.OnValueChangeListener, ColorPickerDialog.OnColorChangedListener, 
	SaveDialog.OnFileNameSelectedListener, OnFileNameImportSelectedListener {

	DrawingPanel drawView;
	private int WINDOWHEIGHT, WINDOWWIDTH;
	
	private Bitmap filterbitmap;
	private Paint filterpaint;
	private PorterDuffXfermode srcPorter;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// Set full screen view
//		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
//				WindowManager.LayoutParams.FLAG_FULLSCREEN);
//		requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		getActionBar().setDisplayShowTitleEnabled(false);
		getActionBar().setIcon(new ColorDrawable(getResources().getColor(android.R.color.transparent)));   

		Display display = getWindowManager().getDefaultDisplay();
		Point size = new Point();
		display.getSize(size);
		WINDOWWIDTH = size.x;
		WINDOWHEIGHT = size.y;

		RelativeLayout container = new RelativeLayout(this);

		drawView = new DrawingPanel(this);

		container.addView(drawView);
		drawView.setLayoutParams(new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
		setContentView(container);

		drawView.requestFocus();

//				    drawView.importPaths();
		
		filterpaint = new Paint();
		filterpaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.ADD));
		filterbitmap = BitmapFactory.decodeResource(getResources(), R.drawable.texture);
		filterbitmap.setWidth(this.getWidth());
		
		srcPorter = new PorterDuffXfermode(PorterDuff.Mode.SRC_ATOP);


	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
	    MenuInflater inflater = getMenuInflater();
	    inflater.inflate(R.menu.redraw, menu);
	    return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    // Handle item selection
	    switch (item.getItemId()) {
	        case R.id.strokecolor:
	        	showColorDialog();
	            return true;
	        case R.id.strokestylefill:
	        	drawView.setPaintStrokeStyle(0);
	            return true;
	        case R.id.strokestylefillandstroke:
	        	drawView.setPaintStrokeStyle(1);
	            return true;
	        case R.id.strokestylestroke:
	        	drawView.setPaintStrokeStyle(2);
	            return true;
	        case R.id.strokewidth:
	        	showNumberDialog();
	            return true;
	        case R.id.replay:
	        	drawView.replayDrawing();
	            return true;
	        case R.id.delete:
	        	drawView.clearDrawing();
	            return true;
	        case R.id.undo:
	        	drawView.undoPath();
	            return true;
	        case R.id.savefile:
	        	showSaveDialog();
//	        	drawView.savePaths("testpath");
	            return true;
	        case R.id.importfile:
//	        	drawView.clearDrawing();
//	        	drawView.importPaths("testpath");
	        	showImportDialog();
	            return true;
	            
	            
	        default:
	            return super.onOptionsItemSelected(item);
	    }
	}
	
	@Override
	public void colorChanged(int color) {
		
		drawView.setPaintColor(color);
		
	}
	
	public void showColorDialog(){
		
		new ColorPickerDialog(this, this, drawView.getPaintColor()).show();
		
	}
	
	@Override
	public void nameSelected(String name) {
		
		drawView.savePaths(name);
		
	}
	
	public void showSaveDialog(){
		
		new SaveDialog(this, this, drawView.getCurDrawName()).show();
		
	}
	
	@Override
	public void importNameSelected(String name) {
		
		drawView.clearDrawing();
		drawView.importPaths(name);
		
	}
	
	public void showImportDialog(){
		
		new ImportDialog(this, this).show();
		
	}
	
	
	
	public void showNumberDialog(){

	         final Dialog d = new Dialog(this);
	         d.setTitle("NumberPicker");
	         d.setContentView(R.layout.dialog);
	         Button b1 = (Button) d.findViewById(R.id.pallete1);
	         Button b2 = (Button) d.findViewById(R.id.button2);
	         final NumberPicker np = (NumberPicker) d.findViewById(R.id.numberPicker1);
	         np.setMaxValue(100);
	         np.setMinValue(1);
	         np.setValue((int)drawView.getPaintStrokeWidth());
	         np.setWrapSelectorWheel(false);
	         np.setOnValueChangedListener(this);
	         b1.setOnClickListener(new OnClickListener()
	         {
	          @Override
	          public void onClick(View v) {
	        	  drawView.setPaintStrokeWidth(np.getValue());
	              d.dismiss();
	           }    
	          });
	         b2.setOnClickListener(new OnClickListener()
	         {
	          @Override
	          public void onClick(View v) {
	              d.dismiss();
	           }    
	          });
	       d.show();
	}
	
	@Override
	public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
		
		
	}

	@Override
	public void onPause(){
//		drawView.savePaths("testpath");

		super.onPause();

	}


	public class DrawingPanel extends View implements OnTouchListener {
		private static final String TAG = "DrawView";

		private static final float MINP = 0.25f;
		private static final float MAXP = 0.75f;
		
		private static final int FILL_STYLE = 0;
		private static final int FILL_STROKE_STYLE = 1;
		private static final int STROKE_STYLE = 2;

//		private Canvas  mCanvas;
		private PathPlus    mPath;
		private Paint       mPaint;
		private Paint       defaultPaint;
		private LinkedList<PathPlus> paths = new LinkedList<PathPlus>();


		private final float lineSaveQual = 0.001f;


		public DrawingPanel(Context context) {
			super(context);
			setFocusable(true);
			setFocusableInTouchMode(true);

			this.setOnTouchListener(this);

			defaultPaint = new Paint();
			defaultPaint.setAntiAlias(true);
			defaultPaint.setDither(true);
			defaultPaint.setColor(Color.BLACK);
			defaultPaint.setStyle(Paint.Style.STROKE);
			defaultPaint.setStrokeJoin(Paint.Join.ROUND);
			defaultPaint.setStrokeCap(Paint.Cap.ROUND);
			defaultPaint.setStrokeWidth(6);
			
			mPaint = new Paint();
			mPaint.setAntiAlias(true);
			mPaint.setDither(true);
			mPaint.setColor(Color.BLACK);
			mPaint.setStyle(Paint.Style.STROKE);
			mPaint.setStrokeJoin(Paint.Join.ROUND);
			mPaint.setStrokeCap(Paint.Cap.ROUND);
			mPaint.setStrokeWidth(6);
			
//			mCanvas = new Canvas();
			mPath = new PathPlus(mPaint);
			paths.add(mPath);

		}
		
		public String getCurDrawName(){
			return curDrawName;
		}
		
		public void clearDrawing(){
			
			if(!redrawing){
				
				curDrawName = null;
				
				paths.clear();
				
				mPath = new PathPlus(mPaint);
				paths.add(mPath);
			}
			
		}
		
		public void undoPath(){
			
			if(!redrawing && paths.size() > 1){
				paths.remove(paths.size()-2);
			}
			
		}

		public void importPaths(String name){

			File file = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/Redraw", name+".json");

			if(file.exists()){
				
				curDrawName = name;

				try {

					BufferedReader reader = new BufferedReader(new FileReader(file));
					StringBuilder sb = new StringBuilder();
					String line = null;
					while ((line = reader.readLine()) != null) {
						sb.append(line).append("\n");
					}

					reader.close();

					//					Log.d("REDRAW", sb.toString());

					JSONObject ob = new JSONObject(sb.toString());

					JSONArray arr = ob.getJSONArray("paths");
					
					int cwidth = ob.getInt("canvaswidth");
					int cheight = ob.getInt("canvasheight");
					
					float offsetX = 0, offsetY = 0;
					
					float scale = (float)this.getWidth() / (float)cwidth;
					
					if(scale * cheight > this.getHeight()){
						scale = (float)this.getHeight() / (float)cheight;
						
						offsetX =  Math.abs(this.getWidth() - this.getWidth()*scale)/(float)4;
					
					}else{
						offsetY = Math.abs(this.getHeight() - this.getHeight()*scale)/(float)4;
					}
					
					
					
					Log.e("REDRAW", "SCALE: " + scale);

					for(int i =0; i < arr.length(); i++){

						JSONObject path = arr.getJSONObject(i);
						
						JSONArray patharr = path.getJSONArray("points");
						
						JSONObject cp = path.getJSONObject("paint");
						
						Paint curPaint = new Paint();
						curPaint.setAntiAlias(true);
						curPaint.setDither(true);
						curPaint.setStrokeJoin(Paint.Join.ROUND);
						curPaint.setStrokeCap(Paint.Cap.ROUND);
						
						curPaint.setColor(cp.getInt("color"));
						curPaint.setStyle(cp.getInt("style") == FILL_STYLE ? Paint.Style.FILL : 
							(cp.getInt("style") == FILL_STROKE_STYLE ? Paint.Style.FILL_AND_STROKE : Paint.Style.STROKE));
						curPaint.setStrokeWidth(cp.getInt("strokewidth"));
						
						mPath.setPaint(curPaint);

						float oldX = 0, oldY = 0;

						for(int j = 0; j < patharr.length(); j++){

							JSONArray cord = patharr.getJSONArray(j);

							float x = cord.getLong(0)*scale + offsetX;
							float y = cord.getLong(1)*scale + offsetY;


							if(j == 0){
								mPath.reset();
								mPath.moveTo(x, y);
								oldX = x;
								oldY = y;

							}else if(j == patharr.length()-1){
								mPath.lineTo(oldX, oldY);
								// commit the path to our offscreen
//								mCanvas.drawPath(mPath, mPath.getPaint());
								// kill this so we don't double draw  

								mPath = new PathPlus(defaultPaint);
								paths.add(mPath);
							}else{

								float dx = Math.abs(x - oldX);
								float dy = Math.abs(y - oldY);
								if (dx >= TOUCH_TOLERANCE || dy >= TOUCH_TOLERANCE) {
									mPath.quadTo(oldX, oldY, (x + oldX)/2, (y + oldY)/2);
									oldX = x;
									oldY = y;
								}
							}

						}						

					}

				} catch (IOException e) {
					e.printStackTrace();
				} catch (JSONException e) {
					e.printStackTrace();
				}     

			}

		}
		
		private String curDrawName;

		public void savePaths(String filename){
			Log.d("REDRAW", "SAVE PATHS");
			
			curDrawName = filename;
			
			synchronized(paths){
				
				if(redrawing){
					if(timer != null){
						timer.cancel();
						timer.purge();
					}
					paths = pathsToRedrawDraw;
				}

				JSONObject ob = new JSONObject();
				JSONArray patharr = new JSONArray();
				
				
				
				DecimalFormat df = new DecimalFormat("#.###");
	
				try {
					
					ob.put("canvaswidth", this.getWidth());
					ob.put("canvasheight", this.getHeight());
					
					ob.put("paths", patharr);
	
					for(PathPlus p: paths){
						
						PathMeasure pm = new PathMeasure(p, false);
						
						if(pm.getLength() > 0){
	
							JSONObject path = new JSONObject();
							JSONArray points = new JSONArray();
							JSONObject paint = new JSONObject();
							
							path.put("points", points);
							path.put("paint", paint);
							
							paint.put("color", p.getPaint().getColor());
							paint.put("style", p.getPaint().getStyle() == Paint.Style.FILL ? FILL_STYLE : 
								(p.getPaint().getStyle() == Paint.Style.FILL_AND_STROKE ? FILL_STROKE_STYLE : STROKE_STYLE));
							paint.put("strokewidth", p.getPaint().getStrokeWidth());
		
		
							patharr.put(path);
		
							
		
							float cords[] = {0f, 0f};
							
							float lineQual = 1.0f / pm.getLength();
							
							for(float f = 0.0f; f <= 1.0f; f+= lineQual){
								boolean worked = pm.getPosTan(pm.getLength() * f, cords, null);
		
								if(worked){
		
									JSONArray point = new JSONArray();
									point.put(df.format(cords[0]));
									point.put(df.format(cords[1]));
		
									points.put(point);
		
								}
		
							}
						}
	
					}
	
				} catch (JSONException e1) {
					e1.printStackTrace();
				}
	
				File dir = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/Redraw");
				dir.mkdir();
				File file = new File(dir, filename+".json");
	
				try {
					FileOutputStream f = new FileOutputStream(file);
					PrintWriter pw = new PrintWriter(f);
					pw.print(ob.toString());
					pw.flush();
					pw.close();
					f.close();
				} catch (FileNotFoundException e) {
					e.printStackTrace();
					Log.i("CORE", "******* File not found. Did you" +
							" add a WRITE_EXTERNAL_STORAGE permission to the   manifest?");
				} catch (IOException e) {
					e.printStackTrace();
				}   
			}

		}
		
		private int redrawCount = 0;
		private int redrawPathCount = 0;
		private boolean redrawing = false;
		private PathPlus curDrawPath;
		private PathPlus lastDrawPath = null;
		private int redrawLength = 0;
		private int redrawNextStep = 0;
		private int redrawStep = 0;
		private PathMeasure curPathM;
		private Timer timer;
		
		private void redrawCurPath(PathPlus p){
			
			curDrawPath = p;
			
			if(timer != null){
				timer.cancel();
				timer.purge();
			}
			
			timer = new Timer();
			
//			final Timer t = new Timer();
			
			curPathM = new PathMeasure(curDrawPath, false);
			
			if(curPathM.getLength() == 0.0f){
				doneRedrawing();
			}else{
			
				redrawCount = 0;
				
				for(redrawStep = 0; redrawStep < curPathM.getLength(); redrawStep++){
					
					timer.schedule(new TimerTask(){
	
						@Override
						public void run() {
							
							synchronized(paths){
								
								
								if(lastDrawPath != null && redrawing)
									paths.remove(lastDrawPath);
								
							
							
								float per = (float)++redrawCount /(float) curPathM.getLength();
								
								per = per > 1.0f ? 1.0f : per;
								
								PathPlus seg = new PathPlus(curDrawPath.getPaint());
								seg.reset();
								boolean worked = curPathM.getSegment(0, curPathM.getLength()*per, seg, true);
								
								if(worked){
									lastDrawPath = seg;
									seg.rLineTo(0, 0);
									
									if(redrawing){
										paths.add(seg);
									}
								}
								
								if(per == 1.0f){
									
									lastDrawPath = null;
									redrawPathCount++;
									
									if(redrawing){
									
										if(redrawPathCount < pathsToRedrawDraw.size()){
											redrawCurPath(pathsToRedrawDraw.get(redrawPathCount));
										}else{
											doneRedrawing();
										}
									}
									

									
								}
								
								
							}
							
							
						}
						
					}, redrawStep);
					
				}
			}
			
			
		}
		
		public void setPaintStrokeWidth(float w){
			mPaint.setStrokeWidth(w);
		}
		public float getPaintStrokeWidth(){
			return mPaint.getStrokeWidth();
		}
		
		public int getPaintColor(){
			return mPaint.getColor();
		}
		
		public void setPaintColor(int color){
			mPaint.setColor(color);
		}
		
		public void setPaintStrokeStyle(int s){
			
			switch(s){
				case FILL_STYLE:
					mPaint.setStyle(Paint.Style.FILL);
					break;
				case FILL_STROKE_STYLE:
					mPaint.setStyle(Paint.Style.FILL_AND_STROKE);
					break;
				case STROKE_STYLE:
					mPaint.setStyle(Paint.Style.STROKE);
					break;
			}
		}
		
		
		
		private void doneRedrawing(){
			redrawing = false;
			
			mPath = new PathPlus(mPaint);
			paths.add(mPath);
		}
		
		private LinkedList<PathPlus> pathsToRedrawDraw; 

		public void replayDrawing(){
			
			if(!redrawing && paths.size() > 0){
				redrawing = true;
				
//				savePaths();
				
				LinkedList<PathPlus> newPaths = (LinkedList<PathPlus>) paths.clone();
				
				synchronized(paths){
					
					paths.clear();
					redrawPathCount = 0;
				
					pathsToRedrawDraw = newPaths;
					redrawCurPath(pathsToRedrawDraw.get(0));
				}
				
			}
				
						
//				File file = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/Redraw", "testpath.json");
//	
//				if(file.exists()){
//	
//					try {
//	
//						BufferedReader reader = new BufferedReader(new FileReader(file));
//						StringBuilder sb = new StringBuilder();
//						String line = null;
//						while ((line = reader.readLine()) != null) {
//							sb.append(line).append("\n");
//						}
//	
//						reader.close();
//	
//						//					Log.d("REDRAW", sb.toString());
//	
//						JSONObject ob = new JSONObject(sb.toString());
//	
//						JSONArray arr = ob.getJSONArray("paths");
//						
//						LinkedList<Path> newPaths = new LinkedList<Path>();
//	
//						for(int i =0; i < arr.length(); i++){
//	
//							JSONArray path = arr.getJSONArray(i);
//							
//							Path p = new Path();
//	
//	
//							float oldX = 0, oldY = 0;
//	
//							for(int j =0; j < path.length(); j++){
//	
//								JSONArray cord = path.getJSONArray(j);
//	
//								float x = cord.getLong(0);
//								float y = cord.getLong(1);
//	
//	
//								if(j == 0){
//									p.reset();
//									p.moveTo(x, y);
//									oldX = x;
//									oldY = y;
//	
//								}else if(j == path.length()-1){
//									p.lineTo(oldX, oldY);
//									// commit the path to our offscreen
//	//								mCanvas.drawPath(mPath, mPaint);
//									// kill this so we don't double draw  
//	
//	//								mPath = new Path();
//									newPaths.add(p);
//								}else{
//	
//									float dx = Math.abs(x - oldX);
//									float dy = Math.abs(y - oldY);
//									if (dx >= TOUCH_TOLERANCE || dy >= TOUCH_TOLERANCE) {
//										p.quadTo(oldX, oldY, (x + oldX)/2, (y + oldY)/2);
//										oldX = x;
//										oldY = y;
//									}
//								}
//	
//							}						
//	
//						}
//						
//						synchronized(paths){
//						
//							replayDrawing(newPaths);
//						}
//	
//					} catch (IOException e) {
//						e.printStackTrace();
//						redrawing = false;
//					} catch (JSONException e) {
//						e.printStackTrace();
//						redrawing = false;
//					}     
//	
//				}else{
//					redrawing = false;
//				}
//			}

		}

		@Override
		protected void onSizeChanged(int w, int h, int oldw, int oldh) {
			super.onSizeChanged(w, h, oldw, oldh);
		}

		@Override
		protected void onDraw(Canvas canvas) {            
			
//			mCanvas = canvas;
			
			synchronized(paths){
			
				
				LinkedList<PathPlus> drawPath = (LinkedList<PathPlus>) paths.clone();
				
				for(int i = drawPath.size()-1; i >= 0 ; i--){
					
					PathPlus pa = drawPath.get(i);
					
					if(pa.getPaint().getColor() == Color.BLACK){
						drawPath.remove(pa);
						drawPath.addLast(pa);
					}
				}
				
				for (PathPlus p : drawPath){
					Paint pat = p.getPaint();
					pat.setXfermode(srcPorter);
					canvas.drawPath(p, pat);
				}
				
				canvas.drawBitmap(filterbitmap, 0, 0, filterpaint);
				
				invalidate();
			}
		}

		private float mX, mY;
		private static final float TOUCH_TOLERANCE = 4;

		private void touch_start(float x, float y) {
			mPath.reset();
			mPath.moveTo(x, y);
			mX = x;
			mY = y;
			
			mPath.setPaint(mPaint);
		}
		private void touch_move(float x, float y) {
			float dx = Math.abs(x - mX);
			float dy = Math.abs(y - mY);
			if (dx >= TOUCH_TOLERANCE || dy >= TOUCH_TOLERANCE) {
				mPath.quadTo(mX, mY, (x + mX)/2, (y + mY)/2);
				mX = x;
				mY = y;
			}
		}
		private void touch_up() {
			mPath.lineTo(mX, mY);
			// commit the path to our offscreen
//			mCanvas.drawPath(mPath, mPath.getPaint());
			// kill this so we don't double draw
			
			PathMeasure pathm = new PathMeasure(mPath, false);
			if(pathm.getLength() != 0.0f){
				mPath = new PathPlus(mPaint);
				paths.add(mPath);
			}

			//
//			savePaths();
			//
		}



		@Override
		public boolean onTouch(View arg0, MotionEvent event) {

			if(!redrawing){
			
				float x = event.getX();
				float y = event.getY();
	
				switch (event.getAction()) {
				case MotionEvent.ACTION_DOWN:
					touch_start(x, y);
					invalidate();
					break;
				case MotionEvent.ACTION_MOVE:
					touch_move(x, y);
					invalidate();
					break;
				case MotionEvent.ACTION_UP:
					touch_up();
					invalidate();
					break;
				}
			}
			return true;
		} 
	}
	
	public class PathPlus extends Path {
		
		private Paint paint;
		
		public PathPlus(Paint p){
			paint = new Paint();
			paint.set(p);
		}
		
		public Paint getPaint(){
			return paint;
		}
		
		public void setPaint(Paint p){
			paint.set(p);
		}		
	}

	

}
