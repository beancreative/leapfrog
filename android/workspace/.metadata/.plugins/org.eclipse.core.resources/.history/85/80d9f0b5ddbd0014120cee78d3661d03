package com.bean.redraw;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorMatrix;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.SweepGradient;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;

public class ColorPickerDialog extends Dialog {

public interface OnColorChangedListener {
    void colorChanged(int color);
}

private OnColorChangedListener mListener;
private int mInitialColor;

private static class ColorPickerView extends RelativeLayout {
    private final int[] mColors;
    private OnColorChangedListener mListener;
    private View pickedColor;
    private SeekBar lightenBar;
    private int curColor;
    private float curLight = 1.0f;

    ColorPickerView(Context c, OnColorChangedListener l, int color) {
        super(c);
        mListener = l;
        mColors = new int[] {
            0xFFF44336, 0xFFE91E63, 0xFF9C27B0, 0xFF673AB7, 0xFF3F51B5,
            0xFF2196F3, 0xFFFF0000, 0xFF03A9F4, 0xFF00BCD4, 0xFF009688, 0xFF4CAF50, 0xFF8BC34A, 0xFFCDDC39, 0xFFFFEB3B,
            0xFFFFC107, 0xFFFF9800, 0xFFFF5722, 0xFF795548, 0xFF9E9E9E, 0xFF607D8B, 0xFF000000
        };
        
        curColor = color;
        
        int row = 0;
        int col = 0;
        
        
        for(int i = 0; i < mColors.length; i++){
        	
        	int co = mColors[i];
        	
        	View v = new View(getContext());
        	v.setBackgroundColor(co);
        	
        	
        	
        	RelativeLayout.LayoutParams lay = new RelativeLayout.LayoutParams(50, 50);
        	lay.setMargins(10+60*col, 10+60*row, 0, 0);
        	
        	v.setOnClickListener(new OnClickListener(){

				@Override
				public void onClick(View v) {
					
					curColor = ((ColorDrawable) v.getBackground()).getColor();
					lightenColor(curLight);
					
				}
        		
        	});
        	       	
        	v.setLayoutParams(lay);
        	
        	this.addView(v);
        	
        	if((i+1) % 5 == 0){
        		row++;
        		col = 0;
        	}else{
        		col++;
        	}
        	
        	
        }
        
        pickedColor = new View(getContext());
        pickedColor.setBackgroundColor(color);
        
    	RelativeLayout.LayoutParams lay = new RelativeLayout.LayoutParams(100, 100);
    	lay.setMargins(375, 50, 0, 0);
    	
    	pickedColor.setLayoutParams(lay);
    	
    	pickedColor.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				
				mListener.colorChanged(((ColorDrawable) v.getBackground()).getColor());
				
			}
    		
    	});
    	
    	this.addView(pickedColor);
        
        lightenBar = new SeekBar(getContext());
        lightenBar.setMax(100);
        lightenBar.setProgress(0);
        
        lightenBar.setOnSeekBarChangeListener(new OnSeekBarChangeListener(){

			@Override
			public void onProgressChanged(SeekBar seekBar, int progress,
					boolean fromUser) {
				curLight = 1.0f - (float)progress / (float)seekBar.getMax();
				lightenColor(curLight);
				
			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {}
			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {}
        });
        
        lay = new RelativeLayout.LayoutParams(200, LayoutParams.WRAP_CONTENT);
    	lay.setMargins(325, 175, 0, 0);
    	
    	lightenBar.setLayoutParams(lay);
        
        this.addView(lightenBar);
        
        
    }
    
    private void lightenColor(float per){
    	
    	Log.e("REDRAW", "PER: " + per);
    	
    	float[] hsv = new float[3];
    	int color = curColor;
    	Color.colorToHSV(color, hsv);
    	hsv[2] = per * ( 1.0f + hsv[2] );
    	color = Color.HSVToColor(hsv);
    	pickedColor.setBackgroundColor(color);
    	
    }
    
    


}



public ColorPickerDialog(Context context,
                         OnColorChangedListener listener,
                         int initialColor) {
    super(context);

    mListener = listener;
    mInitialColor = initialColor;
}

@Override
protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    OnColorChangedListener l = new OnColorChangedListener() {
        public void colorChanged(int color) {
            mListener.colorChanged(color);
            dismiss();
        }
    };

    setContentView(new ColorPickerView(getContext(), l, mInitialColor));
    setTitle("Pick a Color");
 }
}
