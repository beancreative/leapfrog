// ILFRewardCenterService.aidl
package com.leapfrog.rewardservice;

interface ILFRewardCenterService {
    /**
    * This logs the microModID as being unlocked for player with if profileID
    * @param profileId The profile ID for the current player
    * @param gameID the string representation of the game ID
    * @param microModID the 22 character micro mod id string
    */
    void unlock(in int profileId ,in String gameID, in String microModID);
    /**
    * Returns an array of microModID strings that are unlocked for the user
    * specified by profileID for the game specified by gameID
    * @param profileId Specify the profile ID of interest
    * @param gameID the string representation of the game ID
    * @return a Vector of micro mod IDs, as Strings, of the unlocked micro mods
    * for the given user, profileID, and game, gameID
    */
    List<String> getUnlockedMicroMods(in int profileID, in String gameID);
    /**
    * Returns whether a micro mod is unlocked
    * @param profileId Specify the profile ID of interest
    * @param gameID the string representation of the game ID
    * @param microModID the 22 character micro mod id string
    * @return true if the micro mod, specified by microModID, is unlocked for the
    * user specified by profileID for the game specified by gameID
    */
    boolean isMicroModUnlocked(in int profileID, in String gameID, in String microModID);
}
