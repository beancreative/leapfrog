/* version="2015.04.14.13.08"
 *****************************************************************************************
 * Descriptions:
 * This profile service interface is used to access or manage the profile records, profile
 * permissions, device information and parent settings via Profile Service. In order to
 * connect to Profile Service, applications need to invoke bindService() then they can use
 * the APIs described in this interface.
 * Additionally, those applications which want to make use of the Profile Service must add
 * the following permission in their AndroidManifest.xml.
 *  <uses-permission android:name=" com.leapfrog.permission.CONTENT_PROFILE" />
 *
 * Changes:
 *  2015-04-10 Joseph Chang
 *             1. Add the APIs of managing profile milestones information.
 *  2015-03-09 Joseph Chang
 *             1. Change from en_UK to en_GB for English (United Kingdom) in
 *                getSystemLocale() and setSystemLocale() so as to synchronize with the
 *                native Android locale string.
 *  2015-03-06 Joseph Chang
 *             1. Resize the profile picture proportionally if its width or height exceeds
 *                512-pixel.
 *             2. Move the stored picture in Picture path in external directory.
 *  2015-01-29 Joseph Chang
 *             1. Add the APIs to get/set profile picture as file.
 *  2015-01-14 Joseph Chang
 *             1. Add setActiveProfile() to set the currently active profile ID. 
 *             2. Add the APIs to get/set profile user ID and profile user child ID.
 *             3. Add syncProfileInfoByWebCall() to synchronize and update profile info. 
 *  2014-12-31 Joseph Chang
 *             1. Add the APIs to get/set device owner ID and device ID.
 *  2014-12-22 Joseph Chang
 *             1. Remove the API of Profile App management.
 *             2. Update createNewProfile() and deleteProfileRecord().
 *  2014-12-19 Joseph Chang
 *             Created.
 ****************************************************************************************/

package com.leapfrog.contentprofile;

import android.graphics.Bitmap;
import com.leapfrog.contentprofile.ProfileInfo;

interface IProfileService
{
    /**
     * To get a list of all profiles.
     * @return A list containing all profiles created on the device.
     */
    List<ProfileInfo> getAllProfiles();

    /**
     * To get the currently active profile ID.
	 * The getActiveProfile() will return the value set by the last setActiveProfile() call;
	 * and getActiveProfile() will default to return 0 (no active/login profile) after device bootup.
	 * Please refer to {@link setActiveProfile}.
     * @return The ID of currently active profile.
     *          -1: parent profile.
     *           0: no active profile (neither parent profile nor child's profile) is logged in.
     *          >0: a specific child profile; a smaller value indicate created earlier in time.
     */
    int getActiveProfile();

    /**
     * To set the currently active profile ID.
	 * The getActiveProfile() will return the value set by the last setActiveProfile() call;
	 * and getActiveProfile() will default to return 0 (no active/login profile) after device bootup.
	 * This setActiveProfile() must be called on the following occasions:
	 * 1. By OOBE
	 *    a. Launched
	 *       Set to Parent Profile by setActiveProfile(-1)
	 *    b. Finished: set to no-profile by setActiveProfile(0)
	 * 2. By Profile-Picker in kids mode launcher
	 *    a. Users logged out
	 *       Set to no-profile by setActiveProfile(0)
	 *    b. Profile auto logged out after idle for a certain interval
	 *       Set to no-profile by setActiveProfile(0)
	 *    c. User logs into parent profile
	 *       Set to Parent Profile by setActiveProfile(-1)
	 *    d. User logs into a certain child profile
	 *       Set currently logged in child profile ID by setActiveProfile(childProfileID).
     * @param profileId Specify the id of currently active profile.
     *          -1: parent profile.
     *           0: no active profile (neither parent profile nor child's profile) is logged in.
     *          >0: a specific child profile; a smaller value indicate created earlier in time.
     */
    void setActiveProfile(int profileId);

    /**
     * To synchronize all device profiles information onto LF server via a web service call.
     * Whenever the profiles' information is changed/updated/deleted/created, the changes must
     * be updated to LF web server via web service call. 
     * ProfileManagementUI is one of the applications that may make this API call on profile
     * creation/modification/deletion. Other applications, such as OOBE, which may 
     * modify/update the profile information, must invoke this API to synchronize the updated 
     * profile information with the web server.
     * Please refer to the "Update device and profile information" REST call on the 
     * NAR-SW-Specification.
     */
    void syncProfileInfoByWebCall();

    //////////////////////////////////////////////////////////////////////////////////////
    // Profile record management.

    /**
     * To create a new profile.
     * On success, it will broadcast an intent of action defined in ProfileInfo.IntentConst.ACTION_MODIFY_CHILD_PROFILE.
     * This broadcasted intent will carry 2 extra parameters (defined in ProfileInfo.IntentConst): 
     * 	    EXTRA_PROFILE_ID: an integer greater than 0 to specify the ID of the newly created profile.
     * 	    EXTRA_PROFILE_OPERATION: a string of EXTRA_PROFILE_OP_CREATED
	 * 	                to indicate the specified profile has been newly created.
     * Note, this API should only be called by Child Profile Settings UI.
     * @param profileInfo Specify the profile related information.
     *                    The profileInfo.mProfileId field will be ignored. You should set
     *                    profileInfo.mNickname to the unique profile name (usually the
     *                    first name) by profileInfo.setNickname(). Once created, the 
     *                    unique profile name can not be modified in any way, but the
     *                    nickname can be changed for this created profile.
     * @return > 0 The unique profile ID, and broadcast an intent of action defined
     *             in ProfileInfo.IntentConst.ACTION_MODIFY_CHILD_PROFILE to indicate which profile-ID
     *             has been created.
     *         < 0 Error code as defined in ProfileInfo.ProfileError class if an error occurs.
     *             e.g. profile name is non-unique or value(s) of profile element is invalid.
     *             Please check ProfileInfo.ProfileError class for possible error codes.
     */
    int createNewProfile(in ProfileInfo profileInfo);

    /**
     * To update an existing profile.
     * Note, this API should only be called by Child Profile Settings UI.
     * @param profileId Specify the profile ID concerns you.
     * @param profileInfo Specify the profile information to be updated.
     *                    The profileInfo.mProfileId field will be ignored.
     * @return = 0 if successful.
     *         < 0 Error code as defined in ProfileInfo.ProfileError class if an error occurrs.
     *             e.g. profile name is non-unique or value(s) of profile element is invalid.
     *             Please check ProfileInfo.ProfileError class for possible error codes.
     */
    int updateProfileRecord(int profileId, in ProfileInfo profileInfo);

    /**
     * To delete an existing profile record as well as related time-control settings.
     * On success, it will broadcast an intent of action defined in ProfileInfo.IntentConst.ACTION_MODIFY_CHILD_PROFILE.
     * This broadcasted intent will carry 2 extra parameters (defined in ProfileInfo.IntentConst): 
     * 	    EXTRA_PROFILE_ID: an integer greater than 0 to specify the ID of the just deleted profile.
     *  	EXTRA_PROFILE_OPERATION: a string of EXTRA_PROFILE_OP_DELETED 
     *                               to indicate the specified profile has been deleted.
     * Note, this API should only be called by Child Profile Settings UI.
     * @param profileId Specify the id of the profile to delete.
     * @return true if successful, and broadcast an intent of action defined
     *             in ProfileInfo.IntentConst.ACTION_MODIFY_CHILD_PROFILE to indicate which profile
     *             has been deleted.
     *         false if cannot delete a record from database or the profile ID is invalid.
     */
    boolean deleteProfileRecord(int profileId);

    /**
     * To get the unique profile name; usually it is just the first name.
     * This name is unique and assigned during profile creation. 
     * This name cannot be changed after profile creation. To change to a 
     * friendly name, please use the getProfileNickname() and setProfileNickname().
     * @param profileId Specify the profile ID concerns you.
     * @return The unique profile name.
     *         null if cannot find profile name for the specified id.
     */
    String getProfileName(int profileId);

    /**
     * To get child's nickname for the specified profile.
     * @param profileId Specify the profile ID concerns you.
     * @return The profile nickname if its value is not empty.
     *         The profile name if the nickname is empty.
     *         null if both nickname and profile name are missing or the specified
     *              profile ID is invalid.
     */
    String getProfileNickname(int profileId);

    /**
     * To set child's nickname for the specified profile.
     * @param profileId Specify the profile ID concerns you.
     * @param nickname Specify the profile nickname.
 	 * @return true if successful and an Intent of
	 *           ProfileInfo.IntentConst.ACTION_CHILD_PROFILE_CHANGED will be 
	 *           broadcasted with an integer extra parameter named
	 *           ProfileInfo.IntentContst.EXTRA_PROFILE_ID to specify which profile data 
	 *           has been updated. 
     *         false if the profile ID is invalid or nickname is non-unique.
     */
    boolean setProfileNickname(int profileId, String nickname);

    /**
     * To get child's grade level for the specified profile.
     * @param profileId Specify the profile ID concerns you.
     * @return Grade level. Valid range is defined in ProfileInfo.
     *                      ProfileInfo.MIN_GRADE=-2=0xFE and ProfileInfo.MAX_GRADE=6.
     *         (ProfileInfo.MAX_GRADE + 1) if this value is not available.
     */
    int getProfileGrade(int profileId);

    /**
     * To set child's grade level for the specified profile.
     * @param profileId Specify the profile ID concerns you.
     * @param grade Specify the profile grade level.
     *              Valid range is defined in ProfileInfo.
     *              ProfileInfo.MIN_GRADE=-2=0xFE and ProfileInfo.MAX_GRADE=6
     * @return true if successful.
     *         false if the profile ID or grade level is invalid.
     */
    boolean setProfileGrade(int profileId, int grade);

    /**
     * To get minimum grade level for the specified profile.
     * @param profileId Specify the profile ID concerns you.
     * @return The profile minimum grade level, which is a constant as defined in
     *         ProfileInfo.MIN_GRADE=-2=0xFE.
     */
    int getProfileMinGrade(int profileId);

    /**
     * To get maximum grade level for the specified profile.
     * @param profileId Specify the profile ID concerns you.
     * @return The profile maximum grade level, which is a constant as defined in
     *         ProfileInfo.MAX_GRADE=6.
     */
    int getProfileMaxGrade(int profileId);

    /**
     * To get child's gender for the specified profile.
     * @param profileId Specify the profile ID concerns you.
     * @return The profile gender as defined in ProfileInfo.MALE=0 and
     *                                          ProfileInfo.FEMALE=1.
     *         -1 if cannot find profile gender for the specified id.
     */
    int getProfileGender(int profileId);

    /**
     * To set child's gender for the specified profile.
     * @param profileId Specify the profile ID concerns you.
     * @param gender Specify the profile gender as defined in ProfileInfo.MALE=0 and
     *               ProfileInfo.FEMALE=1.
     * @return true if successful.
     *         false if the profile ID or gender is invalid.
     */
    boolean setProfileGender(int profileId, int gender);

    /**
     * To get child's birth date for the specified profile in the format of YYYY-MM-DD.
     * @param profileId Specify the profile ID concerns you.
     * @return The profile birth date in the format of YYYY-MM-DD.
     *         null if cannot find profile birth date for the specified id.
     */
    String getProfileBirthdate(int profileId);

    /**
     * To set child's birth date for the specified profile in the format of YYYY-MM-DD.
     * @param profileId Specify the profile ID concerns you.
     * @param birthdate Specify the profile birth date in the format of YYYY-MM-DD.
     * @return true if successful.
     *         false if the profile ID or birth date is invalid.
     */
    boolean setProfileBirthdate(int profileId, String birthdate);

    /**
     * To get the total number of child's badges for the specified profile.
     * @param profileId Specify the profile ID concerns you.
     * @return The total number of profile badges.
     *         -1 if cannot find the specified profile id.
     */
    int getProfileBadges(int profileId);

    /**
     * To add child's badges for the specified profile.
     * @param profileId Specify the profile ID concerns you.
     * @param badgeNum Specify the number of badges to add on.
     *                 A negative value (< 0) is treated as subtraction.
     * @return Number of badges after addition.
     *         -1 if cannot find the specified profile id.
     */
    int addProfileBadges(int profileId, int badgeNum);

    /**
     * To get the total number of child's tokens for the specified profile.
     * @param profileId Specify the profile ID concerns you.
     * @return The total number of profile tokens.
     *         -1 if cannot find the specified profile id.
     */
    int getProfileTokens(int profileId);

    /**
     * To add child's tokens for the specified profile.
     * @param profileId Specify the profile ID concerns you.
     * @param tokenNum Specify the number of tokens to add on.
     *                 A negative value (< 0) is treated as subtraction.
     * @return Number of tokens after addition.
     *         -1 if cannot find the specified profile id.
     */
    int addProfileTokens(int profileId, int tokenNum);

    /**
     * To get the picture bitmap of a specified profile.
	 * If the size of the picture set by setProfilePicture() or setProfilePictureByFile()
	 * exceeds 512-pixel in either width or height, the returned picture will be resized
	 * proportionally so as to shrink the longest side to be less than 512-pixel.
	 * For example, if the set picture is a FHD picture of size 1920x1080 (width by height),
	 * the returned picture will be resized to 512x288.
     * @param profileId Specify the profile ID concerns you.
     * @return The picture bitmap of the specified profile.
     *         null iff the specified profile id is invalid (not existed);
     *         in case the profile picture has not been set, a default bitmap
     *         (a dummy picture of boy or girl) will be returned instead.
     */
    Bitmap getProfilePicture(int profileId);

    /**
     * To get the picture file path of a specified profile located in Environment.DIRECTORY_PICTURES.
	 * The returned filename will be randomly generated under Environment.DIRECTORY_PICTURES.
     * This is a convenient API to getProfilePicture(int profileId).
	 * If the picture is set previously by setProfilePictureByFile(), then a file of
	 * the same content and file suffix (png, jpg, bmp or gif) will be returned. 
	 * However, if the picture is last set by setProfilePicture(), then a PNG file in
	 * Environment.DIRECTORY_PICTURES will be returned, and its content will be what is returned
	 * by getProfilePicture(int profileId), which its lognest dimension is 512-pixel.
     * @param profileId Specify the profile ID concerns you.
     * @return The file path to the specified profile picture stored as a file under Environment.DIRECTORY_PICTURES.
     *         null iff the specified profile id is invalid (not existed);
     *         in case the profile picture has not been set, a default file 
     *         (a dummy picture of boy or girl) will be returned instead.
     */
    String getProfilePictureAsFile(int profileId);

    /**
     * To set the picture bitmap of a specified profile.
     * @param profileId Specify the profile ID concerns you.
     * @param picture Specify the profile picture bitmap.
     *                Pass null to clear existing picture and revert to
     *                system default picture (a dummy picture of boy or girl).
     */
    void setProfilePicture(int profileId, in Bitmap picture);

    /**
     * To set the picture file path of a specified profile.
     * This is a convenient API to setProfilePicture(int profileId, Bitmap picture).
     * @param profileId Specify the profile ID concerns you.
     * @param filePath2Picture Specify the filepath to a picture;
     *                         Supported formats are jpg, png, bmp and gif.
     *                         Pass null to clear existing picture and revert to
     *                         system default picture (a dummy picture of boy or girl).
     */
    void setProfilePictureByFile(int profileId, String filePath2Picture);

    //====================================================================================
    // Profile play log of game.

    /**
     * To get the boolean value of whether profile's play log should be uploaded or not.
     * @param profileId Specify the profile ID concerns you.
     * @return true if the specified profile's play log should be uploaded.
     *         false otherwise.
     */
    boolean getLogUploadable(int profileId);

    /**
     * To set the boolean value of LogUploadable for the specified profile.
     * @param profileId Specify the profile ID concerns you.
     * @param logUploadable Specify the profile's LogUploadable value.
     */
    void setLogUploadable(int profileId, boolean logUploadable);

    /**
     * To get uploadable play data log path for the specified profile. 
	 * If the log data file is locked by a lockLogFile(profileId) without a paired 
	 * releaseLogFile(profileId), it returns null immediately (without blocking).
     * Note: This function should be only used by LogUploadService.
     * @param profileId Specify the profile ID concerns you.
     * @return The uploadable data log path of the specified profile.
     *         null if the data log is locked or the specified profile id is invalid.
     */
    String getUploadableLogPath(int profileId);

    /**
     * To get and lock play data log path for the specified profile.
     * A successful lockLogFile(profileId) must be paired with a releaseLogFile(profileId)
     * so as to release the locked log file for others (such as logUploaderService). 
     * This lockLogFile(profileId) will block indefinitely if it has been locked without
     * a paired releaseLogFile(profileId) yet.
     * The Brio library should invoke this function to lock and get log file path.
     * @param profileId Specify the profile ID concerns you.
     * @return The play data log path of the specified profile.
     *         null if the specified profile id is invalid.
     */
    String lockLogFile(int profileId);

    /**
     * To release play data log file acquired by a previous lockLogFile(profileId) call
     * for the specified profile. 
     * The Brio library should invoke this function to unlock the log file path.
     * @param profileId Specify the profile ID concerns you.
     */
    void releaseLogFile(int profileId);

    //====================================================================================
    // Profile user ID and user child ID.

    /**
     * To get the profile user ID.
     * Profile user ID is the "userId" field retrieved from the JSON response while
     * updating profile information by a web call to POST/GET /devices/{serial}. This
     * value will be updated after each web call to either POST or GET /devices/{serial},
     * and will be used as the profile CRM ID to Google Analytics Event Measurements
     * logging. It will return -1 if this value is not available.
     * @return the profile user ID.
     *         -1 if not available.
     */
    int getUserId(int profileId);

    /**
     * To set the profile user ID.
     * Profile user ID is the "userId" field retrieved from the JSON response while
     * updating profile information by a web call to POST/GET /devices/{serial}. This
     * value will be updated after each web call to either POST or GET /devices/{serial},
     * and will be used as the profile CRM ID to Google Analytics Event Measurements
     * logging.
     * @param profileId Specify the profile ID concerns you.
     * @param userId The profile user ID.
     */
    void setUserId(int profileId, int userId);

    /**
     * To get the profile user child ID.
     * Profile user child ID is the "userChildId" field retrieved from the JSON response
     * while updating profile information by a web call to POST/GET /devices/{serial}. 
     * This value will be updated after each web call to either POST/GET /devices/{serial},
     * and will be used as the child CRM ID to Google Analytics Event Measurements logging.
     * It will return -1 if this value is not available.
     * @return the profile user child ID.
     *        -1 if not available.
     */
    int getUserChildId(int profileId);

    /**
     * To set the profile user child ID.
     * Profile user child ID is the "userChildId" field retrieved from the JSON response
     * while updating profile information by a web call to POST/GET /devices/{serial}. 
     * This value will be updated after each web call to either POST/GET /devices/{serial}, 
     * and will be used as the child CRM ID to Google Analytics Event Measurements logging.
     * @param profileId Specify the profile ID concerns you.
     * @param userChildId The new profile user child ID.
     */
   void setUserChildId(int profileId, int userChildId);

    //////////////////////////////////////////////////////////////////////////////////////
    // Profile permissions.

    /**
     * To get the boolean value of a permission name for the specified profile.
     * @param profileId Specify the profile ID concerns you.
     * @param permissionName Specify the profile permission name (case-sensitive).
     *  Predefined profile permission names in ProfileInfo.ProfilePermission:
     *	  A. Permissions for App Management:
     *       1. AUTO_ADD_NEW_APPS: Whether new apps could be added automatically to the specified profile.
     *    B. Permissions for App Center:
     *	     1. REQUIRE_PASSWORD_FOR_APP_CENTER: whether browsing App Center requires a password for the specified profile.
     *	     2. ENABLE_WISH_LIST_BUTTON: whether wishlist buttons could be enabled for the specified profile or not.
     *	     3. SHOW_PRICE_AND_BUY_BUTTON: whether price and buy button could be shown for the specified profile or not.
     *       4. SHOW_SNEAK_PEAK_VIDEOS: whether sneak peek videos could be shown on home screen for the specified profile or not.
     *    C. Permissions for Demos and Previews:
     *	     1. SHOW_DEMO_GAMES: whether demo games could be shown on child home screen for the specified profile or not.
     *    D. Permissions for Wireless:
     *	     1. ALLOW_P2P_GAMING: whether peer-to-peer gaming is allowed for the specified profile or not.
     *   Additional Permissions: define and pass your own string
     * @return the permission value.
     *			1: enabled
     *			0: disabled
     *		   -1: permissionName not found/defined yet
     */
    int getProfilePermission(int profileId, String permissionName);

    /**
     * To set the boolean value of a permission name for the specified profile.
     * @param profileId Specify the profile ID concerns you.
     * @param permissionName Specify the profile permission name (case-sensitive).
     *  Predefined profile permission names in ProfileInfo.ProfilePermission:
	 *	  A. Permissions for App Management:
	 *       1. AUTO_ADD_NEW_APPS: Whether new apps could be added automatically to the specified profile.
     *    B. Permissions for App Center:
     *	     1. REQUIRE_PASSWORD_FOR_APP_CENTER: whether browsing App Center requires a password for the specified profile.
     *	     2. ENABLE_WISH_LIST_BUTTON: whether wishlist buttons could be enabled for the specified profile or not.
     *	     3. SHOW_PRICE_AND_BUY_BUTTON: whether price and buy button could be shown for the specified profile or not.
     *	     4. SHOW_SNEAK_PEAK_VIDEOS: whether sneak peek videos could be shown on home screen for the specified profile or not.
     *    C. Permissions for Demos and Previews:
     *	     1. SHOW_DEMO_GAMES: whether demo games could be shown on child home screen for the specified profile or not.
     *    D. Permissions for Wireless:
     *	     1. ALLOW_P2P_GAMING: whether peer-to-peer gaming is allowed for the specified profile or not.
     *   Additional Permissions: define and pass your own string
     * @param value The boolean value to set.
     */
    void setProfilePermission(int profileId, String permissionName, boolean value);

    //////////////////////////////////////////////////////////////////////////////////////
    // Profile Milestones.

    /**
     * To add a milestone to the list of milestones for the specified profile.
     * @param profileId Specify the profile ID concerns you.
     * @param milestone Specify the milestone to be added for the profile; 
     *                  it is case-sensitive and the string will be NOT trimmed before
     *                  saving, and a null or empty string (length=0) is invalid.
     * @return true if successful.
     *         false if the profile ID is invalid or milestone is null or empty.
     */
    boolean addMilestone(int profileId, String milestone);

    /**
     * To remove a milestone from the list of milestones for the specified profile.
     * @param profileId Specify the profile ID concerns you.
     * @param milestone Specify the milestone to be removed for the profile.
     *                  it is case-sensitive and the string is used as-is in comparison
     *                  (spaces are NOT trimmed), and a null or empty string (length=0)
     *                  is invalid.
     * @return true if successful even the specified milestone does not exist at all.
     *         false if the profile ID is invalid; or milestone is null or empty
     */
    boolean removeMilestone(int profileId, String milestone);

    /**
     * To clear all milestones for the specified profile.
     * @param profileId Specify the profile ID concerns you.
     * @return true if successful.
     *         false if the profile ID is invalid.
     */
    boolean clearAllMilestones(int profileId);

    /**
     * To get the list of milestones for the specified profile.
     * @param profileId Specify the profile ID concerns you.
     * @return A list containing all milestones created for the profile if successful.
     *         null if the profile ID is invalid;
     *         an empty list of size=0 if no milestones added for the specified profile yet.
     */
    List<String> getAllMilestones(int profileId);

    /**
     * To get the list of matched milestones for the specified profile.
     * @param profileId Specify the profile ID concerns you.
     * @param milestones Specify the list of milestones to match against those stored in database.
     *                   Pass a null list is invalid. 
     *                   All null or empty string in this list will be ignored.
     * @return A list containing matched milestones for the profile if successful.
     *         null if the profile ID is invalid, or milestones is null.
     *         an empty list of size=0 if no matched found.
     */
    List<String> getMatchedMilestones(int profileId, in List<String> milestones);

    //////////////////////////////////////////////////////////////////////////////////////
    // Device info and parent settings.

    /**
     * To get the x-caller-id for WebCall service, such as "34548252-b7a4-4c58-8f3f-d93a6d43ea30".
     * @return the caller ID for this device.
     */
    String getCallerId();

    /**
     * To return x-session-token got in DeviceRegistrationFlow to LFCAM by WebCall service,
     * such as "f45011f9-b26d-419d-9c02-97818eedc120".
     * @return the session token.
     */
    String getSessionToken();

    /**
     * To save/udpate the x-session-token got during DeviceRegistrationFlow to LFCAM by WebCall service for later use.
     * such as "f45011f9-b26d-419d-9c02-97818eedc120".
     * @param sessionToken The session token to set.
     */
    void setSessionToken(String sessionToken);

    /**
     * To get the device serial number in the format of 20 of hex values
     *     YMDD: 4 for 2014
     *     ProductID: 6-hex; a fixed value assigned by LF for all devices of the same product.
     *     PV: product variant: 2-hex; 0x64 (100) for all prototypes, and >0 for MP devices.
     *     CM ID: Manufacturer ID: 2-hex; a fixed value assigned by LF to differentiate ODM/OEM vendors
     *     Mfg Cycle: Manufacturin cycle is a 2-hex value
     *          0x00 for all devices before EP
     *          0x01 for all EP devices
     *          0x02 for all FEP devices
     *          >0x02 for MP devices
     *     Sequence number: 4-hex, a unique sequence number.
     * @return the device serial number.
     */
    String getDeviceSerialNumber();

    /**
     * To get the service code of this device,
     * such as "android1" to be used in Web REST call service request as devServiceCode="android1" in JSON,
     * or as UPCConnectedDeviceType=android1 while pointing to a LF WebView service     
     * @return the device service code
     */
    String getDeviceServiceCode();

    /**
     * To get the device Product Variant, PV, field from the getDeviceSerialNumber().
     * It is 100 (0x64) for all prototype devices, and starting from 1 for MP devices.
     * @return the device Product Variant, PV, field from the serial number
     */
    int getDeviceProductVariant();

    /**
     * To get the product SKU, such as, "00036-00002".
     * This value is required by Web REST call service request as mfgsku="00036-00002" in JSON,
     * @return the product SKU.
     */
    String getProductSku();

    /**
     * To get the parent email address.
     * @return the parent email address.
     */
    String getParentEmail();

    /**
     * To set the parent email address.
     * @param parentEmail The parent email address to set.
     */
    void setParentEmail(String parentEmail);

    /**
     * To get the parent PIN code.
     * @return the parent PIN code.
     */
    String getParentPin();

    /**
     * To set the parent PIN code.
     * @param parentPin The parent PIN code to set.
     */
    void setParentPin(String parentPin);

    /**
     * To get the system locale,
     * such as en_GB for English (United Kingdom),
     *         en_US for English (United States),
     *         en_AU for English (Australia),
     *         en_NZ for English (New Zealand),
     *         en_CA for English (Canada),
     *         en_IE for English (Ireland),
     *         en_OE for English (Rest of the world, others).
     * @return the system locale.
     */
    String getSystemLocale();

    /**
     * To set the system locale,
     * such as en_GB for English (United Kingdom),
     *         en_US for English (United States),
     *         en_AU for English (Australia),
     *         en_NZ for English (New Zealand),
     *         en_CA for English (Canada),
     *         en_IE for English (Ireland),
     *         en_OE for English (Rest of the world, others).
     * @param locale The system locale to set.
     */
    void setSystemLocale(String locale);

    /**
     * To get the device owner ID.
     * Device owner ID is the "devOwnerId" field provided in the JSON response while
     * updating profile information by a web call to either POST or GET /devices/{serial}.
     * This value will be updated after each web call to either POST or GET /devices/{serial},
     * and will be used to compare to the "parentID" field in the response to GET /SSO to 
     * validate the temporary LF.com password in the Update Device PIN/Password flow.
     * It will return -1 if this value is not available.
     * @return the device owner ID.
     *         -1 if not available.
     */
    int getDevOwnerId();

    /**
     * To set the device owner ID.
     * Device owner ID is the "devOwnerId" field provided in the JSON response while
     * updating profile information by a web call to either POST or GET /devices/{serial}. 
     * This value will be updated after each web call to either POST or GET /devices/{serial},
     * and will be used to compare to the "parentID" field in the response to GET /SSO to
     * validate the temporary LF.com password in the Update Device PIN/Password flow.
     * @param devOwnerId The device owner ID.
     */
    void setDevOwnerId(int devOwnerId);

    /**
     * To get the device ID.
     * Device ID is the "devId" field provided in the JSON response while updating profile
     * information by a web call to either POST or GET /devices/{serial}.
     * This value will be updated after each web call to either POST or GET /devices/{serial},
     * and will be provided as the "CRMid" as stipulated in the LF WebView Spec. 
     * It will return -1 if this value is not available.
     * @return the device ID.
     *         -1 if not available.
     */
    int getDevId();

    /**
     * To set the device ID.
     * Device ID is the "devId" field provided in the JSON response while updating profile
     * information by a web call to either POST or GET /devices/{serial}.
     * This value will be updated after each web call to either POST or GET /devices/{serial},
     * and will be provided as the "CRMid" as stipulated in the LF WebView Spec. 
     * @param devId The device ID.
     */
    void setDevId(int devId);
}

