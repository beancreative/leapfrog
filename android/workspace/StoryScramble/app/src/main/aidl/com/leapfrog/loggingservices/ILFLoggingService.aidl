// ILFLoggingService.aidl
package com.leapfrog.loggingservices;
import  com.leapfrog.loggingservices.model.LogData;

// Declare any non-default types here with import statements

interface ILFLoggingService {

    /**
     * This writes the event contained in logData to the
     * log file associated with the player references by
     * playerId
     * @param profileId The profile ID for the log file
     * @param logData the data structure holding the event
     * details to be logged
     */
     boolean append(in int profileId ,in LogData logData, in String token);

     /**
      * To get uploadable play data log path for the specified profile.
      * If the log data file is locked by a lockLogFile(profileId)
      * without a paired
      * releaseLogFile(profileId), it returns null immediately (without blocking).
      * Note: This function should be only used by LogUploadService.
      * @param profileId Specify the profile ID of interest
      * @return The uploadable data log path of the specified profile.
      * null if the data log is locked or the specified profile
      * id is invalid.
      */
      String getLogPath(in int profileId);

     /**
      * To get and lock play data log path for the specified profile.
      * A successful lockLogFile(profileId) must be paired with a
      * releaseLogFile(profileId)
      * so as to release the locked log file for others (such
      * as logUploaderService).
      * This lockLogFile(profileId) will block indefinitely if it has
      * been locked without
      * a paired releaseLogFile(profileId) yet.
      * @param profileId Specify the profile ID of interest
      * @return The play data log path of the specified profile.
      * null if the specified profile id is invalid.
      */
      String lockLogFile(in int profileId);

     /**
      * To release play data log file acquired by a previous
      * lockLogFile(token) call for the specified profile.
      * @param token Specify the token of interest
      */
       void releaseLogFile(in int profileId, in String token);



}
