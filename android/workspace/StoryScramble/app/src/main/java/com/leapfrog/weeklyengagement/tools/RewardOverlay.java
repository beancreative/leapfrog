package com.leapfrog.weeklyengagement.tools;

import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Color;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.os.IBinder;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.leapfrog.weeklyengagement.R;
import com.leapfrog.rewardservice.ILFRewardCenterService;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by jordanpridgen on 5/22/15.
 */
public class RewardOverlay extends RelativeLayout {

    private ILFRewardCenterService rewardsApi;
    private ServiceConnection serviceConnection;

    private ImageView _bg;
    private TextView _dialogText;
    private TextView _countdown;
    private ImageView _coin;
    private ImageView _panel;
    private MediaPlayer _mp;
    private AnimationSet anims;
    private int _coinsNum;
    private Timer _timer;
    private boolean _running = false;

    private Runnable _callback;

    private RelativeLayout self;

    private Typeface font;


    public RewardOverlay(Context context) {


        super(context);

        self = this;



        font =  Typeface.createFromAsset(getContext().getAssets(), "fonts/NeeNoo.ttf");
        _mp = new MediaPlayer();
        _timer = new Timer();

        _bg = new ImageView(getContext());
        _bg.setLayoutParams(new LayoutParams(1024,600));
        _bg.setBackgroundColor(Color.BLACK);
        _bg.setAlpha(0.4f);

        _bg.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                stopRunning();
            }
        });

        _panel = new ImageView(getContext());
        _panel.setImageResource(R.drawable.token_popup_ss);
        _panel.setX((1024 - 498) / 2);
        _panel.setY((550 - 370) / 2);

        _dialogText = new TextView(getContext());
        _dialogText.setText("You earned a reward!");
        _dialogText.setTypeface(font);
        _dialogText.setTextSize(40.0f);
        _dialogText.setWidth(600);
        _dialogText.setGravity(Gravity.CENTER_HORIZONTAL);
        _dialogText.setTextColor(Color.WHITE);
        _dialogText.setY(125);
        _dialogText.setX((1024 - 600) / 2);


        _countdown = new TextView(getContext());
        _countdown.setTypeface(font);
        _countdown.setTextColor(Color.WHITE);
        _countdown.setTextSize(70.0f);
        _countdown.setX(550.0f);
        _countdown.setY(325.0f);

        _coin = new ImageView(getContext());
        _coin.setImageResource(R.drawable.reward_token2);
        _coin.setX(450.0f);
        _coin.setY(325.0f);

        addView(_bg);
        addView(_panel);
        addView(_countdown);
        addView(_dialogText);
        addView(_coin);
        this.setAlpha(0.0f);
    }

    private void initConnection(){
        serviceConnection = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName name, IBinder service) {
                Log.e("REWARDS SERVICE", "connected");
                rewardsApi = ILFRewardCenterService.Stub.asInterface(service);


                try {

                    Log.e("REWARDS SERVICE","looks like it worked?");

                } catch(Exception e){
                    Log.e("ERROR",e.getMessage());
                }
            }

            @Override
            public void onServiceDisconnected(ComponentName name) {
                serviceConnection = null;
            }
        };
        Intent intent = new Intent();
        intent.setAction("com.leapfrog.rewardservice.service.LFRewardCenterService");

        getContext().bindService(intent, serviceConnection, Service.BIND_AUTO_CREATE);
    }

    public void connectService(){

        initConnection();
    }

    public void setCallback(Runnable callback){
        _callback = callback;
    }

    public void destroyService(){
        getContext().unbindService(serviceConnection);
    }

    public void earnCoins(int total){
        _running = true;
        //rewardsApi.
        InstanceChecker.getInstance().addTokens(total);
        _coinsNum = total;
        _countdown.setText(String.valueOf(_coinsNum));
        runAnimation();

    }

    public void cancel(){
        if (_running)
            stopRunning();
    }

    private void runAnimation(){



        anims = new AnimationSet(false);

        TranslateAnimation anim1 = new TranslateAnimation(0.0f,0.0f,0.0f,-100.0f);

        anim1.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                InstanceChecker.EFFECTS_PLAYER = MediaPlayer.create(getContext(),R.raw.token_earned_sfx);
                InstanceChecker.EFFECTS_PLAYER.start();
            }

            @Override
            public void onAnimationEnd(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {
                InstanceChecker.EFFECTS_PLAYER.release();
                InstanceChecker.EFFECTS_PLAYER = MediaPlayer.create(getContext(),R.raw.token_earned_sfx);
                InstanceChecker.EFFECTS_PLAYER.start();
            }
        });
        anim1.setRepeatMode(Animation.REVERSE);
        anim1.setRepeatCount(Animation.INFINITE);
        anim1.setInterpolator(new DecelerateInterpolator(1.0f));
        anim1.setDuration(300);

        AlphaAnimation fade = new AlphaAnimation(0.0f,1.0f);
        fade.setDuration(500);
        fade.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                _bg.setClickable(true);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                self.setAlpha(1.0f);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        self.startAnimation(fade);



        anims.addAnimation(anim1);

        _coin.startAnimation(anims);

        _mp = MediaPlayer.create(getContext(),R.raw.token_earned_audio);
        _mp.start();

        _timer = new Timer();

        for (int i = 0; i < _coinsNum;i++){
            _timer.schedule(new TimerTask() {
                @Override
                public void run() {

                    _coinsNum--;

                    if (_coinsNum >= 0)
                        self.post(new Runnable() {
                            @Override
                            public void run() {
                                adjustCountdown();
                            }
                        });


                }
            },1250+250*i);
        }

        _timer.schedule(new TimerTask() {
            @Override
            public void run() {
                self.post(new Runnable() {
                    @Override
                    public void run() {
                        stopRunning();
                    }
                });

            }
        },1250+250*_coinsNum+250);

    }

    private void adjustCountdown(){
        _countdown.setText(String.valueOf(_coinsNum));
    }

    private void stopRunning(){
        _running = false;
        _bg.setClickable(false);
        _coin.clearAnimation();
        if (_mp.isPlaying()){
            _mp.stop();
            _mp.release();
        }

        _timer.cancel();
        _timer.purge();

        AlphaAnimation anim = new AlphaAnimation(1.0f,0.0f);
        anim.setDuration(1000);
        anim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                if (_callback != null)
                    _callback.run();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        self.startAnimation(anim);


    }
}