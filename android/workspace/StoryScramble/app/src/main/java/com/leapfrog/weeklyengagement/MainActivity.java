package com.leapfrog.weeklyengagement;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Calendar;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONArray;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Service;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Typeface;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.media.Image;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.RemoteException;
import android.os.PowerManager;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.GridLayout;
import android.widget.ImageView;

import android.media.MediaPlayer;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.leapfrog.weeklyengagement.tools.InstanceChecker;
import com.leapfrog.weeklyengagement.tools.RewardOverlay;
import com.leapfrog.loggingservices.ILFLoggingService;
import com.leapfrog.loggingservices.model.TitleStart;
import com.leapfrog.loggingservices.model.TitleExit;
import com.leapfrog.loggingservices.model.ActivityStart;
import com.leapfrog.loggingservices.model.ActivityExit;

public class MainActivity extends Activity {

    private ILFLoggingService serviceApi;
    private ServiceConnection serviceConnection;

    public static final String PREFS_NAME = "UserProfile";
    public static final String INSTANCE = "GameInstance";
    public static final String SUB_INSTANCE = "GameSubInstance";
    public static final String TIMESTAMP = "GameTimestamp";

    private RewardOverlay _rewarder;

    Typeface neenoo;
    Typeface fontBlack;
    Typeface fontBlue;
    Typeface fontPink;
    Typeface fontPurple;
    Typeface fontWhite;
    Typeface fontRegular;

    int phraseIterator = 0;
    int audioIterator = 0;
    int blankIterator = 0;

    int totalSounds = 0;

    boolean gameStopped = false;

    boolean readyToRestart = false;

    boolean okToPlayStory = false;

    boolean inRecMode = false;

    boolean allowHint = true;

    boolean wasAboutToDoPlaySoundPair = false;

    int numBlanks = 0;
    ArrayList<String> blankPrompt = new ArrayList<String>();

    boolean homeConfirmOpen = false;
    boolean deleteAllConfirmOpen = false;
    boolean deleteOneConfirmOpen = false;

    boolean playAllPlaying = false;
    boolean playAllEmpty = true;
    private ImageView playAllButton;

    boolean delayRecord = false;

    boolean madpromptshouldfire = true;

    boolean autoPlayingButtonIntro = false;

    MediaRecorder soundRecorder;
    int recordIterator = 0;
    boolean currentlyRecording = false;
    boolean currentlyPlaying = false;
    boolean playbackPaused = false;
    boolean pauseDebounce = false;
    boolean replayDebounce = false;
    String fileNameTemplate = Environment.getExternalStorageDirectory().getAbsolutePath() + "/StoryScrambleAudio/";
    String recordedFileType = ".m4a";
    String storyTitleSound = null;
    boolean settingUpStoryPlayback = false;

    boolean hintOpen = false;

    boolean titleStartLogged = false;
    boolean readyToLog = false;

    MediaPlayer soundPlayer = null;

    MediaPlayer soundHelper1 = null;
    MediaPlayer soundHelper2 = null;
    MediaPlayer soundHelper3 = null;
    MediaPlayer musicPlayer = null;

    String hintTypeSource = "";
    String hintSfxSource = "";
    String hintExplanationSource = "";
    String hint1Source = "";
    String hint2Source = "";
    String hint3Source = "";

    int storyIndex = 0;
    int scoreForActivity = 0;
    boolean activityStarted = false;
    private boolean _allRecorded = true;
    private ArrayList<String> _tempRecordingList = new ArrayList<String>();

    String[] storyManifest;

    private GridLayout audioGrid;

    private RelativeLayout rewardsLayout;

    int audioCounter = 0;
    Handler managerHandler;
    ArrayList<int[]> audioManagerToDelete = new ArrayList<int[]>();
    Runnable managerRunnable = new Runnable(){
        public void run(){
            Log.e("autoPlay", audioCounter+" "+audioGrid.getChildCount());
            if (!((PowerManager) getSystemService(Context.POWER_SERVICE)).isScreenOn()) {
                audioCounter = 0;
                playAllPlaying = false;
                getWindow().getDecorView().setKeepScreenOn(false);
                Log.e("setKeepScreenOn",">>>>> false 1");
                playAllButton.setImageResource(R.drawable.play_all);
            } else {
                if(audioCounter < audioGrid.getChildCount()) {
                    RelativeLayout testLayout = (RelativeLayout) audioGrid.getChildAt(audioCounter);
                    ImageView button = (ImageView) testLayout.getChildAt(0);
                    if (button.getDrawable().getConstantState() == getResources().getDrawable( R.drawable.audio_button).getConstantState()) {
                        testLayout.getChildAt(0).performClick();
                        audioCounter++;
                        managerHandler.postDelayed(managerRunnable,5100);
                    } else {
                        audioCounter++;
                        managerHandler.postDelayed(managerRunnable,100);
                    }
                } else {
                    if(findViewById(R.id.nextButton).getVisibility()==View.VISIBLE) {
                        ImageView nextButton = (ImageView) findViewById(R.id.nextButton);
                        nextButton.performClick();
                        audioCounter = 0;
                        playAllPlaying = true;
                        getWindow().getDecorView().setKeepScreenOn(true);
                        Log.e("setKeepScreenOn",">>>>> true 1");
                        playAllButton.setImageResource(R.drawable.play_all_pressed);
                        managerHandler.postDelayed(managerRunnable,1000);
                    } else {
                        Log.e("audioQuit", "manager should quit");
                        audioCounter = 0;
                        playAllPlaying = false;
                        getWindow().getDecorView().setKeepScreenOn(false);
                        Log.e("setKeepScreenOn",">>>>> false 2");
                        playAllButton.setImageResource(R.drawable.play_all);
                    }
                }
            }
        }
    };

    JSONObject storyObj;
    JSONObject blankObj;
    JSONArray storyArray;

    String lastRecordedSound = "";
    ArrayList<String> newAudioFiles = new ArrayList<String>();
    ArrayList<ArrayList<String>> newAudioFilesSorted = new ArrayList<ArrayList<String>>();
    ArrayList<String> repeatedAudio = new ArrayList<String>();
    ArrayList<Boolean> isRepeatedAudioStock = new ArrayList<Boolean>();

    int currentPage = 0;
    int iconsPerPage = 15;
    JSONObject audioManagerObject;
    boolean invisibleActive = false;
    int audioManagerIterator = 0;
    boolean normalMode = false;
    boolean superMode = false;
    boolean madMode = false;
    int madPlayers =0;
    boolean madOverlayOpen = false;
    boolean menuButtonsClickable=true;

    //Logging
    private void initConnection() {
        serviceConnection = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName name, IBinder service) {
                serviceApi = ILFLoggingService.Stub.asInterface(service);
            }

            @Override
            public void onServiceDisconnected(ComponentName name) {
                serviceConnection = null;
            }
        };
        Intent intent = new Intent();
        intent.setAction("com.leapfrog.loggingservices.services.LoggerService");
        bindService(intent, serviceConnection, Service.BIND_AUTO_CREATE);
    }

    private void logTitleStart() {
        if (!titleStartLogged && readyToLog) {
            int productID = 3604497;
            String partNumber = "000-00000";
            String verNumber = "1.0.x.x";
            try {
                PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
                verNumber = pInfo.versionName;
            } catch (PackageManager.NameNotFoundException e){
                e.printStackTrace();
            }
            Byte logByte = new Byte("1");
            Log.e("LOG", "Logging this: TitleStart(" + productID + "," + partNumber + "," + verNumber + "," + logByte + ")");
            TitleStart titleStart = new TitleStart(productID, partNumber, verNumber, logByte);
            String token = null;
            InstanceChecker.getInstance().updateInstance();
            int userId = InstanceChecker.getInstance().getCurrentID();
            try {
                token = serviceApi.lockLogFile(userId);
                serviceApi.append(userId, titleStart, token);
                serviceApi.releaseLogFile(userId, token);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
            titleStartLogged = true;
        }
    }

    private void logTitleExit() {
        if (titleStartLogged && readyToLog) {
            Log.e("LOG", "Logging this: TitleExit()");
            TitleExit titleExit = new TitleExit();
            String token = null;
            InstanceChecker.getInstance().updateInstance();
            int userId = InstanceChecker.getInstance().getCurrentID();
            try {
                token = serviceApi.lockLogFile(userId);
                serviceApi.append(userId, titleExit, token);
                serviceApi.releaseLogFile(userId, token);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
            titleStartLogged = false;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        this.requestWindowFeature(Window.FEATURE_NO_TITLE);

        kickoff();

    }

    private void kickoff(){
        setContentView(R.layout.activity_main);

        Log.e("package", getPackageName());

        File directory = new File(Environment.getExternalStorageDirectory()+File.separator+"StoryScrambleAudio");
        directory.mkdirs();

        /////////////////////////////////////////////////////////////
        // make .nomedia file if it doesn't exist
        File output = new File(fileNameTemplate,".nomedia");
        boolean fileCreated = false;
        try {
            fileCreated = output.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Log.e("MAKENOMEDIAFILE", ".nomedia file made: " + fileCreated);
        /////////////////////////////////////////////////////////////

        InstanceChecker.create(this);
        //InstanceChecker.getInstance().getWeekNumber();

        initConnection();

        _rewarder = new RewardOverlay(this);
        _rewarder.connectService();

        //debug
        //resetAudioList();

        for(int i=0; i<12; i++) {
            newAudioFilesSorted.add(new ArrayList<String>());
        }
        populateStoryManifest();

        setContentView(R.layout.activity_main);

        playMusic("mus_splashscreen");

        ImageView intro_letters = (ImageView) findViewById(R.id.introLetters);
        intro_letters.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.letter_spin));

        final Handler introDelay = new Handler();
        introDelay.postDelayed(new Runnable() {
            @Override
            public void run() {
                playTwoSounds("intro_title", "intro_title2");
                readyToLog = true;
                logTitleStart();
            }
        }, 700);

        neenoo = Typeface.createFromAsset(getAssets(), "fonts/NeeNoo.ttf");
        fontBlack = Typeface.createFromAsset(getAssets(), "fonts/NeeNooStoryScramble-Black.ttf");
        fontBlue = Typeface.createFromAsset(getAssets(), "fonts/NeeNooStoryScramble-Blue.ttf");
        fontPink = Typeface.createFromAsset(getAssets(), "fonts/NeeNooStoryScramble-Pink.ttf");
        fontPurple = Typeface.createFromAsset(getAssets(), "fonts/NeeNooStoryScramble-Purple.ttf");
        fontWhite = Typeface.createFromAsset(getAssets(), "fonts/NeeNooStoryScramble-White.ttf");
        fontRegular = Typeface.createFromAsset(getAssets(), "fonts/NeeNooStoryScramble-Regular.ttf");
        TextView introText = (TextView) findViewById(R.id.introText);
        introText.setTypeface(neenoo);

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Log.e("START", "Starting main menu...");
                openMainMenu();
            }
        }, 7000);
    }

    private void kickOffAddendum(){
        if (activityStarted)
            logActivityExit(scoreForActivity);
        if (soundPlayer != null) {
            soundPlayer.release();
            soundPlayer = null;
        }
        homeConfirmOpen = false;

        try {
            // do something potentially risky
            inRecMode = false;
            okToPlayStory = false;
            readyToRestart = false;
            openMainMenu();
        } catch (Exception e) {
            Log.e("error: ", e.toString());
        }
        if (!_allRecorded){
            Log.e("DELETING","whole run of files not finished, deleting "+_tempRecordingList.toString());
            for (int i = 0; i < _tempRecordingList.size();i++){
                deleteTrack(_tempRecordingList.get(i));
            }
            _tempRecordingList.clear();
        }
    }

    @Override
    public void onPause() {
        super.onPause();

        logTitleExit();

        Log.e("SLEEPWAKE", "onPause fired");
        pauseEverything();
    }

    public void pauseEverything() {
        _rewarder.cancel();

        try{
            if (soundPlayer == null)
                soundPlayer = new MediaPlayer();

            if (soundPlayer != null)
                soundPlayer.isPlaying();
        }catch (IllegalStateException e){
            soundPlayer = new MediaPlayer();
        }


        if(musicPlayer != null) {
            if(musicPlayer.isPlaying()) {
                musicPlayer.pause();
            }
        }
        if(soundPlayer != null) {
            if (soundPlayer.isPlaying())
                soundPlayer.pause();

            final Handler pauseIncrement1 = new Handler();
            pauseIncrement1.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if(soundPlayer != null) {
                        if (soundPlayer.isPlaying())
                            soundPlayer.pause();
                    }
                }
            }, 100);

            final Handler pauseIncrement2 = new Handler();
            pauseIncrement2.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if(soundPlayer != null) {
                        if (soundPlayer.isPlaying())
                            soundPlayer.pause();
                    }
                }
            }, 200);

            final Handler pauseIncrement3 = new Handler();
            pauseIncrement3.postDelayed(new

                Runnable() {
                    @Override
                    public void run () {
                        if(soundPlayer != null) {
                            if (soundPlayer.isPlaying())
                                soundPlayer.pause();
                        }
                    }
            }, 400);

        }
        if(soundHelper1 != null) {
            if(soundHelper1.isPlaying()) {
                soundHelper1.pause();
            }
        }
        if(soundHelper2 != null) {
            if(soundHelper2.isPlaying()) {
                soundHelper2.pause();
            }
        }

        if (soundRecorder != null) {
            if(currentlyRecording) {
                try{
                    soundRecorder.stop();
                    soundRecorder.release();
                    soundRecorder = null;
                } catch (RuntimeException e) {
                    e.printStackTrace();
                    soundRecorder.reset();
                    soundRecorder.release();
                    soundRecorder = null;
                }

                autoPlayingButtonIntro = false;

                final ImageView recordAnimationHolder = (ImageView) findViewById(R.id.pulseAnimation);
                final AnimationDrawable recordAnimation = (AnimationDrawable) recordAnimationHolder.getDrawable();
                final TextView nowRecordingText = (TextView) findViewById(R.id.nowRecordingText);
                final ImageView progressBackground = (ImageView) findViewById(R.id.progressBackground);
                final ImageView progressAnimationHolder = (ImageView) findViewById(R.id.progressBar);
                final Animation progressAnimation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.progress);

                progressAnimationHolder.clearAnimation();
                progressAnimation.cancel();

                nowRecordingText.setVisibility(View.GONE);
                progressAnimationHolder.setVisibility(View.INVISIBLE);
                progressBackground.setVisibility(View.INVISIBLE);
                recordAnimation.stop();
                recordAnimationHolder.setVisibility(View.INVISIBLE);
                currentlyRecording = false;
            }
        }
        gameStopped = true;
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus)
    {
        super.onWindowFocusChanged(hasFocus);
        Log.e("WINDOW","has focus");

        if (hasFocus == true)
        {

                resumeEverything();

        }

        if (hasFocus == false)
        {
            pauseEverything();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.e("SLEEPWAKE", "onStop fired");
    }

    @Override
    protected void onResume(){
        super.onResume();

        logTitleStart();

        Log.e("SLEEPWAKE", "onResume fired");

        resumeEverything();

    }

    public void resumeEverything() {

        if (!_noResume) {
            Log.e("SLEEPWAKE", "HELLO");

            if (musicPlayer != null) {
                musicPlayer.start();
            }
            if (soundPlayer != null) {
                if (!playbackPaused && !homeConfirmOpen) {
                    soundPlayer.start();
                }
            }
            if (soundHelper1 != null) {
                soundHelper1.start();
            }
            //if(soundHelper2 != null) {
            //   soundHelper2.start();
            //}
            if (wasAboutToDoPlaySoundPair && !playbackPaused && !homeConfirmOpen) {
                Log.e("SLEEPWAKE", "it should call playSoundPair again");
                playSoundPair();
            }
            gameStopped = false;

            // added to fix issue where audio won't restart if interrupted in intro
            if (settingUpStoryPlayback && !playbackPaused && !homeConfirmOpen) {
                playTwoSoundsToSetupPlayback("playback", storyTitleSound);
            }
        } else{

        }
    }

    private boolean _noResume = false;

    @Override
    protected void onStart() {
        super.onStart();

        if (InstanceChecker.isReady()&& InstanceChecker.getInstance().outOfSync()){

            InstanceChecker.create(this);
            InstanceChecker.getInstance().updateInstance();
            fakehome();
            kickOffAddendum();

        }

        getWindow().getDecorView().setKeepScreenOn(false);
        Log.e("setKeepScreenOn", ">>>>> false 0");

        Log.e("SLEEPWAKE", "onStart fired");
    }

    @Override
    protected void onDestroy() {
        unbindService(serviceConnection);
        super.onDestroy();

        _rewarder.destroyService();

    }

    @Override
    public void onBackPressed() {
        pauseEverything();
        if (activityStarted)
            logActivityExit(scoreForActivity);
        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Leaving Story Spinner")
                .setMessage("Are you sure you want to quit?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        resumeEverything();
                        finish();
                    }

                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        resumeEverything();
                    }
                })
                .show();
    }

    private void openMainMenu() {
        menuButtonsClickable=true;

        readyToRestart = false;

        resetTrackers();

        setContentView(R.layout.main_view);
        if (InstanceChecker.isReady()) {
            InstanceChecker.getInstance().updateInstance();
            storyIndex = InstanceChecker.getInstance().getWeekNumber();
        }

        getStoryJSON(storyManifest[storyIndex]);
        setStoryTitle();
        storyTitleSound = null;
        try {
            storyTitleSound = storyObj.getJSONObject("weeklyengagement").getString("vo_title");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        playMusic("game_mus_splashmenu");
        playTwoSounds("menu_week", storyTitleSound);

        TextView storyIntro = (TextView) findViewById(R.id.storyIntro);
        storyIntro.setTypeface(neenoo);
        TextView storyName = (TextView) findViewById(R.id.storyName);
        storyName.setTypeface(neenoo);

        ImageView storyScrambleButton = (ImageView) findViewById(R.id.storyButton);
        setButtonPressState(storyScrambleButton, R.drawable.story_spinner_button, R.drawable.story_spinner_button_pressed);
        storyScrambleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(menuButtonsClickable){
                    menuButtonsClickable=false;
                    Log.e("Game", "Start Story Scramble");
                    playTwoSounds("storyscramble_sfx", "menu_story");

                    final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            openStoryScramble();
                        }
                    }, 3000);
                }
            }
        });

        ImageView madScrambleButton = (ImageView) findViewById(R.id.madButton);
        setButtonPressState(madScrambleButton, R.drawable.super_spinner_button, R.drawable.super_spinner_button_pressed);
        madScrambleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(menuButtonsClickable) {
                    menuButtonsClickable=false;
                    Log.e("Game", "Start Mad Scramble");
                    playTwoSounds("storyscramble_sfx", "menu_team");

                    final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            openMadScramble();
                        }
                    }, 3000);

                }

            }
        });

        ImageView superScrambleButton = (ImageView) findViewById(R.id.superButton);
        setButtonPressState(superScrambleButton, R.drawable.surprise_spinner_button, R.drawable.surprise_spinner_button_pressed);
        superScrambleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(menuButtonsClickable){
                    menuButtonsClickable=false;
                    Log.e("Game", "Start Super Scramble");
                    playTwoSounds("storyscramble_sfx", "menu_super");

                    final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            openSuperScramble();
                        }
                    }, 3000);

                }

            }
        });

        ImageView audioManagerButton = (ImageView) findViewById(R.id.audioManagement);
        setButtonPressState(audioManagerButton, R.drawable.settings_button, R.drawable.settings_button_pressed);
        audioManagerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(menuButtonsClickable){
                    Log.e("Manager", "Open Audio Manager");
                    playTwoSounds("icon_settings_sfx", "icon_settings");
                    openAudioManager();
                }

            }
        });



        ImageView hintButton1 = (ImageView) findViewById(R.id.storyHint);
        ImageView hintButton2 = (ImageView) findViewById(R.id.madHint);
        ImageView hintButton3 = (ImageView) findViewById(R.id.superHint);
        setButtonPressState(hintButton1, R.drawable.help_button_assets, R.drawable.help_pressed);
        setButtonPressState(hintButton2, R.drawable.help_button_assets, R.drawable.help_pressed);
        setButtonPressState(hintButton3, R.drawable.help_button_assets, R.drawable.help_pressed);
        final RelativeLayout menuHint1 = (RelativeLayout) findViewById(R.id.menuHint1);
        final RelativeLayout menuHint2 = (RelativeLayout) findViewById(R.id.menuHint2);
        final RelativeLayout menuHint3 = (RelativeLayout) findViewById(R.id.menuHint3);
        menuHint1.setVisibility(View.GONE);
        menuHint2.setVisibility(View.GONE);
        menuHint3.setVisibility(View.GONE);

        hintButton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(menuButtonsClickable) {
                    if(menuHint1.getVisibility() == View.VISIBLE){
                        menuHint1.setVisibility(View.GONE);
                        stopSoundHelper();
                    } else {
                        menuHint1.setVisibility(View.VISIBLE);
                        playTwoSounds("hint_sfx", "team_intro1");
                    }
                    menuHint2.setVisibility(View.GONE);
                    menuHint3.setVisibility(View.GONE);
                }
            }
        });
        hintButton2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(menuButtonsClickable)  {
                    menuHint1.setVisibility(View.GONE);
                    if(menuHint2.getVisibility() == View.VISIBLE){
                        menuHint2.setVisibility(View.GONE);
                        stopSoundHelper();
                    } else {
                        menuHint2.setVisibility(View.VISIBLE);
                        playTwoSounds("hint_sfx", "team_intro2");
                    }
                    menuHint3.setVisibility(View.GONE);
                }
            }
        });
        hintButton3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(menuButtonsClickable){
                    menuHint1.setVisibility(View.GONE);
                    menuHint2.setVisibility(View.GONE);
                    if(menuHint3.getVisibility() == View.VISIBLE){
                        menuHint3.setVisibility(View.GONE);
                        stopSoundHelper();
                    } else {
                        menuHint3.setVisibility(View.VISIBLE);
                        playTwoSounds("hint_sfx", "scramble_intro");
                    }
                }
            }
        });

    }

    public void logActivityStart() {
        Log.e("SCREEN STAYING","on");
        scoreForActivity = 0;
        activityStarted = true;
        Log.e("LOG", "Logging this: ActivityStart('SS','','')");
        ActivityStart activityStart = new ActivityStart("SS", "", "");
        String token = null;
        int userId = InstanceChecker.getInstance().getCurrentID();
        try {
            token = serviceApi.lockLogFile(userId);
            serviceApi.append(userId, activityStart, token);
            serviceApi.releaseLogFile(userId, token);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    public void logActivityExit(int score) {
        getWindow().getDecorView().setKeepScreenOn(false);
        Log.e("setKeepScreenOn",">>>>> false 3");
        activityStarted = false;
        Log.e("LOG", "Logging this: ActivityExit(" + score + ")");
        ActivityExit activityExit = new ActivityExit(score);
        String token = null;
        int userId = InstanceChecker.getInstance().getCurrentID();
        try {
            token = serviceApi.lockLogFile(userId);
            serviceApi.append(userId, activityExit, token);
            serviceApi.releaseLogFile(userId, token);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }


    private void openStoryScramble(){
        logActivityStart();
        normalMode = true;
        openRecordPrompts();
    }

    public void openMadScramble() {
        logActivityStart();
        madMode = true;
        madPlayers = 0;
        inRecMode = true;
        setContentView(R.layout.mad_start);
        playSound("team_intro3");
        initializeHomeButton();
        ImageView playersTwo = (ImageView) findViewById(R.id.playersTwo);
        ImageView playersThree = (ImageView) findViewById(R.id.playersThree);
        ImageView playersFour = (ImageView) findViewById(R.id.playersFour);

        setButtonPressState(playersTwo, R.drawable.two_players_pressed, R.drawable.two_players);
        setButtonPressState(playersThree, R.drawable.three_players_pressed, R.drawable.three_players);
        setButtonPressState(playersFour, R.drawable.four_players_pressed, R.drawable.four_players);

        TextView madText = (TextView) findViewById(R.id.madText);
        madText.setTypeface(neenoo);

        playersTwo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playTwoSounds("storyscramble_sfx", "team_intro4");
                madPlayers = 2;
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (!homeConfirmOpen && inRecMode) {
                            openRecordPrompts();
                        }
                    }
                }, 2000);
            }
        });
        playersThree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playTwoSounds("storyscramble_sfx", "team_intro5");
                madPlayers = 3;
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (!homeConfirmOpen && inRecMode) {
                            openRecordPrompts();
                        }
                    }
                }, 2000);
            }
        });
        playersFour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playTwoSounds("storyscramble_sfx", "team_intro6");
                madPlayers = 4;
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (!homeConfirmOpen && inRecMode) {
                            openRecordPrompts();
                        }
                    }
                }, 2000);
            }
        });
    }

    public void openSuperScramble() {
        logActivityStart();
        setContentView(R.layout.playback);

        try {
            String background = storyObj.getJSONObject("weeklyengagement").getString("background");
            ImageView playbackBackground = (ImageView) findViewById(R.id.playbackBackground);
            playbackBackground.setImageResource(getResources().getIdentifier(background, "drawable", getPackageName()));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        superMode = true;

        setStoryTitle();
        initializeHomeButton();
        initializePlaybackControls();
        try {
            playMusic(storyObj.getJSONObject("weeklyengagement").getString("music"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        storyTitleSound = null;
        try {
            storyTitleSound = storyObj.getJSONObject("weeklyengagement").getString("vo_title");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        okToPlayStory = true;
        playTwoSoundsToSetupPlayback("playback", storyTitleSound);

/*
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                readyToRestart = true;
                playSoundPair();
            }
        }, 4500);
*/
    }

    private boolean _justRecorded = false;

    public void openRecordPrompts() {
        allowHint = true;
        setContentView(R.layout.word_prompt);
        initializeHomeButton();
        hideHintArea();
        stopMusic();

        final RelativeLayout madOverlay = (RelativeLayout) findViewById(R.id.madOverlay);
        final ImageView madBackground = (ImageView) findViewById(R.id.madBackground);
        final ImageView playerIndicator = (ImageView) findViewById(R.id.playerIndicator);

        final ImageView recordButton = (ImageView) findViewById(R.id.recordBackground);
        final ImageView recordAnimationHolder = (ImageView) findViewById(R.id.pulseAnimation);
        final AnimationDrawable recordAnimation = (AnimationDrawable) recordAnimationHolder.getDrawable();
        final TextView nowRecordingText = (TextView) findViewById(R.id.nowRecordingText);
        nowRecordingText.setTypeface(neenoo);
        final ImageView progressBackground = (ImageView) findViewById(R.id.progressBackground);
        final ImageView progressAnimationHolder = (ImageView) findViewById(R.id.progressBar);
        final Animation progressAnimation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.progress);
        final ImageView useButton = (ImageView) findViewById(R.id.useButton);
        final ImageView hintButton = (ImageView) findViewById(R.id.hint);
        final ImageView closeHint = (ImageView) findViewById(R.id.closeHint);

        final ImageView hint1Button = (ImageView) findViewById(R.id.hint1Button);
        final ImageView hint2Button = (ImageView) findViewById(R.id.hint2Button);
        final ImageView hint3Button = (ImageView) findViewById(R.id.hint3Button);


        progressAnimation.cancel();
        nowRecordingText.setVisibility(View.GONE);
        progressAnimationHolder.setVisibility(View.GONE);
        progressBackground.setVisibility(View.GONE);
        recordAnimation.stop();
        recordAnimationHolder.setVisibility(View.INVISIBLE);
        //currentlyRecording = false;

        GridLayout blankProgress = (GridLayout) findViewById(R.id.blankProgress);
        blankProgress.removeAllViews();
        for(int i=0; i<numBlanks; i++) {
            ImageView thisBlank = new ImageView(getApplicationContext());
            if(i==0) {
                thisBlank.setImageResource(R.drawable.story_indicator_selected);
            } else {
                thisBlank.setImageResource(R.drawable.story_indicator_unselected);
            }
            thisBlank.setLayoutParams(new GridLayout.LayoutParams(GridLayout.spec(0), GridLayout.spec(i)));
            blankProgress.addView(thisBlank);
        }

        if(madMode) {
            madOverlay.setVisibility(View.VISIBLE);
            int currentPlayer = recordIterator%madPlayers;
            int randomNum = (int)(Math.random() * 100);
            Log.e("madMode", String.format("randomNum = %d", randomNum));
            switch(currentPlayer) {
                case 0:
                    playerIndicator.setImageResource(R.drawable.player_one);
                    if (randomNum < 51) {
                        playSound("multi_player1");
                    } else {
                        playSound("multi_player1b");
                    }
                    break;
                case 1:
                    playerIndicator.setImageResource(R.drawable.player_two);
                    if (randomNum < 51) {
                        playSound("multi_player2");
                    } else {
                        playSound("multi_player2b");
                    }
                    break;
                case 2:
                    playerIndicator.setImageResource(R.drawable.player_three);
                    if (randomNum < 51) {
                        playSound("multi_player3");
                    } else {
                        playSound("multi_player3b");
                    }

                    break;
                case 3:
                    playerIndicator.setImageResource(R.drawable.player_four);
                    if (randomNum < 51) {
                        playSound("multi_player4");
                    } else {
                        playSound("multi_player4b");
                    }

                    break;
            }

            madpromptshouldfire = true;
            // wait 3.7 seconds for 1/2/3/4
            int timeDelayMadScramble = 3700;
            // or wait 1.8 seconds for 1b/2b/3b/4b
            if(randomNum >= 51) {
                timeDelayMadScramble = 1800;
            }
            final Handler madprompthandler = new Handler();
            final Runnable madpromptrunnable = new Runnable() {
                @Override
                public void run() {
                    if(!homeConfirmOpen && madpromptshouldfire) {
                        if (madOverlay.getVisibility() == View.VISIBLE) {
                            stopSoundHelper();
                            madOverlay.setVisibility(View.GONE);
                            if (inRecMode) {
                                setBlankType();
                            }
                            Log.e("madBack", "overlay timed out");
                        }
                    }
                }
            };
            madprompthandler.postDelayed(madpromptrunnable, timeDelayMadScramble);

            madBackground.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    madpromptshouldfire = false;
                    if(!homeConfirmOpen) {
                        stopSoundHelper();
                        madOverlay.setVisibility(View.GONE);
                        setBlankType();
                        Log.e("madBack", "we clicked the player overlay");
                    }
                }
            });

        } else if(normalMode) {
            madOverlay.setVisibility(View.GONE);

            newAudioFilesSorted.clear();
            for(int i=0; i<12; i++) {
                newAudioFilesSorted.add(new ArrayList<String>());
            }

            //playSound("scramble_record");
            recordAnimationHolder.setVisibility(View.VISIBLE);
            recordAnimation.start();
            autoPlayingButtonIntro = true;

            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (autoPlayingButtonIntro) {
                        autoPlayingButtonIntro = false;
                        recordAnimationHolder.setVisibility(View.INVISIBLE);
                        recordAnimation.stop();
                    }
                }
            }, 3000);

            final ImageView background = (ImageView) findViewById(R.id.recordBackground);
            final ImageView text = (ImageView) findViewById(R.id.recordWord);
            background.setVisibility(View.VISIBLE);
            text.setVisibility(View.VISIBLE);
            hintButton.setVisibility(View.VISIBLE);
            setBlankType();
        }

        progressAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                Log.e("animation", "start");
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                Log.e("animation", "ended");
                if(currentlyRecording) {
                    recordButton.performClick();
                }

            }

            @Override
            public void onAnimationRepeat(Animation animation) {
                Log.e("animation", "repeat");
            }
        });

        recordButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _allRecorded = false;
                autoPlayingButtonIntro = false;
                recordAnimationHolder.setVisibility(View.INVISIBLE);
                recordAnimation.stop();

                hideHintArea();
                allowHint = true;
                if(!hintOpen && !madOverlayOpen && !homeConfirmOpen && !delayRecord) {
                    delayRecord = true;
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            delayRecord = false;
                        }
                    }, 750);

                    stopSoundHelper();
                    Log.e("Click", "Recording");

                    if(currentlyRecording == false) {

                        if (_justRecorded)
                            deleteTrack(lastRecordedSound);

                        currentlyRecording = true;

                        //Log.e("time", generateFileName());
                        lastRecordedSound = generateFileName();

                        soundRecorder = new MediaRecorder();
                        soundRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
                        soundRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
                        soundRecorder.setOutputFile(fileNameTemplate+lastRecordedSound+recordedFileType);
                        soundRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);
                        soundRecorder.setAudioEncodingBitRate(96000);
                        soundRecorder.setAudioSamplingRate(44100);

                        useButton.setVisibility(View.GONE);
                        nowRecordingText.setVisibility(View.VISIBLE);
                        progressBackground.setVisibility(View.VISIBLE);
                        progressAnimationHolder.setVisibility(View.VISIBLE);

                        try{
                            soundRecorder.prepare();
                        } catch(IOException e) {
                            e.printStackTrace();
                        }

                        recordAnimationHolder.setVisibility(View.VISIBLE);
                        recordAnimation.start();
                        progressAnimationHolder.startAnimation(progressAnimation);

                        soundRecorder.start();

                    } else {
                        try{
                            soundRecorder.stop();
                            soundRecorder.release();
                            soundRecorder = null;

                            useButton.setVisibility(View.VISIBLE);

                        } catch (RuntimeException e) {
                            e.printStackTrace();
                            soundRecorder.reset();
                            soundRecorder.release();
                            soundRecorder = null;
                        }

                        progressAnimation.cancel();
                        nowRecordingText.setVisibility(View.GONE);
                        progressAnimationHolder.setVisibility(View.GONE);
                        progressBackground.setVisibility(View.GONE);
                        recordAnimation.stop();
                        recordAnimationHolder.setVisibility(View.INVISIBLE);
                        currentlyRecording = false;
                        _justRecorded = true;
                        Log.e("recorded", "sound " + fileNameTemplate + lastRecordedSound + recordedFileType);

                    }
                }

            }
        });

        useButton.setVisibility(View.GONE);
        setButtonPressState(useButton, R.drawable.arrow_right, R.drawable.arrow_right_pressed);
        useButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                autoPlayingButtonIntro = false;
                recordAnimationHolder.setVisibility(View.INVISIBLE);
                recordAnimation.stop();

                hideHintArea();
                allowHint = true;
                if(!hintOpen && !madOverlayOpen && !homeConfirmOpen && !delayRecord) {
                    Log.e("Click", "Use");
                    _justRecorded = false;

                    readyToRestart = false;

                    playSound("nextpage_sfx");
                    newAudioFiles.add(lastRecordedSound);
                    _tempRecordingList.add(lastRecordedSound);
                    newAudioFilesSorted.get(Integer.parseInt(blankPrompt.get(recordIterator))-1).add(lastRecordedSound);
                    Log.e("sorted", newAudioFilesSorted.toString());
                    //_tempRecordingList = newAudioFiles;
                    recordIterator++;

                    if(recordIterator>=numBlanks){

                        _allRecorded = true;
                        _tempRecordingList.clear();

                        Log.e("filenum", "new length of audio list "+newAudioFiles.size());

                        Log.e("WRITING","full list of new files");

                        writeAudioList(newAudioFilesSorted);

                        newAudioFilesSorted.clear();
                        for(int i=0; i<12; i++) {
                            newAudioFilesSorted.add(new ArrayList<String>());
                        }



                        setContentView(R.layout.playback);

                        try {
                            String background = storyObj.getJSONObject("weeklyengagement").getString("background");
                            ImageView playbackBackground = (ImageView) findViewById(R.id.playbackBackground);
                            playbackBackground.setImageResource(getResources().getIdentifier(background, "drawable", getPackageName()));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        initializeHomeButton();
                        initializePlaybackControls();
                        setStoryTitle();

                        try {
                            playMusic(storyObj.getJSONObject("weeklyengagement").getString("music"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        storyTitleSound = null;
                        try {
                            storyTitleSound = storyObj.getJSONObject("weeklyengagement").getString("vo_title");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        playTwoSoundsToSetupPlayback("playback", storyTitleSound);

/*
                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                if(normalMode || madMode) {
                                    okToPlayStory = true;
                                    playSoundPair();
                                }
                            }
                        }, 4500);
*/

                    } else {

                        GridLayout blankProgress = (GridLayout) findViewById(R.id.blankProgress);
                        blankProgress.removeAllViews();

                        for(int i=0; i<numBlanks; i++) {
                            ImageView thisBlank = new ImageView(getApplicationContext());
                            if(i==recordIterator) {
                                thisBlank.setImageResource(R.drawable.story_indicator_selected);
                            } else {
                                thisBlank.setImageResource(R.drawable.story_indicator_unselected);
                            }
                            thisBlank.setLayoutParams(new GridLayout.LayoutParams(GridLayout.spec(0), GridLayout.spec(i)));
                            blankProgress.addView(thisBlank);
                        }

                        Log.e("filenum", "new length of audio list "+newAudioFiles.size());

                        if(madMode) {
                            int currentPlayer = recordIterator%madPlayers;
                            int randomNum = (int)(Math.random() * 100);
                            Log.e("madMode", String.format("randomNum = %d", randomNum));
                            switch(currentPlayer) {
                                case 0:
                                    playerIndicator.setImageResource(R.drawable.player_one);
                                    if (randomNum < 51) {
                                        playSound("multi_player1");
                                    } else {
                                        playSound("multi_player1b");
                                    }
                                    break;
                                case 1:
                                    playerIndicator.setImageResource(R.drawable.player_two);
                                    if (randomNum < 51) {
                                        playSound("multi_player2");
                                    } else {
                                        playSound("multi_player2b");
                                    }
                                    break;
                                case 2:
                                    playerIndicator.setImageResource(R.drawable.player_three);
                                    if (randomNum < 51) {
                                        playSound("multi_player3");
                                    } else {
                                        playSound("multi_player3b");
                                    }

                                    break;
                                case 3:
                                    playerIndicator.setImageResource(R.drawable.player_four);
                                    if (randomNum < 51) {
                                        playSound("multi_player4");
                                    } else {
                                        playSound("multi_player4b");
                                    }

                                    break;
                            }
                            madOverlay.setVisibility(View.VISIBLE);

                            madpromptshouldfire = true;
                            // wait 3.7 seconds for 1/2/3/4
                            int timeDelayMadScramble = 3700;
                            // or wait 1.8 seconds for 1b/2b/3b/4b
                            if(randomNum >= 51) {
                                timeDelayMadScramble = 1800;
                            }
                            final Handler madprompthandler = new Handler();
                            final Runnable madpromptrunnable = new Runnable() {
                                @Override
                                public void run() {
                                    if(!homeConfirmOpen && madpromptshouldfire && inRecMode) {
                                        if (madOverlay.getVisibility() == View.VISIBLE) {
                                            stopSoundHelper();
                                            madOverlay.setVisibility(View.GONE);
                                            setBlankType();
                                            Log.e("madBack", "overlay timed out");
                                        }
                                    }
                                }
                            };
                            madprompthandler.postDelayed(madpromptrunnable, timeDelayMadScramble);

                        } else {

                            TranslateAnimation recordButtonAnimation = new TranslateAnimation(0f, 0f,
                                    0f, -800f);
                            recordButtonAnimation.setDuration(500);


                            View tempRecordButton = findViewById(R.id.recordContainer);
                            tempRecordButton.startAnimation(recordButtonAnimation);


                            final Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    setBlankType();
                                }
                            }, 500);

                        }

                        useButton.setVisibility(View.GONE);
                    }
                }

            }
        });

        setButtonPressState(hintButton, R.drawable.help_button_assets, R.drawable.help_pressed);
        hintButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (allowHint) {
                    autoPlayingButtonIntro = false;
                    recordAnimationHolder.setVisibility(View.INVISIBLE);
                    recordAnimation.stop();

                    if(!madOverlayOpen && !homeConfirmOpen) {
                        Log.e("Hint", "open hint");
                        showHintArea();
                        allowHint = false;
                        //playTwoSounds(hintExplanationSource, "example_intro1");
                        playThreeSounds("hint_sfx", hintExplanationSource, "example_intro1");
                        if(currentlyRecording) {
                            try{
                                soundRecorder.stop();
                                soundRecorder.release();
                                soundRecorder = null;
                            } catch (RuntimeException e) {
                                e.printStackTrace();
                                soundRecorder.reset();
                                soundRecorder.release();
                                soundRecorder = null;
                            }

                            progressAnimation.cancel();
                            nowRecordingText.setVisibility(View.GONE);
                            progressAnimationHolder.setVisibility(View.GONE);
                            progressBackground.setVisibility(View.GONE);
                            recordAnimation.stop();
                            recordAnimationHolder.setVisibility(View.INVISIBLE);
                            currentlyRecording = false;
                        }
                    }
                }
            }
        });

        closeHint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!madOverlayOpen && !homeConfirmOpen) {
                    stopSoundHelper();
                    hideHintArea();
                    allowHint = true;
                    playSound("storyplayend_sfx");
                    Log.e("test", "test");
                }
            }
        });

        hint1Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!madOverlayOpen && !homeConfirmOpen) {
                    playSound(hint1Source);
                }
            }
        });

        hint2Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!madOverlayOpen && !homeConfirmOpen) {
                    playSound(hint2Source);
                }
            }
        });

        hint3Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!madOverlayOpen && !homeConfirmOpen) {
                    playSound(hint3Source);
                }
            }
        });

    }

    private void deleteTrack(String filename){
        Log.e("DELETING",fileNameTemplate+filename+recordedFileType);
        File file = new File(fileNameTemplate+filename+recordedFileType);
        file.delete();
    }

    public void resetTrackers() {

        normalMode = false;
        superMode = false;
        madMode = false;

        homeConfirmOpen = false;
        deleteOneConfirmOpen = false;
        deleteAllConfirmOpen = false;

        for(int i=0; i<audioManagerToDelete.size(); i++) {
            int[] temp = audioManagerToDelete.get(i);
            deleteOneAudioListFile(temp[0], temp[1]);
        }
        audioManagerToDelete.clear();

        invisibleActive = false;

        newAudioFiles.clear();
        repeatedAudio.clear();
        isRepeatedAudioStock.clear();

        phraseIterator = 0;
        audioIterator = 0;
        blankIterator = 0;
        recordIterator = 0;
        currentlyRecording = false;
        hintOpen = false;
    }

    public String readFile(String name){


        InputStream is;
        String line = null;
        String full = "";
        try {
            is = getAssets().open(name);

            BufferedReader br = new BufferedReader(new InputStreamReader(is));

            while ((line = br.readLine()) != null) {
//	    	    Log.e("wtf", line);
                full += line;
            }
            br.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

        return full;

    }

    public JSONObject getCurrentGame(){


        String[] files;
        String match = getInstance() + "_";
        String fname = null;

        setTimestamp(System.currentTimeMillis());

        try {
            files = getAssets().list("json/gamedata");

            for(int i = 0; i < files.length; i++){
                if(files[i].contains(match)){
                    fname = files[i];
                    break;
                }
            }

            if(fname == null){
                fname = files[0];

                int inst = Integer.parseInt(fname.substring(0, 4));
                setInstance(inst);
                Log.d("CORE", "New Instance: " + inst);
            }

            return new JSONObject(readFile("json/gamedata/"+fname));



        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        //failsafe

        return loadDefaultGame();

    }

    private final String defaultGame = "{}";

    public JSONObject loadDefaultGame(){
        try {
            return new JSONObject(defaultGame);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return new JSONObject();
    }

    private String getProfileLoc(){
        return "json/profile.json";
    }

    public JSONObject profileObject(){

        String proff = readFile(getProfileLoc());


        try {
            return new JSONObject(proff);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;

    }

    public String getUsersName(){
        try {
            return profileObject().getString("Name");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }

    public int getInstance(){

        SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
        return settings.getInt(INSTANCE, -1);

    }

    public int getSubInstance(){

        SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
        return settings.getInt(SUB_INSTANCE, -1);

    }

    public void setSubInstance(int instance){
        SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
        settings.edit().putInt(SUB_INSTANCE, instance).commit();

    }

    public long getTimestamp(){

        SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
        return settings.getLong(TIMESTAMP, -1);

    }

    public void setInstance(int instance){
        SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
        settings.edit().putInt(INSTANCE, instance).commit();

    }

    public void setTimestamp(long time){
        SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
        settings.edit().putLong(TIMESTAMP, time).commit();

    }

    public Drawable getImage(String name){

        int id = getResources().getIdentifier("drawable/ic_launcher", null, getPackageName());
        if(id != 0)
            return this.getResources().getDrawable(id);
        return null;
    }

    public Uri getAudio(String name){

        int id = getResources().getIdentifier(name, "raw", getPackageName());
        return Uri.parse("android.resource://"+getPackageName()+"/" + id);
    }

    public String getRandomStockRecordingByIndex(String index) {
        String recording = "";
        String audioList = readAudioList();
        JSONObject audioObj = null;

        try {
            audioObj = new JSONObject(audioList);
            if(audioObj.getJSONArray(index).length()<=0) {
                JSONArray tempArray = blankObj.getJSONArray(index);
                int randomSelection = (int) Math.floor(Math.random()*tempArray.length());
                recording = tempArray.getString(randomSelection);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return recording;
    }

    public String getRandomCustomRecordingByIndex(String index) {
        String recording = "";
        String audioList = readAudioList();
        JSONObject audioObj = null;

        try {
            audioObj = new JSONObject(audioList);
            int randomSelection = (int) Math.floor(Math.random()*audioObj.getJSONArray(index).length());
            recording = audioObj.getJSONArray(index).getString(randomSelection);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return recording;
    }

    private boolean becauseScreenOff = false;
    public void playSoundPair() {
        if (!((PowerManager) getSystemService(Context.POWER_SERVICE)).isScreenOn()) {
            Log.e("SOUNDPAIR","screen off");
            okToPlayStory = false;
            readyToRestart = true;
            becauseScreenOff = true;
        } else{
            if (becauseScreenOff){
                becauseScreenOff = false;
                okToPlayStory = true;

            }
        }
        if (okToPlayStory) {
            Log.e("SOUNDPAIR","it is ok to play the story");

            getWindow().getDecorView().setKeepScreenOn(true);
            Log.e("setKeepScreenOn", ">>>>> true 2");

            wasAboutToDoPlaySoundPair = false;

            currentlyPlaying = true;
            readyToRestart = true;

            boolean escapeBlankVo = false;

            try {
                final JSONArray thisPhrase = storyArray.getJSONArray(phraseIterator);
                JSONObject thisSound = thisPhrase.getJSONObject(audioIterator);

                if(audioIterator == 0) { //this is called at the beginning of a new phrase
                    final TextView storyText = (TextView) findViewById(R.id.storyText);
                    storyText.setTypeface(fontRegular);
                    final TextView storyBlue = (TextView) findViewById(R.id.storyBlue);
                    storyBlue.setTypeface(fontBlue);
                    final TextView storyBlack = (TextView) findViewById(R.id.storyBlack);
                    storyBlack.setTypeface(fontBlack);
                    final TextView storyPurple = (TextView) findViewById(R.id.storyPurple);
                    storyPurple.setTypeface(fontPurple);
                    final TextView storyPink = (TextView) findViewById(R.id.storyPink);
                    storyPink.setTypeface(fontPink);
                    final TextView storyWhite = (TextView) findViewById(R.id.storyWhite);
                    storyWhite.setTypeface(fontWhite);

                    String phraseText = "";
                    for(int i=0; i<thisPhrase.length(); i++) {
                        if(thisPhrase.getJSONObject(i).has("text")) {
                            if (InstanceChecker.getInstance().isUK() && thisPhrase.getJSONObject(i).has("text_uk")) {
                                Log.e("CONTENT", "isUK returned TRUE TEXT");
                                phraseText += thisPhrase.getJSONObject(i).getString("text_uk");
                            } else {
                                Log.e("CONTENT", "isUK returned FALSE TEXT");
                                phraseText += thisPhrase.getJSONObject(i).getString("text");
                            }
                        } else if(thisPhrase.getJSONObject(i).has("blanktype") || thisPhrase.getJSONObject(i).has("repeat")) {
                            phraseText += "%";
                        }
                        phraseText+="_";
                    }
                    storyText.setText(phraseText);
                    storyBlue.setText(phraseText);
                    storyBlack.setText(phraseText);
                    storyPurple.setText(phraseText);
                    storyWhite.setText(phraseText);
                    storyPink.setText(phraseText);
                }

                soundPlayer = new MediaPlayer();

                if(thisSound.has("vo")){
                    if (InstanceChecker.getInstance().isUK() && thisSound.has("vo_uk")) {
                        Log.e("CONTENT", "isUK returned TRUE VO");
                        if(!thisSound.getString("vo_uk").isEmpty()){
                            Log.e("VO", "found non-blank vo "+thisSound.getString("vo_uk"));
                            soundPlayer.setDataSource(this, getAudio(thisSound.getString("vo_uk")));
                        } else {
                            Log.e("NoVo", "VO left blank");
                            escapeBlankVo = true;
                        }
                    } else {
                        Log.e("CONTENT", "isUK returned FALSE VO");
                        if(!thisSound.getString("vo").isEmpty()){
                            Log.e("VO", "found non-blank vo "+thisSound.getString("vo"));
                            soundPlayer.setDataSource(this, getAudio(thisSound.getString("vo")));
                        } else {
                            Log.e("NoVo", "VO left blank");
                            escapeBlankVo = true;
                        }
                    }

                } else if(thisSound.has("blanktype")) {
                    Log.e("blanknum", "i am currently on blank "+(blankIterator+1)+" of "+numBlanks);

                    boolean doSave = false;
                    if(thisSound.has("save")) {
                        doSave = true;
                    }

                    if(superMode) {
                        String randomSound = getRandomStockRecordingByIndex(thisSound.getString("blanktype"));
                        if(randomSound.equals("")) {
                            randomSound = getRandomCustomRecordingByIndex(thisSound.getString("blanktype"));
                            soundPlayer.setDataSource(fileNameTemplate+randomSound+recordedFileType);
                            if(doSave) {
                                repeatedAudio.add(fileNameTemplate+randomSound+recordedFileType);
                                isRepeatedAudioStock.add(false);
                            }
                        } else {
                            Log.e("play", "attempting to play sound "+randomSound);
                            soundPlayer.setDataSource(this, getAudio(randomSound));
                            if(doSave) {
                                repeatedAudio.add(randomSound);
                                isRepeatedAudioStock.add(true);
                            }
                        }

                    } else {
                        soundPlayer.setDataSource(fileNameTemplate+newAudioFiles.get(blankIterator)+recordedFileType);
                        if(doSave) {
                            repeatedAudio.add(fileNameTemplate+newAudioFiles.get(blankIterator)+recordedFileType);
                            isRepeatedAudioStock.add(false);
                        }
                    }
                    if(doSave) Log.e("saved", "just saved audio "+repeatedAudio.get(repeatedAudio.size()-1));

                    blankIterator++;
                } else if(thisSound.has("repeat")){
                    Log.e("playrepeat", "attempting to play sound "+repeatedAudio.get(Integer.parseInt(thisSound.getString("repeat"))-1));
                    int repeatIndex = Integer.parseInt(thisSound.getString("repeat"))-1;
                    if(isRepeatedAudioStock.get(repeatIndex)){
                        soundPlayer.setDataSource(this, getAudio(repeatedAudio.get(repeatIndex)));
                    } else {
                        soundPlayer.setDataSource(repeatedAudio.get(repeatIndex));
                    }


                } else{
                    Log.e("NoVo", "No blank or vo found, skipping");
                    escapeBlankVo = true;
                }

                if(!escapeBlankVo) {
                    soundPlayer.prepare();
                    soundPlayer.setVolume(0.9f,0.9f);

                    soundPlayer.start();
                    soundPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                        @Override
                        public void onCompletion(MediaPlayer mediaPlayer) {
                           Log.e("STORYPLAYER", "sound completed, releasing");
                            mediaPlayer.stop();
                            mediaPlayer.reset();
                            mediaPlayer.release();
                            mediaPlayer = null;
                            audioIterator++;
                            if(audioIterator >= thisPhrase.length()) {
                                audioIterator = 0;
                                phraseIterator++;
                            }
                            if(phraseIterator < storyArray.length()) {
                                Log.e("story", "story continues... 1");
                                wasAboutToDoPlaySoundPair = true;
                                playSoundPair();
                            } else {
                                soundPlayer.release();
                                soundPlayer = null;
                                Log.e("story", "story is over 1");
                                currentlyPlaying = false;
                                final ImageView playPauseButton = (ImageView) findViewById(R.id.pauseButton);
                                playPauseButton.setAlpha(0.5f);
                                doTheEnd();
                            }
                        }
                    });
                } else {
                    escapeBlankVo = false;
                    soundPlayer.release();
                    soundPlayer = null;

                    audioIterator++;
                    if(audioIterator >= thisPhrase.length()) {
                        audioIterator = 0;
                        phraseIterator++;
                    }
                    if(phraseIterator < storyArray.length()) {
                        Log.e("story", "story continues... 2");
                        wasAboutToDoPlaySoundPair = true;
                        playSoundPair();
                    } else {
                        Log.e("story", "story is over 2");
                        currentlyPlaying = false;
                        final ImageView playPauseButton = (ImageView) findViewById(R.id.pauseButton);
                        playPauseButton.setAlpha(0.5f);
                        doTheEnd();
                    }
                }

            } catch (JSONException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else{
            Log.e("SOUNDPAIR","not ok to play story");
        }
    }

    public void doTheEnd() {
        //getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        getWindow().getDecorView().setKeepScreenOn(false);
        Log.e("setKeepScreenOn",">>>>> false 4");
        readyToRestart = false;
        final TextView storyText = (TextView) findViewById(R.id.storyText);
        final TextView storyBlue = (TextView) findViewById(R.id.storyBlue);
        final TextView storyBlack = (TextView) findViewById(R.id.storyBlack);
        final TextView storyPurple = (TextView) findViewById(R.id.storyPurple);
        final TextView storyWhite = (TextView) findViewById(R.id.storyWhite);
        final TextView storyPink = (TextView) findViewById(R.id.storyPink);
        storyText.setVisibility(View.GONE);
        storyBlue.setVisibility(View.GONE);
        storyBlack.setVisibility(View.GONE);
        storyPurple.setVisibility(View.GONE);
        storyWhite.setVisibility(View.GONE);
        storyPink.setVisibility(View.GONE);
        final ImageView theEnd = (ImageView) findViewById(R.id.theEnd);
        theEnd.setVisibility(View.VISIBLE);
        if (InstanceChecker.getInstance().getNewWeek(this)) {
            rewardsLayout = (RelativeLayout) findViewById(R.id.rewardsLayout);
            rewardsLayout.addView(_rewarder);
            _rewarder.setCallback(new Runnable() {
                @Override
                public void run() {
                    Log.d("REWARDS", "Removing rewards view");
                    rewardsLayout.removeView(_rewarder);
                    playSound("replay_prompt");
                    readyToRestart = true;
                }
            });
            _rewarder.earnCoins(35);
            scoreForActivity = 0;
            // if LF decides they want to track tokens on ActivityExit, replace this with value of tokens earned
        } else {
            playSound("replay_prompt");
            readyToRestart = true;
        }
        InstanceChecker.getInstance().setNewWeek(false);
    }

    public void findNumBlanks() {
        numBlanks = 0;
        blankPrompt.clear();
        for(int i=0; i<storyArray.length(); i++) {
            try {
                JSONArray subArray = storyArray.getJSONArray(i);
                for(int j=0; j<subArray.length(); j++) {
                    if(subArray.getJSONObject(j).has("blanktype")) {
                        blankPrompt.add(subArray.getJSONObject(j).getString("blanktype"));
                        Log.e("Blanks", numBlanks+" Found "+blankPrompt.get(numBlanks)+" blanks");
                        numBlanks++;
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

    }

    public void setBlankType() {
        String type = blankPrompt.get(recordIterator);
        Log.e("setblank", "blanktype is "+type);

        ImageView background = (ImageView) findViewById(R.id.recordBackground);
        ImageView text = (ImageView) findViewById(R.id.recordWord);

        TextView hintText = (TextView) findViewById(R.id.hintWordType);
        hintText.setTypeface(neenoo);
        TextView hint1 = (TextView) findViewById(R.id.hint1Text);
        hint1.setTypeface(neenoo);
        TextView hint2 = (TextView) findViewById(R.id.hint2Text);
        hint2.setTypeface(neenoo);
        TextView hint3 = (TextView) findViewById(R.id.hint3Text);
        hint3.setTypeface(neenoo);

        int randomHelpNum = (int)(Math.random() * 6);
        Log.e("randomHelpNum", String.format("randomHelpNum >>>>>> = %d", randomHelpNum));

        int[] myIntArray = new int[3];
        switch (randomHelpNum) {
            case 0:
                myIntArray[0] = 0;
                myIntArray[1] = 1;
                myIntArray[2] = 2;
                break;
            case 1:
                myIntArray[0] = 3;
                myIntArray[1] = 4;
                myIntArray[2] = 5;
                break;
            case 2:
                myIntArray[0] = 0;
                myIntArray[1] = 2;
                myIntArray[2] = 4;
                break;
            case 3:
                myIntArray[0] = 1;
                myIntArray[1] = 3;
                myIntArray[2] = 5;
                break;
            case 4:
                myIntArray[0] = 1;
                myIntArray[1] = 2;
                myIntArray[2] = 4;
                break;
            default:
                myIntArray[0] = 0;
                myIntArray[1] = 3;
                myIntArray[2] = 5;
                break;
        }

        String[] ary1text = {"Cat","Monkey","Fire Engine","Baseball","Bumblebee","Sandal"};
        String[] ary1source = {"example_noun_1","example_noun_2","example_noun_3","example_noun_4","example_noun_5","example_noun_6"};
        String[] ary2text = {"Zebras","Drums","Unicorns","Bicycles","Bunnies","Beachballs"};
        String[] ary2source = {"example_pluralnoun_1","example_pluralnoun_2","example_pluralnoun_3","example_pluralnoun_4","example_pluralnoun_5","example_pluralnoun_6"};
        String[] ary3text = {"Run","Explode","Snore","Laugh","Whisper","Scurry"};
        String[] ary3source = {"example_verb_1","example_verb_2","example_verb_3","example_verb_4","example_verb_5","example_verb_6"};
        String[] ary4text = {"Swimming","Singing","Dancing","Giggling","Driving","Surfing"};
        String[] ary4source = {"example_verbing_1","example_verbing_2","example_verbing_3","example_verbing_4","example_verbing_5","example_verbing_6"};
        String[] ary5text = {"Purple","Stinky","Grumpy","Smooth","Fuzzy","Loud"};
        String[] ary5source = {"example_adj_1","example_adj_2","example_adj_3","example_adj_4","example_adj_5","example_adj_6"};
        String[] ary6text = {"Sound 1","Sound 2","Sound 3","Sound 4","Sound 5","Sound 6"};
        String[] ary6source = {"example_sfx_1","example_sfx_2","example_sfx_3","example_sfx_4","example_sfx_5","example_sfx_6"};
        String[] ary7text = {"One","Forty seven","A thousand","Six","Twenty","A billion"};
        String[] ary7source = {"example_number_1","example_number_2","example_number_3","example_number_4","example_number_5","example_number_6"};
        String[] ary8text = {"Pizza","Hamburger","Cucumber","Salad","Pasta","Broccoli"};
        String[] ary8source = {"example_food_1","example_food_2","example_food_3","example_food_4","example_food_5","example_food_6"};
        String[] ary9text = {"Mom","Dad","My sister","My brother","George Washington","Cinderella"};
        String[] ary9source = {"example_person_1","example_person_2","example_person_3","example_person_4","example_person_5","example_person_6"};
        String[] ary10text = {"Song 1","Song 2","Song 3","Song 4","Song 5","Song 6"};
        String[] ary10source = {"example_song_1","example_song_2","example_song_3","example_song_4","example_song_5","example_song_6"};
        String[] ary11text = {"Wow","Hurray","Oh no","Whoa","Yippee","Ta da"};
        String[] ary11source = {"example_excl_1","example_excl_2","example_excl_3","example_excl_4","example_excl_5","example_excl_6"};
        String[] ary12text = {"Mooooo","Baaa","Woof Woof","Meow","Caw Caw","Oink Oink"};
        String[] ary12source = {"example_animalnoise_1","example_animalnoise_2","example_animalnoise_3","example_animalnoise_4","example_animalnoise_5","example_animalnoise_6"};

        if (InstanceChecker.getInstance().isUK()) {
            Log.e("CONTENT", "isUK returned TRUE");
            ary9text[0] = "Mum";
            ary9source[0] = "example_person_1_uk";
        } else {
            Log.e("CONTENT", "isUK returned FALSE");
        }

        switch(type) {
            case "1":
                text.setImageResource(R.drawable.noun_keyword);
                background.setImageResource(R.drawable.blue_bg);
                setButtonPressState(background, R.drawable.blue_bg, R.drawable.blue_bg_pressed);
                hintText.setText("A person, place or thing like 'dog' or 'city'");
                hintTypeSource = "record_noun1";
                hintSfxSource = "noun_sfx";
                hintExplanationSource = "noun_song";
                hint1.setText(ary1text[myIntArray[0]]);
                hint2.setText(ary1text[myIntArray[1]]);
                hint3.setText(ary1text[myIntArray[2]]);
                hint1Source = ary1source[myIntArray[0]];
                hint2Source = ary1source[myIntArray[1]];
                hint3Source = ary1source[myIntArray[2]];
                break;
            case "2":
                text.setImageResource(R.drawable.plural_noun_keyword);
                background.setImageResource(R.drawable.blue_green_bg);
                setButtonPressState(background, R.drawable.blue_green_bg, R.drawable.blue_green_bg_pressed);
                hintText.setText("Several people, places, or things, like 'dogs' or 'cities'");
                hintTypeSource = "record_pluralnoun1";
                hintSfxSource = "pluralnoun_sfx";
                hintExplanationSource = "pluralnoun_song";
                hint1.setText(ary2text[myIntArray[0]]);
                hint2.setText(ary2text[myIntArray[1]]);
                hint3.setText(ary2text[myIntArray[2]]);
                hint1Source = ary2source[myIntArray[0]];
                hint2Source = ary2source[myIntArray[1]];
                hint3Source = ary2source[myIntArray[2]];
                break;
            case "3":
                text.setImageResource(R.drawable.verb_keyword);
                background.setImageResource(R.drawable.green_bg);
                setButtonPressState(background, R.drawable.green_bg, R.drawable.green_bg_pressed);
                hintText.setText("An action or something you do like 'run' or 'explode'");
                hintTypeSource = "record_verb1";
                hintSfxSource = "verb_sfx";
                hintExplanationSource = "verb_song";
                hint1.setText(ary3text[myIntArray[0]]);
                hint2.setText(ary3text[myIntArray[1]]);
                hint3.setText(ary3text[myIntArray[2]]);
                hint1Source = ary3source[myIntArray[0]];
                hint2Source = ary3source[myIntArray[1]];
                hint3Source = ary3source[myIntArray[2]];
                break;
            case "4":
                text.setImageResource(R.drawable.verb_ing_keyword);
                background.setImageResource(R.drawable.green_yellow_bg);
                setButtonPressState(background, R.drawable.green_yellow_bg, R.drawable.green_yellow_bg_pressed);
                hintText.setText("An action or something you do that ends in -ing like 'running' or 'exploding'");
                hintTypeSource = "record_verbing1";
                hintSfxSource = "verb_sfx";
                hintExplanationSource = "verb_ing_song";
                hint1.setText(ary4text[myIntArray[0]]);
                hint2.setText(ary4text[myIntArray[1]]);
                hint3.setText(ary4text[myIntArray[2]]);
                hint1Source = ary4source[myIntArray[0]];
                hint2Source = ary4source[myIntArray[1]];
                hint3Source = ary4source[myIntArray[2]];
                break;
            case "5":
                text.setImageResource(R.drawable.adjective_keyword);
                background.setImageResource(R.drawable.orange_bg);
                setButtonPressState(background, R.drawable.orange_bg, R.drawable.orange_bg_pressed);
                hintText.setText("A word that describes people, places, or things like 'quick' or 'happy'");
                hintTypeSource = "record_adjective1";
                hintSfxSource = "adjective_sfx";
                hintExplanationSource = "adjective_song_alt";
                hint1.setText(ary5text[myIntArray[0]]);
                hint2.setText(ary5text[myIntArray[1]]);
                hint3.setText(ary5text[myIntArray[2]]);
                hint1Source = ary5source[myIntArray[0]];
                hint2Source = ary5source[myIntArray[1]];
                hint3Source = ary5source[myIntArray[2]];
                break;
            case "6":
                text.setImageResource(R.drawable.sound_effect_keyword);
                background.setImageResource(R.drawable.orange_red_bg);
                setButtonPressState(background, R.drawable.orange_red_bg, R.drawable.orange_red_bg_pressed);
                hintText.setText("Go ahead - make any sound you want");
                hintTypeSource = "record_soundeffect1";
                hintSfxSource = "soundeffect_sfx";
                hintExplanationSource = "record_soundeffect2";
                hint1.setText(ary6text[myIntArray[0]]);
                hint2.setText(ary6text[myIntArray[1]]);
                hint3.setText(ary6text[myIntArray[2]]);
                hint1Source = ary6source[myIntArray[0]];
                hint2Source = ary6source[myIntArray[1]];
                hint3Source = ary6source[myIntArray[2]];
                break;
            case "7":
                text.setImageResource(R.drawable.number_keyword);
                background.setImageResource(R.drawable.red_bg);
                setButtonPressState(background, R.drawable.red_bg, R.drawable.red_bg_pressed);
                hintText.setText("Pick any number");
                hintTypeSource = "record_number1";
                hintSfxSource = "number_sfx";
                hintExplanationSource = "record_number2";
                hint1.setText(ary7text[myIntArray[0]]);
                hint2.setText(ary7text[myIntArray[1]]);
                hint3.setText(ary7text[myIntArray[2]]);
                hint1Source = ary7source[myIntArray[0]];
                hint2Source = ary7source[myIntArray[1]];
                hint3Source = ary7source[myIntArray[2]];
                break;
            case "8":
                text.setImageResource(R.drawable.food_keyword);
                background.setImageResource(R.drawable.red_violet_bg);
                setButtonPressState(background, R.drawable.red_violet_bg, R.drawable.red_violet_bg_pressed);
                hintText.setText("Pick any food");
                hintTypeSource = "record_food1";
                hintSfxSource = "food_sfx";
                hintExplanationSource = "record_food2";
                hint1.setText(ary8text[myIntArray[0]]);
                hint2.setText(ary8text[myIntArray[1]]);
                hint3.setText(ary8text[myIntArray[2]]);
                hint1Source = ary8source[myIntArray[0]];
                hint2Source = ary8source[myIntArray[1]];
                hint3Source = ary8source[myIntArray[2]];
                break;
            case "9":
                text.setImageResource(R.drawable.person_keyword);
                background.setImageResource(R.drawable.violet_bg);
                setButtonPressState(background, R.drawable.violet_bg, R.drawable.violet_bg_pressed);
                hintText.setText("A name of any person, even someone in the room");
                hintTypeSource = "record_person1";
                hintSfxSource = "person_sfx";
                hintExplanationSource = "record_person2";
                hint1.setText(ary9text[myIntArray[0]]);
                hint2.setText(ary9text[myIntArray[1]]);
                hint3.setText(ary9text[myIntArray[2]]);
                hint1Source = ary9source[myIntArray[0]];
                hint2Source = ary9source[myIntArray[1]];
                hint3Source = ary9source[myIntArray[2]];
                break;
            case "10":
                text.setImageResource(R.drawable.song_keyword);
                background.setImageResource(R.drawable.violet_blue_bg);
                setButtonPressState(background, R.drawable.violet_blue_bg, R.drawable.violet_blue_bg_pressed);
                hintText.setText("Just sing for a couple seconds");
                hintTypeSource = "record_song1";
                hintSfxSource = "song_sfx";
                hintExplanationSource = "record_song2";
                hint1.setText(ary10text[myIntArray[0]]);
                hint2.setText(ary10text[myIntArray[1]]);
                hint3.setText(ary10text[myIntArray[2]]);
                hint1Source = ary10source[myIntArray[0]];
                hint2Source = ary10source[myIntArray[1]];
                hint3Source = ary10source[myIntArray[2]];
                break;
            case "11":
                text.setImageResource(R.drawable.exclamation_keyword);
                background.setImageResource(R.drawable.yellow_bg);
                setButtonPressState(background, R.drawable.yellow_bg, R.drawable.yellow_bg_pressed);
                hintText.setText("Something you might say when you are happy or surprised, like 'wow!' or 'oh no!'");
                hintTypeSource = "record_exclamation1";
                hintSfxSource = "exclamation_sfx";
                hintExplanationSource = "exclamation_song_alt";
                hint1.setText(ary11text[myIntArray[0]]);
                hint2.setText(ary11text[myIntArray[1]]);
                hint3.setText(ary11text[myIntArray[2]]);
                hint1Source = ary11source[myIntArray[0]];
                hint2Source = ary11source[myIntArray[1]];
                hint3Source = ary11source[myIntArray[2]];
                break;
            case "12":
                text.setImageResource(R.drawable.animal_noise_keyword);
                background.setImageResource(R.drawable.yellow_orange_bg);
                setButtonPressState(background, R.drawable.yellow_orange_bg, R.drawable.yellow_orange_bg_pressed);
                hintText.setText("Make any animal sound");
                hintTypeSource = "record_animalnoise1";
                hintSfxSource = "exclamation_sfx";
                hintExplanationSource = "record_animalnoise2";
                hint1.setText(ary12text[myIntArray[0]]);
                hint2.setText(ary12text[myIntArray[1]]);
                hint3.setText(ary12text[myIntArray[2]]);
                hint1Source = ary12source[myIntArray[0]];
                hint2Source = ary12source[myIntArray[1]];
                hint3Source = ary12source[myIntArray[2]];
                break;
        }

        hintText.setText("Here are some ideas!");

        // resize hint font as needed
        if(hint1.getText().length() > 14) {
            hint1.setTextSize(8f);
        } else if(hint1.getText().length() > 7) {
            hint1.setTextSize(14f);
        } else {
            hint1.setTextSize(20f);
        }
        if(hint2.getText().length() > 14) {
            hint2.setTextSize(8f);
        } else if(hint2.getText().length() > 7) {
            hint2.setTextSize(14f);
        } else {
            hint2.setTextSize(20f);
        }
        if(hint3.getText().length() > 14) {
            hint3.setTextSize(8f);
        } else if(hint3.getText().length() > 7) {
            hint3.setTextSize(14f);
        } else {
            hint3.setTextSize(20f);
        }

        if (normalMode) {
            Log.e("normalMode", String.format("numBlanks = %d", numBlanks));
            Log.e("normalMode", String.format("recordIterator = %d", recordIterator));
            if (recordIterator == 0) {
                playThreeSounds("scramble_record", hintSfxSource, hintTypeSource);
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        //animate big button
                        //wait (total of) 3 sec, then animate bug button again
                    }
                }, 500);
            } else {
                if ((recordIterator+1) >= numBlanks) {
                    playThreeSounds("scramble_start3", hintSfxSource, hintTypeSource);
                } else {
                    int randomNum = (int)(Math.random() * 100);
                    if (randomNum < 51) {
                        playThreeSounds("scramble_start2", hintSfxSource, hintTypeSource);
                    } else {
                        playThreeSounds("scramble_startb", hintSfxSource, hintTypeSource);
                    }
                }
            }
        } else {
            playTwoSounds(hintSfxSource, hintTypeSource);
        }
    }

    public void fakehome(){
        if(!homeConfirmOpen && !deleteAllConfirmOpen && !deleteOneConfirmOpen) {
            homeConfirmOpen = true;
            allowHint = true;
            if (soundRecorder != null) {
                if(currentlyRecording) {
                    try{
                        soundRecorder.stop();
                        soundRecorder.release();
                        soundRecorder = null;
                    } catch (RuntimeException e) {
                        e.printStackTrace();
                        soundRecorder.reset();
                        soundRecorder.release();
                        soundRecorder = null;
                    }

                    autoPlayingButtonIntro = false;

                    final ImageView recordAnimationHolder = (ImageView) findViewById(R.id.pulseAnimation);
                    final AnimationDrawable recordAnimation = (AnimationDrawable) recordAnimationHolder.getDrawable();
                    final TextView nowRecordingText = (TextView) findViewById(R.id.nowRecordingText);
                    final ImageView progressBackground = (ImageView) findViewById(R.id.progressBackground);
                    final ImageView progressAnimationHolder = (ImageView) findViewById(R.id.progressBar);
                    final Animation progressAnimation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.progress);

                    progressAnimationHolder.clearAnimation();
                    progressAnimation.cancel();

                    nowRecordingText.setVisibility(View.GONE);
                    progressAnimationHolder.setVisibility(View.INVISIBLE);
                    progressBackground.setVisibility(View.INVISIBLE);
                    recordAnimation.stop();
                    recordAnimationHolder.setVisibility(View.INVISIBLE);
                    currentlyRecording = false;
                }
            }

            ImageView playPauseButton = (ImageView) findViewById(R.id.pauseButton);
            if (playPauseButton != null)
                playPauseButton.setImageResource(R.drawable.playback_play);

            if (soundPlayer != null) {

                if (playPauseButton != null)
                    playPauseButton.setImageResource(R.drawable.playback_pause);

                soundPlayer.pause();
                final Handler pauseIncrement1 = new Handler();
                pauseIncrement1.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            soundPlayer.pause();
                        } catch (Exception e){

                        }
                    }
                }, 100);

                final Handler pauseIncrement2 = new Handler();
                pauseIncrement2.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            soundPlayer.pause();
                        } catch (Exception e){

                        }
                    }
                }, 200);

                final Handler pauseIncrement3 = new Handler();
                pauseIncrement3.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            soundPlayer.pause();
                        } catch (Exception e){

                        }
                    }
                }, 400);
            }

            if (managerHandler != null) {
                managerHandler.removeCallbacks(managerRunnable);
            }
        }
    }

    public void initializeHomeButton() {
        final ImageView homeButton = (ImageView) findViewById(R.id.homeButton);

        final RelativeLayout homeConfirm = (RelativeLayout) findViewById(R.id.homeConfirm);
        homeConfirm.setVisibility(View.GONE);

        setButtonPressState(homeButton, R.drawable.home_button_assets, R.drawable.home_pressed);
        homeButton.setSoundEffectsEnabled(false);
        homeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!homeConfirmOpen && !deleteAllConfirmOpen && !deleteOneConfirmOpen) {
                    homeConfirmOpen = true;
                    allowHint = true;
                    if (soundRecorder != null) {
                        if(currentlyRecording) {
                            try{
                                soundRecorder.stop();
                                soundRecorder.release();
                                soundRecorder = null;
                            } catch (RuntimeException e) {
                                e.printStackTrace();
                                soundRecorder.reset();
                                soundRecorder.release();
                                soundRecorder = null;
                            }

                            autoPlayingButtonIntro = false;

                            final ImageView recordAnimationHolder = (ImageView) findViewById(R.id.pulseAnimation);
                            final AnimationDrawable recordAnimation = (AnimationDrawable) recordAnimationHolder.getDrawable();
                            final TextView nowRecordingText = (TextView) findViewById(R.id.nowRecordingText);
                            final ImageView progressBackground = (ImageView) findViewById(R.id.progressBackground);
                            final ImageView progressAnimationHolder = (ImageView) findViewById(R.id.progressBar);
                            final Animation progressAnimation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.progress);

                            progressAnimationHolder.clearAnimation();
                            progressAnimation.cancel();

                            nowRecordingText.setVisibility(View.GONE);
                            progressAnimationHolder.setVisibility(View.INVISIBLE);
                            progressBackground.setVisibility(View.INVISIBLE);
                            recordAnimation.stop();
                            recordAnimationHolder.setVisibility(View.INVISIBLE);
                            currentlyRecording = false;
                        }
                    }

                    ImageView playPauseButton = (ImageView) findViewById(R.id.pauseButton);
                    if (playPauseButton != null)
                        playPauseButton.setImageResource(R.drawable.playback_play);

                    if (soundPlayer != null) {

                        if (playPauseButton != null)
                            playPauseButton.setImageResource(R.drawable.playback_pause);

                        soundPlayer.pause();
                        final Handler pauseIncrement1 = new Handler();
                        pauseIncrement1.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                soundPlayer.pause();
                            }
                        }, 100);

                        final Handler pauseIncrement2 = new Handler();
                        pauseIncrement2.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                soundPlayer.pause();
                            }
                        }, 200);

                        final Handler pauseIncrement3 = new Handler();
                        pauseIncrement3.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                soundPlayer.pause();
                            }
                        }, 400);
                    }

                    playTwoSounds("home_icon_sfx", "menu_confirm1");
                    if (managerHandler != null) {
                        managerHandler.removeCallbacks(managerRunnable);
                    }


                    TextView confirmText = (TextView) findViewById(R.id.confirmText);
                    confirmText.setTypeface(neenoo);

                    homeConfirm.setVisibility(View.VISIBLE);




                    final ImageView confirmYes = (ImageView) findViewById(R.id.confirmYes);
                    final ImageView confirmNo = (ImageView) findViewById(R.id.confirmNo);

                    //setButtonPressState(confirmYes, R.drawable.home_accept, R.drawable.home_accept_pressed);
                    //setButtonPressState(confirmNo, R.drawable.home_cancel, R.drawable.home_cancel_pressed);
                    confirmYes.setOnTouchListener(new View.OnTouchListener() {
                        @Override
                        public boolean onTouch(View v, MotionEvent event) {
                            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                                confirmYes.setImageResource(R.drawable.home_accept_pressed);
                            }
                            if (event.getAction() == MotionEvent.ACTION_UP) {
                                confirmYes.setImageResource(R.drawable.home_accept);
                            }
                            return false;
                        }
                    });
                    confirmYes.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            _pauseButtonDisabled = false;
                            if (activityStarted)
                                logActivityExit(scoreForActivity);
                            if (soundPlayer != null) {
                                soundPlayer.release();
                                soundPlayer = null;
                            }
                            playSound("menu_confirm2");
                            homeConfirmOpen = false;
                            homeConfirm.setVisibility(View.GONE);
                            try {
                                // do something potentially risky
                                inRecMode = false;
                                okToPlayStory = false;
                                readyToRestart = false;
                                openMainMenu();
                            } catch (Exception e) {
                                Log.e("error: ", e.toString());
                            }
                            if (!_allRecorded){
                                Log.e("DELETING","whole run of files not finished, deleting "+_tempRecordingList.toString());
                                for (int i = 0; i < _tempRecordingList.size();i++){
                                    deleteTrack(_tempRecordingList.get(i));
                                }
                                _tempRecordingList.clear();
                            }
                        }
                    });
                    confirmNo.setOnTouchListener(new View.OnTouchListener() {
                        @Override
                        public boolean onTouch(View v, MotionEvent event) {
                            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                                confirmNo.setImageResource(R.drawable.home_cancel_pressed);
                            }
                            if (event.getAction() == MotionEvent.ACTION_UP) {
                                confirmNo.setImageResource(R.drawable.home_cancel);
                            }
                            return false;
                        }
                    });
                    confirmNo.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            homeConfirm.setVisibility(View.GONE);
                            playSound("menu_confirm3");
                            playAllPlaying = false;
                            ImageView playPauseButton = (ImageView) findViewById(R.id.pauseButton);
                            if (playPauseButton != null)
                                playPauseButton.setImageResource(R.drawable.playback_play);
                            if (playAllButton != null)
                                playAllButton.setImageResource(R.drawable.play_all);
                            if (soundPlayer != null && !playbackPaused) {
                                soundPlayer.start();
                                if (playPauseButton != null)
                                playPauseButton.setImageResource(R.drawable.playback_pause);
                            }
                            homeConfirmOpen = false;
                            // added to fix issue where audio won't restart if interrupted in intro
                            if (settingUpStoryPlayback) {
                                playTwoSoundsToSetupPlayback("playback", storyTitleSound);
                            }
                        }
                    });
                }
            }
        });
    }

    public void initializePlaybackControls() {
        final ImageView playPauseButton = (ImageView) findViewById(R.id.pauseButton);
        ImageView startoverButton = (ImageView) findViewById(R.id.startoverButton);

        playPauseButton.setAlpha(1.0f);

        //pauses or resumes playback
        playPauseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!_pauseButtonDisabled) {
                    if (!homeConfirmOpen) {
                        Log.e("pause", "play pause button pressed");
                        if (!pauseDebounce) {
                            if (soundPlayer != null) {
                                Log.e("pause", "soundplayer is not null");
                                if (soundPlayer.isPlaying()) {
                                    playSound("storyplayend_sfx");
                                    Log.e("pause", "soundplayer is playing");
                                    soundPlayer.pause();

                                    final Handler pauseIncrement1 = new Handler();
                                    pauseIncrement1.postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            soundPlayer.pause();
                                        }
                                    }, 100);

                                    final Handler pauseIncrement2 = new Handler();
                                    pauseIncrement2.postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            soundPlayer.pause();
                                        }
                                    }, 200);

                                    final Handler pauseIncrement3 = new Handler();
                                    pauseIncrement3.postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            soundPlayer.pause();
                                        }
                                    }, 400);

                                    playbackPaused = true;
                                    playPauseButton.setImageResource(R.drawable.playback_play);
                                } else {
                                    playSound("storyplaystart_sfx");
                                    soundPlayer.start();
                                    playbackPaused = false;
                                    playPauseButton.setImageResource(R.drawable.playback_pause);
                                }
                            }
                            pauseDebounce = true;

                            final Handler debounceHandler = new Handler();
                            debounceHandler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    pauseDebounce = false;
                                }
                            }, 400);
                        }
                    }
                }

            }
        });

        //takes you to recording area of same story
        startoverButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!homeConfirmOpen && readyToRestart) {
                    if(!replayDebounce) {
                        okToPlayStory = true;
                        final TextView storyText = (TextView) findViewById(R.id.storyText);
                        final TextView storyBlue = (TextView) findViewById(R.id.storyBlue);
                        final TextView storyBlack = (TextView) findViewById(R.id.storyBlack);
                        final TextView storyPurple = (TextView) findViewById(R.id.storyPurple);
                        final TextView storyWhite = (TextView) findViewById(R.id.storyWhite);
                        final TextView storyPink = (TextView) findViewById(R.id.storyPink);
                        storyText.setVisibility(View.VISIBLE);
                        storyBlue.setVisibility(View.VISIBLE);
                        storyBlack.setVisibility(View.VISIBLE);
                        storyPurple.setVisibility(View.VISIBLE);
                        storyWhite.setVisibility(View.VISIBLE);
                        storyPink.setVisibility(View.VISIBLE);
                        final ImageView theEnd = (ImageView) findViewById(R.id.theEnd);
                        theEnd.setVisibility(View.GONE);

                        playPauseButton.setAlpha(1.0f);
                        playSound("storyplaystart_sfx");
                        playbackPaused = false;
                        if (soundPlayer != null) {
                            soundPlayer.stop();
                            soundPlayer.reset();
                            soundPlayer.release();
                            soundPlayer = null;
                        }

                        playPauseButton.setImageResource(R.drawable.playback_pause);

                        blankIterator = 0;
                        audioIterator = 0;
                        phraseIterator = 0;
                        wasAboutToDoPlaySoundPair = true;
                        playSoundPair();

                        replayDebounce = true;

                        //newAudioFiles.clear();
                        repeatedAudio.clear();
                        isRepeatedAudioStock.clear();

                        final Handler debounceHandler = new Handler();
                        debounceHandler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                replayDebounce = false;
                            }
                        }, 800);
                    }
                }
            }
        });
    }

    public void setStoryTitle() {
        try {
            String pictureName = storyObj.getJSONObject("weeklyengagement").getString("image");
            String title = storyObj.getJSONObject("weeklyengagement").getString("title");

            ImageView storyPicture = (ImageView) findViewById(R.id.storyPicture);
            storyPicture.setImageResource(getResources().getIdentifier(pictureName, "drawable", getPackageName()));
            TextView storyIntro = (TextView) findViewById(R.id.storyIntro);
            storyIntro.setTypeface(neenoo);
            TextView storyTitle = (TextView) findViewById(R.id.storyName);
            storyTitle.setTypeface(neenoo);
            storyTitle.setText(title);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void getStoryJSON(String jsonfile) {
        Log.e("data", "selected story is "+jsonfile);

        //default
        String storyJSON = readFile("json/gamedata/"+jsonfile+".json");
        String blankJSON = readFile("json/gamedata/blank_data.json");

        try {
            blankObj = new JSONObject(blankJSON);

            storyObj = new JSONObject(storyJSON);
            storyArray = storyObj.getJSONObject("weeklyengagement").getJSONArray("story");

            findNumBlanks();

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void playSound(String sound1) {
        if(soundHelper1!=null){
            soundHelper1.stop();
            soundHelper1.release();
            soundHelper1 = null;
        }
        if(soundHelper2!=null){
            soundHelper2.stop();
            soundHelper2.release();
            soundHelper2 = null;
        }
        if(soundHelper3!=null){
            soundHelper3.stop();
            soundHelper3.release();
            soundHelper3 = null;
        }

        try {
            soundHelper1 = new MediaPlayer();

            soundHelper1.setDataSource(this, getAudio(sound1));
            soundHelper1.prepare();
            soundHelper1.setVolume(0.9f, 0.9f);

            soundHelper1.start();

            soundHelper1.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    soundHelper1.stop();
                    soundHelper1.release();
                    soundHelper1 = null;
                }
            });

        } catch (IOException e) {
            e.printStackTrace();
        }
        if(gameStopped) {
            soundHelper1.pause();
        }
    }

    public void playTwoSounds(String sound1, String sound2) {
        if(soundHelper1!=null){
            soundHelper1.stop();
            soundHelper1.release();
            soundHelper1 = null;
        }
        if(soundHelper2!=null){
            soundHelper2.stop();
            soundHelper2.release();
            soundHelper2 = null;
        }
        if(soundHelper3!=null){
            soundHelper3.stop();
            soundHelper3.release();
            soundHelper3 = null;
        }

        try {
            soundHelper1 = new MediaPlayer();
            soundHelper2 = new MediaPlayer();

            soundHelper1.setDataSource(this, getAudio(sound1));
            soundHelper1.prepare();
            soundHelper1.setVolume(0.9f, 0.9f);
            soundHelper2.setDataSource(this, getAudio(sound2));
            soundHelper2.prepare();
            soundHelper2.setVolume(0.9f, 0.9f);

            soundHelper1.start();

            soundHelper1.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    soundHelper1.stop();
                    soundHelper1.release();
                    soundHelper1 = null;
                    soundHelper2.start();
                }
            });

            soundHelper2.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    soundHelper2.stop();
                    soundHelper2.release();
                    soundHelper2 = null;
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
        if(gameStopped) {
            soundHelper1.pause();
        }
    }

    private boolean _pauseButtonDisabled = false;

    public void playTwoSoundsToSetupPlayback(String sound1, String sound2) {
        _pauseButtonDisabled = true;
        settingUpStoryPlayback = true;
        if(soundHelper1!=null){
            soundHelper1.stop();
            soundHelper1.release();
            soundHelper1 = null;
        }
        if(soundHelper2!=null){
            soundHelper2.stop();
            soundHelper2.release();
            soundHelper2 = null;
        }
        if(soundHelper3!=null){
            soundHelper3.stop();
            soundHelper3.release();
            soundHelper3 = null;
        }

        try {
            soundHelper1 = new MediaPlayer();
            soundHelper2 = new MediaPlayer();

            soundHelper1.setDataSource(this, getAudio(sound1));
            soundHelper1.prepare();
            soundHelper1.setVolume(0.9f, 0.9f);
            soundHelper2.setDataSource(this, getAudio(sound2));
            soundHelper2.prepare();
            soundHelper2.setVolume(0.9f, 0.9f);

            soundHelper1.start();

            soundHelper1.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    soundHelper1.stop();
                    soundHelper1.release();
                    soundHelper1 = null;
                    soundHelper2.start();
                }
            });

            soundHelper2.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {

                    Log.e("PLAYBACK", "playTwoSoundsToSetupPlayback reached onCompletion");

                    soundHelper2.stop();
                    soundHelper2.release();
                    soundHelper2 = null;

                    //start the playback
                    if(normalMode || madMode) {
                        okToPlayStory = true;
                        wasAboutToDoPlaySoundPair = true;
                        playSoundPair();
                    } else {
                        readyToRestart = true;
                        wasAboutToDoPlaySoundPair = true;
                        playSoundPair();
                    }
                    settingUpStoryPlayback = false;
                    _pauseButtonDisabled = false;

                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
        if(gameStopped) {
            soundHelper1.pause();
        }
    }

    public void playThreeSounds(String sound1, String sound2, String sound3) {
        if(soundHelper1!=null){
            soundHelper1.stop();
            soundHelper1.release();
            soundHelper1 = null;
        }
        if(soundHelper2!=null){
            soundHelper2.stop();
            soundHelper2.release();
            soundHelper2 = null;
        }
        if(soundHelper3!=null){
            soundHelper3.stop();
            soundHelper3.release();
            soundHelper3 = null;
        }

        try {
            soundHelper1 = new MediaPlayer();
            soundHelper2 = new MediaPlayer();
            soundHelper3 = new MediaPlayer();

            soundHelper1.setDataSource(this, getAudio(sound1));
            soundHelper1.prepare();
            soundHelper1.setVolume(0.9f, 0.9f);
            soundHelper2.setDataSource(this, getAudio(sound2));
            soundHelper2.prepare();
            soundHelper2.setVolume(0.9f, 0.9f);
            soundHelper3.setDataSource(this, getAudio(sound3));
            soundHelper3.prepare();
            soundHelper3.setVolume(0.9f, 0.9f);

            soundHelper1.start();

            soundHelper1.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    soundHelper1.stop();
                    soundHelper1.release();
                    soundHelper1 = null;
                    soundHelper2.start();
                }
            });

            soundHelper2.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    soundHelper2.stop();
                    soundHelper2.release();
                    soundHelper2 = null;
                    soundHelper3.start();
                }
            });

            soundHelper3.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    soundHelper3.stop();
                    soundHelper3.release();
                    soundHelper3 = null;
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
        if(gameStopped) {
            soundHelper1.pause();
        }
    }

    public void stopSoundHelper() {
        if(soundHelper1!=null){
            soundHelper1.stop();
            soundHelper1.release();
            soundHelper1 = null;
        }
        if(soundHelper2!=null){
            soundHelper2.stop();
            soundHelper2.release();
            soundHelper2 = null;
        }
        if(soundHelper3!=null){
            soundHelper3.stop();
            soundHelper3.release();
            soundHelper3 = null;
        }
    }

    public void playMusic(String music) {
        if(musicPlayer!=null){
            musicPlayer.stop();
            musicPlayer.release();
            musicPlayer = null;
        }

        try {
            musicPlayer = new MediaPlayer();
            musicPlayer.setDataSource(this, getAudio(music));
            musicPlayer.prepare();
            musicPlayer.setVolume(0.6f,0.6f);
            musicPlayer.setLooping(true);
            musicPlayer.start();

        } catch (IOException e) {
            e.printStackTrace();
        }
        if(gameStopped) {
            musicPlayer.pause();
        }
    }

    public void stopMusic() {
        if(musicPlayer!=null){
            musicPlayer.stop();
            musicPlayer.release();
            musicPlayer = null;
        }
    }

    public void playAudioManagerSound(String sound1, final ImageView button) {
        if(managerHandler!=null) {
            managerHandler.removeCallbacks(managerRunnable);
        }

        if(soundHelper1!=null){
            soundHelper1.stop();
            soundHelper1.release();
            soundHelper1 = null;
        }
        if(soundHelper2!=null){
            soundHelper2.stop();
            soundHelper2.release();
            soundHelper2 = null;
        }

        try {
            /////////////////////////////////////////////////////////////////////////////////////////
            // here we need to skip through audioGrid and then subLayout to figure out what is an image view - use setTag and getTag to specifically ID views; then set those ImageViews to the normal button (sound is already being stopped)
            //for(int i=0; i<subLayout.getChildCount(); ++i) {
            //subLayout.getChildAt(i).
            //}
            /////////////////////////////////////////////////////////////////////////////////////////

            soundHelper1 = new MediaPlayer();

            soundHelper1.setDataSource(fileNameTemplate+sound1+recordedFileType);
            soundHelper1.prepare();
            soundHelper1.start();
            button.setImageResource(R.drawable.audio_button_highlighted);

            soundHelper1.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    soundHelper1.stop();
                    soundHelper1.release();
                    soundHelper1 = null;

                }
            });

            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (button.getDrawable().getConstantState() == getResources().getDrawable( R.drawable.audio_button_highlighted).getConstantState()) {
                        button.setImageResource(R.drawable.audio_button);
                    }

                }
            }, 5000);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void hideHintArea() {
        hintOpen = false;
        ImageView hintBackground = (ImageView) findViewById(R.id.hintBackground);
        TextView hintText = (TextView) findViewById(R.id.hintWordType);
        LinearLayout hintLayout = (LinearLayout) findViewById(R.id.hintLayout);
        ImageView closeHint = (ImageView) findViewById(R.id.closeHint);

        hintBackground.setVisibility(View.GONE);
        hintText.setVisibility(View.GONE);
        hintLayout.setVisibility(View.GONE);
        closeHint.setVisibility(View.GONE);
    }

    public void showHintArea() {
        hintOpen = true;
        ImageView hintBackground = (ImageView) findViewById(R.id.hintBackground);
        TextView hintText = (TextView) findViewById(R.id.hintWordType);
        LinearLayout hintLayout = (LinearLayout) findViewById(R.id.hintLayout);
        ImageView closeHint = (ImageView) findViewById(R.id.closeHint);

        hintBackground.setVisibility(View.VISIBLE);
        hintText.setVisibility(View.VISIBLE);
        hintLayout.setVisibility(View.VISIBLE);
        closeHint.setVisibility(View.VISIBLE);
    }

    public void openAudioManager() {
        //getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.audio_manager);
        getWindow().getDecorView().setKeepScreenOn(false);
        Log.e("setKeepScreenOn",">>>>> false 5");

        stopMusic();
        initializeHomeButton();
        currentPage = 0;
        invisibleActive = false;

        final RelativeLayout deleteAllConfirm = (RelativeLayout) findViewById(R.id.deleteAllConfirm);
        deleteAllConfirm.setVisibility(View.GONE);
        final RelativeLayout deleteOneConfirm = (RelativeLayout) findViewById(R.id.deleteOneConfirm);
        deleteOneConfirm.setVisibility(View.GONE);
        TextView deleteAllText = (TextView) findViewById(R.id.deleteAllText);
        deleteAllText.setTypeface(neenoo);
        TextView deleteOneText = (TextView) findViewById(R.id.deleteOneText);
        deleteOneText.setTypeface(neenoo);

        drawAudioGrid();

        playAllButton = (ImageView) findViewById(R.id.playAllButton);
        //setButtonPressState(playAllButton, R.drawable.play_all, R.drawable.play_all_pressed);
        if(playAllEmpty) {
            playAllButton.setAlpha(0.5f);
        } else {
            playAllButton.setAlpha(1.0f);
        }
        playAllButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!homeConfirmOpen && !deleteAllConfirmOpen && !deleteOneConfirmOpen) {
                    if(!playAllEmpty) {
                        Log.e("playAll", "play all button pressed");

                        if (managerHandler != null) {
                            managerHandler.removeCallbacks(managerRunnable);
                        }

                        if (playAllPlaying) {
                            playAllButton.setImageResource(R.drawable.play_all);
                            playAllPlaying = false;
                            getWindow().getDecorView().setKeepScreenOn(false);
                            Log.e("setKeepScreenOn",">>>>> false 6");
                            audioCounter = 0;
                        } else {
                            playAllPlaying = true;
                            getWindow().getDecorView().setKeepScreenOn(true);
                            Log.e("setKeepScreenOn", ">>>>> true 3");

                            playAllButton.setImageResource(R.drawable.play_all_pressed);

                            audioGrid = (GridLayout) findViewById(R.id.audioGrid);

                            audioCounter = 0;

                            managerHandler = new Handler();
                            managerRunnable.run();
                        }
                    }
                }
            }
        });

        final ImageView deleteAll = (ImageView) findViewById(R.id.deleteAllButton);
        setButtonPressState(deleteAll, R.drawable.delete_all, R.drawable.delete_all_pressed);
        if(playAllEmpty) {
            deleteAll.setAlpha(0.5f);
        } else {
            deleteAll.setAlpha(1.0f);
        }
        deleteAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!homeConfirmOpen && !deleteOneConfirmOpen && !deleteAllConfirmOpen) {
                    if(!playAllEmpty) {
                        deleteAllConfirmOpen = true;

                        playTwoSounds("confirm_sfx", "delete_confirm1");

                        if (playAllPlaying) {
                            playAllButton.setImageResource(R.drawable.play_all);
                            playAllPlaying = false;
                            getWindow().getDecorView().setKeepScreenOn(false);
                            Log.e("setKeepScreenOn",">>>>> false 7");
                            audioCounter = 0;
                        }

                        deleteAllConfirm.setVisibility(View.VISIBLE);

                        final ImageView deleteAllYes = (ImageView) findViewById(R.id.deleteAllYes);
                        //setButtonPressState(deleteAllYes, R.drawable.home_accept, R.drawable.home_accept_pressed);
                        deleteAllYes.setOnTouchListener(new View.OnTouchListener() {
                            @Override
                            public boolean onTouch(View v, MotionEvent event) {
                                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                                    deleteAllYes.setImageResource(R.drawable.home_accept_pressed);
                                }
                                if (event.getAction() == MotionEvent.ACTION_UP) {
                                    deleteAllYes.setImageResource(R.drawable.home_accept);
                                }
                                return false;
                            }
                        });
                        deleteAllYes.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                playTwoSounds("menu_confirm2", "delete_sfx");
                                deleteAllConfirm.setVisibility(View.GONE);

                                try{
                                    Log.e("DELETE","chosen to delete all");
                                    for (int i = 1; i < 13;i++) {
                                        JSONArray arr = audioManagerObject.getJSONArray(""+i);
                                        Log.e("DELETING",arr.toString(3));
                                        if (arr.length()>0){
                                            for (int j = 0; j < arr.length();j++){
                                                deleteTrack(arr.getString(j));
                                            }
                                        }
                                    }
                                } catch(JSONException e){
                                    Log.e("DELETING","Error reading audioManagerObject");
                                }

                                resetAudioList();
                                currentPage = 0;
                                drawAudioGrid();
                                deleteAllConfirmOpen = false;
                                playAllEmpty = true;
                                playAllButton.setAlpha(0.5f);
                                deleteAll.setAlpha(0.5f);
                            }
                        });

                        final ImageView deleteAllNo = (ImageView) findViewById(R.id.deleteAllNo);
                        //setButtonPressState(deleteAllNo, R.drawable.home_cancel, R.drawable.home_cancel_pressed);
                        deleteAllNo.setOnTouchListener(new View.OnTouchListener() {
                            @Override
                            public boolean onTouch(View v, MotionEvent event) {
                                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                                    deleteAllNo.setImageResource(R.drawable.home_cancel_pressed);
                                }
                                if (event.getAction() == MotionEvent.ACTION_UP) {
                                    deleteAllNo.setImageResource(R.drawable.home_cancel);
                                }
                                return false;
                            }
                        });
                        deleteAllNo.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                playSound("menu_confirm3");
                                deleteAllConfirm.setVisibility(View.GONE);
                                deleteAllConfirmOpen = false;
                            }
                        });

                        if (managerHandler != null) {
                            managerHandler.removeCallbacks(managerRunnable);
                        }
                    }
                }
            }
        });

        TextView storyToggle = (TextView) findViewById(R.id.version);
        storyToggle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (invisibleActive) {
                    //if (getPropFromSetProp("persist.lf.content_cheats").equals("1") || getPropFromSetProp("persist.lf.content_cheats").equals("")) {
                    if (getPropFromSetProp("persist.lf.content_cheats").equals("1")) {
                        Intent intent = new Intent("com.leapfrog.action.WEEKLY_ENGAGEMENT");
                        intent.putExtra("com.leapfrog.extra.WEEKLY_ENGAGEMENT_STATE", true);
                        sendBroadcast(intent);

                        InstanceChecker.getInstance().artificialWeek();
                        InstanceChecker.getInstance().setNewWeek(true);
                        storyIndex = InstanceChecker.getInstance().getWeekNumber();

                        Log.e("story", "new story is index " + storyIndex);
                    }
                }
            }
        });

        TextView invisibleButton = (TextView) findViewById(R.id.invisibleButton);
        invisibleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                invisibleActive = true;
            }
        });

    }

    public void drawAudioGrid() {
        String audioList = readAudioList();
        try {
            audioManagerObject = new JSONObject(audioList);
        } catch (JSONException e) {
            e.printStackTrace();
            audioManagerObject = new JSONObject();
        }

        totalSounds = 0;

        audioGrid = (GridLayout) findViewById(R.id.audioGrid);
        audioGrid.removeAllViews();

        for(int i=1; i<13; i++) {
            try {
                for(int j=0; j<audioManagerObject.getJSONArray(""+i).length(); j++) {
                    totalSounds++;
                    if(currentPage*iconsPerPage<totalSounds && totalSounds <=(currentPage+1)*iconsPerPage) {
                        int row = (int) Math.floor((totalSounds-1)/5);
                        int column = (int) (totalSounds-1)%5;

                        RelativeLayout subLayout = new RelativeLayout(this);
                        audioGrid.addView(subLayout);
                        subLayout.setLayoutParams(new GridLayout.LayoutParams(GridLayout.spec(row), GridLayout.spec(column)));

                        ImageView thisAudioButton = new ImageView(this);
                        thisAudioButton.setImageResource(R.drawable.audio_button);
                        thisAudioButton.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                        subLayout.addView(thisAudioButton);
                        thisAudioButton.setSoundEffectsEnabled(false);

                        ImageView thisCloseButton = new ImageView(this);
                        thisCloseButton.setImageResource(R.drawable.close_button);
                        thisCloseButton.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                        thisCloseButton.setPadding(50,0,0,0);
                        subLayout.addView(thisCloseButton);

                        addSoundToAudioManagerButton(thisAudioButton, i, j);
                        activateSoundCloseButton(thisCloseButton, thisAudioButton, i, j);

                        for(int k=0; k<audioManagerToDelete.size(); k++) {
                            int[] temp = audioManagerToDelete.get(k);
                            if(temp[0] == i && temp[1] == j) {
                                thisCloseButton.setVisibility(View.GONE);
                                thisAudioButton.setOnClickListener(null);
                                thisAudioButton.setImageResource(R.drawable.audio_button_disabled);
                            }
                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        Log.e("audio", "found " + totalSounds + " sounds in data");

        TextView amPageTracker = (TextView) findViewById(R.id.amPageTracker);
        amPageTracker.setText("Page " + (currentPage + 1));
        amPageTracker.setTypeface(neenoo);

        final ImageView previousButton = (ImageView) findViewById(R.id.previousButton);
        if(currentPage == 0) {
            previousButton.setVisibility(View.GONE);
        } else {
            previousButton.setVisibility(View.VISIBLE);
        }

        final ImageView nextButton = (ImageView) findViewById(R.id.nextButton);
        if((currentPage+1)*iconsPerPage>=totalSounds) {
            nextButton.setVisibility(View.GONE);
        } else {
            nextButton.setVisibility(View.VISIBLE);
        }

        if(totalSounds > 0) {
            playAllEmpty = false;
        } else {
            playAllEmpty = true;
        }

        setButtonPressState(nextButton, R.drawable.arrow_right, R.drawable.arrow_right_pressed);
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!homeConfirmOpen && !deleteAllConfirmOpen && !deleteOneConfirmOpen) {
                    if (playAllPlaying) {
                        playAllButton.setImageResource(R.drawable.play_all);
                        playAllPlaying = false;
                        getWindow().getDecorView().setKeepScreenOn(false);
                        Log.e("setKeepScreenOn",">>>>> false 8");
                        audioCounter = 0;
                    }
                    if (managerHandler != null) {
                        managerHandler.removeCallbacks(managerRunnable);
                    }
                    if((currentPage+1)*iconsPerPage<totalSounds) {
                        playSound("nextpage_sfx");
                        currentPage++;
                        drawAudioGrid();
                    } else {
                        nextButton.setVisibility(View.GONE);
                    }
                }
            }
        });

        setButtonPressState(previousButton, R.drawable.arrow_left, R.drawable.arrow_left_pressed);
        previousButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!homeConfirmOpen && !deleteAllConfirmOpen && !deleteOneConfirmOpen) {
                    if (playAllPlaying) {
                        playAllButton.setImageResource(R.drawable.play_all);
                        playAllPlaying = false;
                        getWindow().getDecorView().setKeepScreenOn(false);
                        Log.e("setKeepScreenOn",">>>>> false 9");
                        audioCounter = 0;
                    }
                    if (managerHandler != null) {
                        managerHandler.removeCallbacks(managerRunnable);
                    }
                    if(currentPage > 0) {
                        playSound("nextpage_sfx");
                        currentPage--;
                        drawAudioGrid();
                    } else {
                        previousButton.setVisibility(View.GONE);
                    }
                }
            }
        });
    }

    public void addSoundToAudioManagerButton(View button, int i, int j) {
        final String sound;
        try {
            sound = (String) audioManagerObject.getJSONArray(""+i).getString(j);

            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(!deleteOneConfirmOpen && !deleteAllConfirmOpen && !homeConfirmOpen) {
                        for(int k=0; k<audioGrid.getChildCount(); k++) {
                            ViewGroup child = (ViewGroup)audioGrid.getChildAt(k);
                            ImageView childbutton = (ImageView)child.getChildAt(0);
                            if (childbutton.getDrawable().getConstantState() == getResources().getDrawable( R.drawable.audio_button_highlighted).getConstantState()) {
                                childbutton.setImageResource(R.drawable.audio_button);
                            }
                        }
                        playAudioManagerSound(sound, (ImageView) v);
                        if (playAllPlaying) {
                            playAllButton.setImageResource(R.drawable.play_all);
                            playAllPlaying = false;
                            audioCounter = 0;
                        }
                    }
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void activateSoundCloseButton(final ImageView closeButton, final ImageView audioButton, int i, int j) {
        final int audioType = i;
        final int audioIndex = j;

        final RelativeLayout deleteOneConfirm = (RelativeLayout) findViewById(R.id.deleteOneConfirm);

        setButtonPressState((ImageView) closeButton, R.drawable.close_button, R.drawable.close_button_pressed);
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!deleteOneConfirmOpen && !deleteAllConfirmOpen && !homeConfirmOpen){
                    deleteOneConfirmOpen = true;
                    Log.e("close", "button to delete sound "+audioType+" "+audioIndex+" has been pressed");

                    if(managerHandler!=null) {
                        managerHandler.removeCallbacks(managerRunnable);
                    }
                    playTwoSounds("confirm_sfx", "delete_confirm2");

                    deleteOneConfirm.setVisibility(View.VISIBLE);
                    final ImageView deleteOneYes = (ImageView) findViewById(R.id.deleteOneYes);
                    final ImageView deleteOneNo = (ImageView) findViewById(R.id.deleteOneNo);
                    //setButtonPressState(deleteOneYes, R.drawable.home_accept, R.drawable.home_accept_pressed);
                    //setButtonPressState(deleteOneNo, R.drawable.home_cancel, R.drawable.home_cancel_pressed);
                    deleteOneYes.setOnTouchListener(new View.OnTouchListener() {
                        @Override
                        public boolean onTouch(View v, MotionEvent event) {
                            if(event.getAction() == MotionEvent.ACTION_DOWN) {
                                deleteOneYes.setImageResource(R.drawable.home_accept_pressed);
                            }
                            if(event.getAction() == MotionEvent.ACTION_UP) {
                                deleteOneYes.setImageResource(R.drawable.home_accept);
                            }
                            return false;
                        }
                    });
                    deleteOneYes.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            try {
                                Log.e("DELETING","from object "+audioManagerObject.toString(3));
                                Log.e("DELETING",audioManagerObject.getJSONArray("" + audioType).toString(3)+" index "+audioIndex);
                                deleteTrack(audioManagerObject.getJSONArray("" + audioType).getString(audioIndex));
                                audioManagerObject.getJSONArray("" + audioType).remove(audioIndex);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            playTwoSounds("menu_confirm2", "delete_sfx");
                            deleteOneConfirm.setVisibility(View.GONE);
                            //deleteOneAudioListFile(audioType, audioIndex);
                            int[] temp = new int[2];
                            temp[0] = audioType;
                            temp[1] = audioIndex;
                            audioManagerToDelete.add(temp);
                            closeButton.setVisibility(View.GONE);
                            audioButton.setOnClickListener(null);
                            audioButton.setImageResource(R.drawable.audio_button_disabled);
                            deleteOneConfirmOpen = false;
                        }
                    });
                    deleteOneNo.setOnTouchListener(new View.OnTouchListener() {
                        @Override
                        public boolean onTouch(View v, MotionEvent event) {
                            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                                deleteOneNo.setImageResource(R.drawable.home_cancel_pressed);
                            }
                            if (event.getAction() == MotionEvent.ACTION_UP) {
                                deleteOneNo.setImageResource(R.drawable.home_cancel);
                            }
                            return false;
                        }
                    });
                    deleteOneNo.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            playSound("menu_confirm3");
                            deleteOneConfirm.setVisibility(View.GONE);
                            deleteOneConfirmOpen = false;
                        }
                    });
                }
            }
        });
    }

    public void resetAudioList() {
        try {
            Log.e("READWRITE","writing empty list to audio_list.txt");
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(openFileOutput("audio_list.txt", Context.MODE_PRIVATE));
            outputStreamWriter.write("{'1': [],'2': [],'3': [],'4': [],'5': [],'6': [],'7': [],'8': [],'9': [],'10': [],'11': [],'12': []}");
            outputStreamWriter.close();
        }
        catch (IOException e) {
            Log.e("Exception", "File write failed: " + e.toString());
        }
    }

    //get current audio list and append arraylist of new sounds
    public void writeAudioList(ArrayList<ArrayList<String>> data) {
        String existingData = readAudioList();
        JSONObject existingDataObject;
        JSONArray existingDataArray = null;

        String dataToWrite = "";

        try {
            existingDataObject = new JSONObject(existingData);

            Log.e("read", "existingData = "+existingData);

            for(int j=1; j<13; j++) {
                for(int i=0; i<data.get(j-1).size(); i++) {
                    existingDataObject.getJSONArray(""+j).put(data.get(j - 1).get(i));
                }
            }


            //existingDataObject.put("audio", existingDataArray);
            dataToWrite = existingDataObject.toString();

        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            Log.e("READWRITE","writing to audio_list.txt "+dataToWrite);
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(openFileOutput("audio_list.txt", Context.MODE_PRIVATE));
            outputStreamWriter.write(dataToWrite);
            outputStreamWriter.close();
        }
        catch (IOException e) {
            Log.e("Exception", "File write failed: " + e.toString());
        }

    }

    public String readAudioList() {
        String ret = "";

        try {

            InputStream inputStream = openFileInput("audio_list.txt");

            if ( inputStream != null ) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();

                while ( (receiveString = bufferedReader.readLine()) != null ) {
                    stringBuilder.append(receiveString);
                }

                inputStream.close();
                ret = stringBuilder.toString();
                Log.e("READWRITE","reading in audio_list.txt "+ret);
            } else {
                ret = "{'1': [],'2': [],'3': [],'4': [],'5': [],'6': [],'7': [],'8': [],'9': [],'10': [],'11': [],'12': []}";
            }
        }
        catch (FileNotFoundException e) {
            Log.e("login activity", "File not found: " + e.toString());
        } catch (IOException e) {
            Log.e("login activity", "Can not read file: " + e.toString());
        }

        if(ret.equals("")) {
            ret = "{'1': [],'2': [],'3': [],'4': [],'5': [],'6': [],'7': [],'8': [],'9': [],'10': [],'11': [],'12': []}";
        }

        Log.e("read", "i read "+ret);

        return ret;
    }

    public void deleteOneAudioListFile(int i, int j) {
        String currentAudioList = readAudioList();
        Log.e("AUDIO LIST",currentAudioList);
        try {
            JSONObject audioObject = new JSONObject(currentAudioList);
            audioObject.getJSONArray(""+i).remove(j);

            Log.e("READWRITE","writing to audio_list.txt "+audioObject.toString());
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(openFileOutput("audio_list.txt", Context.MODE_PRIVATE));
            outputStreamWriter.write(audioObject.toString());
            outputStreamWriter.close();
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (IOException e) {
            Log.e("Exception", "File write failed: " + e.toString());
        }
    }

    public void setButtonPressState(final ImageView view, final int upImage, final int downImage) {
        view.setSoundEffectsEnabled(false);
        view.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(!homeConfirmOpen && !deleteAllConfirmOpen && !deleteOneConfirmOpen && !delayRecord) {
                    if(event.getAction() == MotionEvent.ACTION_DOWN) {
                        view.setImageResource(downImage);
                    }
                    if(event.getAction() == MotionEvent.ACTION_UP) {
                        view.setImageResource(upImage);
                    }
                }
                return false;
            }
        });
    }

    public String generateFileName() {
        Calendar rightNow = Calendar.getInstance();
        String year = ""+rightNow.get(Calendar.YEAR);
        String month = ""+rightNow.get(Calendar.MONTH);
        String day = ""+rightNow.get(Calendar.DAY_OF_WEEK);
        String hour = ""+rightNow.get(Calendar.HOUR_OF_DAY);
        String minute = ""+rightNow.get(Calendar.MINUTE);
        String second = ""+rightNow.get(Calendar.SECOND);
        String milli = ""+rightNow.get(Calendar.MILLISECOND);

        return year+month+day+hour+minute+second+milli;
    }

    public void populateStoryManifest() {
        storyManifest = new String[26];
        storyManifest[0] = "ss_game_001";
        storyManifest[1] = "ss_game_002";
        storyManifest[2] = "ss_game_003";
        storyManifest[3] = "ss_game_004";
        storyManifest[4] = "ss_game_005";
        storyManifest[5] = "ss_game_006";
        storyManifest[6] = "ss_game_007";
        storyManifest[7] = "ss_game_008";
        storyManifest[8] = "ss_game_009";
        storyManifest[9] = "ss_game_010";
        storyManifest[10] = "ss_game_011";
        storyManifest[11] = "ss_game_012";
        storyManifest[12] = "ss_game_013";
        storyManifest[13] = "ss_game_014";
        storyManifest[14] = "ss_game_015";
        storyManifest[15] = "ss_game_016";
        storyManifest[16] = "ss_game_017";
        storyManifest[17] = "ss_game_018";
        storyManifest[18] = "ss_game_019";
        storyManifest[19] = "ss_game_020";
        storyManifest[20] = "ss_game_021";
        storyManifest[21] = "ss_game_022";
        storyManifest[22] = "ss_game_023";
        storyManifest[23] = "ss_game_024";
        storyManifest[24] = "ss_game_025";
        storyManifest[25] = "ss_game_026";
    }

    public static String getPropFromSetProp(String key) {
        String ret= "";
        try {
            Class SystemProperties = null;
            SystemProperties = Class.forName("android.os.SystemProperties");
            Method method = SystemProperties.getDeclaredMethod("get", String.class);
            ret = (String) method.invoke(null, key);
        } catch (Exception e) {
            e.printStackTrace();
        }
        Log.e("PROPERTIES","ret= "+ret);
        return ret;
    }

}
