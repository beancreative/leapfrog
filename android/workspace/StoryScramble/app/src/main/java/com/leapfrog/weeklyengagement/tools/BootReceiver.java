package com.leapfrog.weeklyengagement.tools;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.util.Log;

/**
 * Created by jordanpridgen on 6/24/15.
 */
public class BootReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        if(Intent.ACTION_BOOT_COMPLETED.equals(intent.getAction())) {
            // do your thing here
            Log.e("BOOTED", "story scramble booted");
            //MediaPlayer.create(context, R.raw.gameplay_instructions11).start();

            String allIDs = context.getSharedPreferences(InstanceChecker.ALL_IDS, 0).getString(InstanceChecker.ALL_IDS,"0");
            final String[] idList = allIDs.split(",");
            Log.e("LISTING PROFS","Profs:"+idList.length);


            for (int i = 0; i < idList.length;i++){
                SharedPreferences settings = context.getSharedPreferences(InstanceChecker.PREFS_NAME + idList[i], 0);
                boolean newThisWeek = settings.getBoolean(InstanceChecker.NEW_THIS_WEEK,true);

                if (!newThisWeek){
                    InstanceChecker.updateIfNeeded(Integer.decode(idList[i]),context);
                }
            }

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Log.e("BOOT","happened for story scramble");
                }
            },10000);


        }
    }
}
