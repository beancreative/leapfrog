package com.leapfrog.weeklyengagement.tools;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.RemoteException;
import android.util.Log;
import android.widget.Toast;

/**
 * Created by jordanpridgen on 4/7/15.
 */
public class AlarmReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context arg0, Intent arg1) {
        // For our recurring task, we'll just display a message
        //Toast.makeText(arg0, "WEEKLY ENGAGEMENT STATE CHANGED", Toast.LENGTH_SHORT).show();

        Log.e("INTENT FIRING", "THIS_APP:" + InstanceChecker.THIS_APP + " THIS_APP_STATE:" + InstanceChecker.THIS_APP_STATE + ",true for ID:" + Integer.toString(arg1.getIntExtra("com.leapfrog.extra.profileId", -1)));

        Intent intent = new Intent(InstanceChecker.THIS_APP);
        //intent.setAction("com.leapfrog.profileIntent" + Integer.toString(arg1.getIntExtra("com.leapfrog.extra.profileId", -1)));
        intent.putExtra(InstanceChecker.THIS_APP_STATE, true);
        intent.putExtra("com.leapfrog.extra.profileId", arg1.getIntExtra("com.leapfrog.extra.profileId", -1));

        InstanceChecker.setSpecificNewWeek(arg1.getIntExtra("com.leapfrog.extra.profileId", -1), arg0);

        try {
            arg0.sendBroadcast(intent);
            Log.e("INTENT","TRUE fired on " + arg0);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}