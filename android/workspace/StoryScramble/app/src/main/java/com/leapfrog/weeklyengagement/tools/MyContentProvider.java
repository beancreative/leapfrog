package com.leapfrog.weeklyengagement.tools;

import android.content.ComponentName;
import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;

import com.leapfrog.contentprofile.IProfileService;
import com.leapfrog.contentprofile.ProfileInfo;

import java.util.List;


public class MyContentProvider extends ContentProvider {

    private static final String AUTHORITY = "com.leapfrog.weeklyengagement.provider";
    public static final int TUTORIALS = 100;
    public static final int TUTORIAL_ID = 110;

    private static final String TUTORIALS_BASE_PATH = "engagement";
    public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY
            + "/" + TUTORIALS_BASE_PATH);

    public static final String ID = "id";
    public static final String STATE = "state";

    private static IProfileService pService;

    private static ServiceConnection pConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {

            try {
                Log.e("CPSERVICE", "service connected");
                pService = IProfileService.Stub.asInterface(service);


            } catch (Exception e){
                Log.e("ERROR",e.getMessage());
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            Log.e("SERVICE ERROR","Service Disconnected");
        }

    };

    private void updateConnection(){
        try {
           // Log.e("ACTIVE PROFILE",Integer.toString(pService.getActiveProfile()));

            pService = InstanceChecker.getInstance().getService();
            int active = pService.getActiveProfile();
            if (active != 0) {

                //Log.e("CONTENT SERVICE TEST", pService.getProfileName(pService.getActiveProfile()));
                List<ProfileInfo> profiles = pService.getAllProfiles();

                String fullString = "";

                for (int i = 0; i < profiles.size(); i++) {
                    ProfileInfo prof = profiles.get(i);
                    fullString = fullString + prof.getProfileId();

                    if (i != profiles.size() - 1) {
                        fullString = fullString + ",";
                    }
                }

                Log.e("CONTENTPROVIDER","all users"+fullString);

                SharedPreferences allUsers = getContext().getSharedPreferences(InstanceChecker.ALL_IDS, 0);
                SharedPreferences.Editor editor = allUsers.edit();
                editor.putString(InstanceChecker.ALL_IDS, fullString).commit();
            }


        } catch (RemoteException e){
            Log.e("SERVICE ERROR",e.getMessage());
        }
    }


//	public static final String CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE
//	        + "/mt-tutorial";
//	public static final String CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE
//	        + "/mt-tutorial";

    private static final UriMatcher sURIMatcher = new UriMatcher(
            UriMatcher.NO_MATCH);
    static {
        sURIMatcher.addURI(AUTHORITY, TUTORIALS_BASE_PATH, TUTORIALS);
        sURIMatcher.addURI(AUTHORITY, TUTORIALS_BASE_PATH + "/#", TUTORIAL_ID);
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public String getType(Uri uri) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public boolean onCreate() {

        InstanceChecker.create(getContext());

       /* Context context = getContext();
        Log.e("CONTENTPROVIDER","created");

        Intent serviceIntent = new Intent();
        serviceIntent.setClassName("com.leapfrog.contentprofile",
                "com.leapfrog.contentprofile.ProfileService");
        try {
            if (!context.bindService(serviceIntent, pConnection, Context.BIND_AUTO_CREATE)) {
                StringBuilder sb = new StringBuilder(128).append("bindService(")
                        .append(IProfileService.class.getName())
                        .append(") FAILED!!!\n");
            }
        } catch (SecurityException se) {
            se.printStackTrace();
        }
        Log.e("CONTENTPROVIDER","bindService called");*/

        //updateConnection();

        return true;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection,
                        String[] selectionArgs, String sortOrder) {
        //Log.d("CORE", "HERERERER");
        String[] cols = {"id","state"};
        if (InstanceChecker.isReady())
            updateConnection();

        String allIDs = getContext().getSharedPreferences(InstanceChecker.ALL_IDS,0).getString(InstanceChecker.ALL_IDS,"none");
        String[] idList = allIDs.split(",");
        Log.e("LISTING PROFS","Profs:"+idList.length);

        MatrixCursor curs;
        if (allIDs != "none")
            curs= new MatrixCursor(cols,idList.length);
        else
            curs = new MatrixCursor(cols,0);

        if (allIDs != "none") {
            for (int i = 0; i < idList.length; i++) {
                if (idList[i]!= "") {
                    MatrixCursor.RowBuilder builder = curs.newRow();
                    builder.add("id", Integer.parseInt(idList[i]));

                    SharedPreferences settings = getContext().getSharedPreferences(InstanceChecker.PREFS_NAME + idList[i], 0);
                    boolean newThisWeek = settings.getBoolean(InstanceChecker.NEW_THIS_WEEK, true);
                    builder.add("state", newThisWeek ? 1 : 0);
                }
            }
        }

        return curs;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection,
                      String[] selectionArgs) {
        // TODO Auto-generated method stub
        return 0;
    }



}