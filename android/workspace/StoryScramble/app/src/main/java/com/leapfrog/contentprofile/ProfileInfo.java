/* version="2015.04.14.13.07"
 *****************************************************************************************
 * Descriptions:
 * This file encapsulates the profile basic information, constants of intent actions and 
 * extra parameters, predefined profile permission names, and profile error codes. These
 * profile related information will be communicated between Profile Service and those
 * clients which bind to Profile Service.
 *
 * Changes:
 *  2015-04-10 Joseph Chang
 *             1. Remove the intent constant of ACTION_CHANGE_LAUNCHER_MODE and its
 *                related extra parameters.
 *  2015-01-23 Joseph Chang
 *             1. Add the predefined permission of "showDemoGames".
 *  2015-01-12 Joseph Chang
 *             1. Add ERR_INTERNAL_DATABASE_ERROR error code.
 *  2014-12-23 Joseph Chang
 *             1. Correct compilation error.
 *             2. Update the constants of broadcasted intent actions and extra parameters.
 *  2014-12-22 Joseph Chang
 *             1. Add the constants of broadcasted intent actions and extra parameters.
 *             2. Add the predefined profile permission names.
 *             3. Add the profile error codes returned by Profile Service.
 *  2014-12-19 Joseph Chang
 *             Created.
 ****************************************************************************************/

package com.leapfrog.contentprofile;

import android.os.Parcel;
import android.os.Parcelable;


//////////////////////////////////////////////////////////////////////////////////////////
//
// ProfileInfo:
// Define the profile basic information, constants of intent actions and extra parameters,
// profile permissions, and profile error codes.
//
public class ProfileInfo implements Parcelable
{
    /**
     * IntentConst class defines constant of actions, extra parameters and their possible
     * values used by ProfileService.
     */
    public static class IntentConst {

        /**
         * Broadcasted intent on new profile creation or existing profile deletion.
         * It will carry 2 extra parameters: 
         *      EXTRA_PROFILE_ID: an integer greater than 0 to specify which profile is created/deleted.
         *      EXTRA_PROFILE_OPERATION: a string of either EXTRA_PROFILE_OP_CREATED or EXTRA_PROFILE_OP_DELETED
         *                  to indicate the specified profile is just newly created or deleted.
         */
        public static final String ACTION_MODIFY_CHILD_PROFILE = "com.leapfrog.action.MODIFY_CHILD_PROFILE";

        /**
         *  The intent that Kids mode launcher should broadcast to change active profile system-wide on
         *     1. switching profiles
         *     2. launched after device reboot
         *     3. on profile auto logged out after idle a certain interval
         * It will carry one extra parameter: 
         *      EXTRA_PROFILE_ID: 
         *          -1: parent profile
         *           0: no-active-profile (no profile is currently logged in)
         *          >0: child profile
         * e.g.:
         * {
         * 	Intent intent = new Intent(ACTION_CHANGE_ACTIVE_PROFILE);
         * 	// id is of integer type, where
         *  //    -1 for parent profile,
         *  //    0 for no-profile,
         *  //    and >0 for  currently/newly logged in child profile ID
         * 	intent.putExtra(EXTRA_PROFILE_ID, id);
         * 	braodcastIntent(intent);
         * }
         */
        public static final String ACTION_CHANGE_ACTIVE_PROFILE = "com.leapfrog.action.CHANGE_ACTIVE_PROFILE";

		/**
         * Broadcasted intent on modifying profile data.
		 * Currently only update to Nickname will trigger this, 
		 * and the removal/deletion of child profile will not trigger this, either.
         * It will carry 1 extra parameter: 
         *      EXTRA_PROFILE_ID: an integer greater than 0 to specify which profile has been updated.
         */
        public static final String ACTION_CHILD_PROFILE_CHANGED = "com.leapfrog.action.CHILD_PROFILE_CHANGED";

        /**
         * Extra integer parameter carried in the broadcasted actions to specify the concerned profile-ID.
         * A value of -1 indicates parent profile
         * A value of 0 indicates no-active-profile (no profile is currently logged in)
         * A positive value (greater than 0) indicates a child profile
         */
        public static final String EXTRA_PROFILE_ID = "com.leapfrog.extra.profileId";
		
        /**
         * Extra string parameter in the broadcasted ACTION_MODIFY_CHILD_PROFILE intent
         * to indicate the specified profile is just newly created or deleted.
         * the recognized string is eitherEXTRA_PROFILE_OP_CREATED or EXTRA_PROFILE_OP_DELETED. 
         */
        public static final String EXTRA_PROFILE_OPERATION = "com.leapfrog.extra.profile.operation";
		
        /**
         * Constant string for EXTRA_PROFILE_OPERATION extra parameter in ACTION_MODIFY_CHILD_PROFILE intent.
         * Its value is "created" (case-sensitive).
         */
        public static final String EXTRA_PROFILE_OP_CREATED = "created";
	
        /**
         * Constant string for EXTRA_PROFILE_OPERATION extra parameter in ACTION_MODIFY_CHILD_PROFILE intent.
         * Its value is "deleted" (case-sensitive).
         */
        public static final String EXTRA_PROFILE_OP_DELETED = "deleted";
    }

    /**
     * ProfileError:
     * This calss defines the error code of content profile.
     */
    public static class ProfileError {
	    // Content profile errors.
	    /**
	     * Cannot insert a row into database while creating a new profile.
	     */
	    public static final int ERR_INTERNAL_DATABASE_ERROR = -1;

	    /**
	     * There are already 3 profiles while creating a new profile.
	     */
	    public static final int ERR_TOO_MANY_PROFILES = -2;

	    /**
	     * The specified profile id is not existed/available.
	     */
	    public static final int ERR_PROFILE_ID_INVALID = -3;

	    /**
	     * Failed to create/update a profile due to non-unique name.
	     */
	    public static final int ERR_NAME_IS_NOT_UNIQUE = -4;

	    /**
	     * Failed to update a profile due to non-unique name.
	     */
	    public static final int ERR_NICKNAME_IS_NOT_UNIQUE = -5;

	    /**
	     * Failed to create/update a profile due to invalid profile grade.
	     */
	    public static final int ERR_GRADE_INVALID = -6;

	    /**
	     * Failed to create/update a profile due to invalid gender.
	     */
	    public static final int ERR_GENDER_INVALID = -7;

	    /**
	     * Failed to create/update a profile due to invalid birth date.
	     */
	    public static final int ERR_BIRTHDATE_INVALID = -8;
    }

    /**
     *    Predefined profile permission names:
     *	  A. Permissions for App Management:
     *       1. "autoAddNewApps": Whether new apps could be added automatically to the specified profile.
     *    B. Permissions for App Center:
     *	     1. "requirePasswordForAppCenter": whether browsing App Center requires a password for the specified profile.
     *	     2. "enableWishListButton": whether wishlist buttons could be enabled for the specified profile or not.
     *	     3. "showPriceAndBuyButton": whether price and buy button could be shown for the specified profile or not.
     *	     4. "showSneakPeekVideos": whether sneak peek videos could be shown on home screen for the specified profile or not.
     *	     5. "showDemoGames": whether demo games could be shown on home screen for the specified profile or not.
     *    C. Permissions for Wireless:
     *	     1. "allowPeerToPeerGaming": whether peer-to-peer gaming is allowed for the specified profile or not.
     */
    public static class ProfilePermission {
        /**
         * Permissions for App Management
         */
		
        /**
         * Prefined profile permission constant of whether new apps could be added automatically to the specified profile.
         */
        public static final String AUTO_ADD_NEW_APPS = "autoAddNewApps";

        /**
         * Prefined profile permission constant of whether browsing App Center requires a password for the specified profile.
         */
        public static final String REQUIRE_PASSWORD_FOR_APP_CENTER = "requirePasswordForAppCenter";

        /**
         * Prefined profile permission constant of whether wishlist buttons could be enabled for the specified profile or not.
         */
        public static final String ENABLE_WISH_LIST_BUTTON = "enableWishListButton";

        /**
         * Prefined profile permission constant of whether price and buy button could be shown for the specified profile or not.
         */
        public static final String SHOW_PRICE_AND_BUY_BUTTON = "showPriceAndBuyButton";

        /**
         * Prefined profile permission constant of whether sneak peek videos could be shown on home screen for the specified profile or not.
         */
        public static final String SHOW_SNEAK_PEAK_VIDEOS = "showSneakPeekVideos";

        /**
         * Prefined profile permission constant of whether demo games could be shown on child home screen for the specified profile or not.
         */
        public static final String SHOW_DEMO_GAMES = "showDemoGames";

        /**
         * Permissions for Wireless:
         */

        /**
         * Prefined profile permission constant of whether peer-to-peer gaming is allowed for the specified profile or not.
         */
        public static final String ALLOW_P2P_GAMING = "allowPeerToPeerGaming";
    } 

    /**
     * Constant value for gender of male=0.
     */
    public static final int MALE = 0;

    /**
     * Constant value for gender of female=1.
     */
    public static final int FEMALE = 1;

    /**
     * Constant value for min-grade supported in this system=0xFE=-2.
     */
    public static final int MIN_GRADE = -2; // 0xFE

    /**
     * Constant value for max-grade supported in this system=6.
     */
    public static final int MAX_GRADE = 6;

    /**
     * Profile ID.
     *      -1: parent's profile ID.
     *       0: invalid (no user).
     *     > 0: child's profile ID; a smaller value indicate created earlier in time.
     */
    private int mProfileId;

    /**
     * Profile nickname, usually it is derived from the profile unique name (the first name).
     */
    private String mNickname;

    /**
     * Child's grade level. Range: MIN_GRADE=-2(0xFE) ~ MAX_GRADE=6(0x06).
     */
    private int mGrade;

    /**
     * Child's gender: MALE=0, FEMALE=1.
     */
    private int mGender;

    /**
     * Child's birth date in YYYY-MM-DD format.
     */
    private String mBirthdate;

    public ProfileInfo() {
    }

    /**
     * Retrieve the profile id assigned/returned by ProfileService.
     * @return the profile id.
     *         -1: parent's profile ID.
     *          0: invalid (no user).
     *        > 0: child's profile ID; a smaller value indicate created earlier in time.
     */
    public int getProfileId() {
        return mProfileId;
    }

    /**
     * Retrieve the profile nickname, usually it is derived from the profile unique name (the first name).
     * @return the profile name.
     */
    public String getNickname() {
        return mNickname;
    }

    /**
     * Set the new profile nickname.
     * @param name the new profile nickname.
     */
    public void setNickname(String nickname) {
        if (nickname != null)
            this.mNickname = nickname;
    }

    /**
     * Get the child's grade level.
     * @return the child's grade level; Range: MIN_GRADE=-2(0xFE) ~ MAX_GRADE=6(0x06).
     */
    public int getGrade() {
        return mGrade;
    }

    /**
     * Set the child's grade level.
     * @param grade the child's grade level.
     *              Range: MIN_GRADE=-2(0xFE) ~ MAX_GRADE=6(0x06).
     */
    public void setGrade(int grade) {
        this.mGrade = grade;
    }

    /**
     * Get the minimum grade supported in this system.
     * @return MIN_GRADE=-2(0xfe).
     */
    public static int getMinGrade() { return MIN_GRADE; }

    /**
     * Get the maximum grade supported in this system.
     * @return MAX_GRADE=6(0x06).
     */
    public static int getMaxGrade() { return MAX_GRADE; }

    /**
     * Retrieve the child's gender.
     * @return the child's gender, MALE=0, FEMALE=1.
     */
    public int getGender() {
        return mGender;
    }

    /**
     * Set the child's gender.
     * @param gender the child's gender: MALE=0, FEMALE=1.
     */
    public void setGender(int gender) {
        this.mGender = gender;
    }

    /**
     * Retrieve child's birth date.
     * @return the birth date in YYYY-MM-DD format.
     */
    public String getBirthdate() {
        return mBirthdate;
    }

    /**
     * Set the child's birth date.
     * @param birthdate the birth date in YYYY-MM-DD format.
     */
    public void setBirthdate(String birthdate)
    {
        if (birthdate != null)
            this.mBirthdate = birthdate;
    }

    /**
     * Parse birth date string in the format of YYYY-MM-DD. This is a convenient birth
     * day utility for web callback.
     * @param birthdate the birth date string in the format of YYYY-MM-DD.
     * @return null on errors.
     *         int[3] for YYYY:[0], MM:[1] and DD[2].
     */
    public static int[] parseBirthdate(String birthdate) {
        if (birthdate == null || birthdate.isEmpty()) return null;

        String[] fields = birthdate.split("-", 3);
        int[] values = new int[3];
        int i = 0;
        for (String f : fields) {
            try {
                values[i] = Integer.parseInt(f, 10);
            } catch (NumberFormatException nfe) {
                return null;
            }
            if (values[i] < 1) return null;
            i++;
        }

        return values;
    }

    public String toString() {
        return new StringBuilder()
            .append("Profile-ID=").append(mProfileId)
            .append("\n\tnickname=").append(mNickname)
            .append("\n\tgender=").append(mGender)
            .append(mGender == MALE ? "(male)" : "(female)")
            .append("\n\tbirthdate=").append(mBirthdate)
            .append("\n\tgrade=").append(mGrade)
            .toString();
    }

    //////////////////////////////////////////////////////////////////////////////////////
    // Parcelable interface.

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel out, int flags) {

        // int: mProfileId, mGrade, and mGender.
        int[] values = {mProfileId, mGrade, mGender};
        for (int value : values)
            out.writeInt(value);

        // String: mName and mBirthdate.
        String[] strings = { mNickname, mBirthdate };
        for (String str : strings) {
            if (str == null || str.isEmpty())
                out.writeInt(0); // Write null flag first.
            else {
                out.writeInt(str.length()); // Write null flag first.
                out.writeString(str);
            }
        }
    }

    public static final Parcelable.Creator<ProfileInfo> CREATOR = 
                        new Parcelable.Creator<ProfileInfo>() {
        public ProfileInfo createFromParcel(Parcel in) {
            return new ProfileInfo(in);
        }

        public ProfileInfo[] newArray(int size) {
            return new ProfileInfo[size];
        }
    };

    public ProfileInfo(int profileId) {
        mProfileId = profileId;
    }

    private ProfileInfo(Parcel in) {
        // int: mProfileId, mGrade, and mGender.
        mProfileId = in.readInt();
        mGrade = in.readInt();
        mGender = in.readInt();

        // String: mName and mBirthdate.
        int len = in.readInt();
        if (len == 0) mNickname = null;
        else mNickname = in.readString();

        len = in.readInt();
        if (len == 0) mBirthdate = null;
        else mBirthdate = in.readString();
    }

}

