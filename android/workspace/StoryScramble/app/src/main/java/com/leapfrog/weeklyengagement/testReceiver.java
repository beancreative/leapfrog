package com.leapfrog.weeklyengagement;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

/**
 * Created by Patrick on 3/20/2015.
 */
public class testReceiver extends BroadcastReceiver{
    @Override
    public void onReceive(Context context, Intent intent) {
        Log.e("intent", "I RECEIVED AN INTENT");

        String message = intent.getStringExtra("com.leapfrog.extra.WEEKLY_ENGAGEMENT_STATE");
        Log.e("receiver", "received a message "+message);
    }
}
