package com.leapfrog.weeklyengagement.tools;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;
import android.widget.Toast;

import com.leapfrog.contentprofile.IProfileService;
import com.leapfrog.contentprofile.ProfileInfo;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * Created by jordanpridgen on 3/19/15.
 */
public class InstanceChecker {

    private static InstanceChecker checkerInstance = null;

    public static final String PREFS_NAME = "UserProfile";
    public static final String ALL_IDS = "AllUserIDs";
    public static final String INSTANCE = "GameInstance";
    public static final String SUB_INSTANCE = "GameSubInstance";
    public static final String WEEK_NUMBER = "weekNumber";
    public static final String TIMESTAMP = "GameTimestamp";
    public static final String NEW_TODAY = "NewToday";
    public static final String NEW_THIS_WEEK = "newThisWeek";
    public static final String THIS_APP = "com.leapfrog.action.WEEKLY_ENGAGEMENT";
    public static final String THIS_APP_STATE = "com.leapfrog.extra.WEEKLY_ENGAGEMENT_STATE";
    private static final String ACTION_SET_ALARM = "com.leapfrog.narnia.timerservice.action.SET_ALARM";
    private static final String EXTRA_PARAM_TYPE = "com.leapfrog.narnia.timerservice.extra.TYPE";
    private static final String EXTRA_PARAM_TRIGGER_AT = "com.leapfrog.narnia.timerservice.extra.TRIGGER_AT";
    private static final String EXTRA_PARAM_PENDING_INTENT = "com.leapfrog.narnia.timerservice.extra.INTENT";
    private static final String EXTRA_PARAM_REQ_ID = "com.leapfrog.narnia.timerservice.extra.REQ_ID";

    public static MediaPlayer MUSIC_PLAYER;
    public static MediaPlayer EFFECTS_PLAYER;
    private static int PLAYER_PROFILE_NO = 0;

    private int _id = 0;
    private static Context con;
    private AlarmManager manager;

    private int _weekFound = -1;

    private static IProfileService pService;

    private static ServiceConnection pConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {

            try {
                Log.e("SERVICE", "service connected");
                pService = IProfileService.Stub.asInterface(service);


                checkerInstance = new InstanceChecker(pService.getActiveProfile());


                updateConnection();
            } catch (RemoteException e){
                Log.e("ERROR",e.getMessage());
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            Log.e("SERVICE ERROR","Service Disconnected");
        }

    };

    private static void updateConnection(){
        try {
            Log.e("ACTIVE PROFILE",Integer.toString(pService.getActiveProfile()));

            int active = pService.getActiveProfile();
            if (active != 0) {

               // Log.e("SERVICE TEST", pService.getProfileName(pService.getActiveProfile()));
                List<ProfileInfo> profiles = pService.getAllProfiles();

                String fullString = "";

                for (int i = 0; i < profiles.size(); i++) {
                    ProfileInfo prof = profiles.get(i);
                    fullString = fullString + prof.getProfileId();

                    if (i != profiles.size() - 1) {
                        fullString = fullString + ",";
                    }
                }

                Log.e("CONTENT","all users"+fullString);

                SharedPreferences allUsers = con.getSharedPreferences(ALL_IDS, 0);
                SharedPreferences.Editor editor = allUsers.edit();
                editor.putString(ALL_IDS, fullString).commit();

                getInstance().callProvider();
            }


        } catch (RemoteException e){
            Log.e("SERVICE ERROR",e.getMessage());
        }
    }

    protected InstanceChecker(int id){
        _id = id;
    }

    public static InstanceChecker getInstance(){
        return checkerInstance;
    }

    public static void create(Context context){
        if (checkerInstance == null || getInstance().outOfSync()) {
            con = context;
            Intent serviceIntent = new Intent();
            serviceIntent.setClassName("com.leapfrog.contentprofile",
                    "com.leapfrog.contentprofile.ProfileService");
            try {
                if (!context.bindService(serviceIntent, pConnection, Context.BIND_AUTO_CREATE)) {
                    StringBuilder sb = new StringBuilder(128).append("bindService(")
                            .append(IProfileService.class.getName())
                            .append(") FAILED!!!\n");
                }
            } catch (SecurityException se) {
                se.printStackTrace();
            }
            Log.e("SERVICE", "bindService called");
        }



    }

    public void updateInstance(){
        //INSTANCE EXAMPLE
        try {
            if (_id != pService.getActiveProfile()) {
                Log.e("PROFILE","profile updated to "+pService.getActiveProfile());
                _id = pService.getActiveProfile();
            }
        }catch (RemoteException e){

        }

    }

    public void artificialDay(Context c){

        int inst = getInstance(c);
        int subinst = getSubInstance(c);

        subinst++;
        setNewToday(true, c);

        if(subinst >= 7){
            inst++;
            subinst = 0;
        }

        if (inst > 3)
            inst = 1;

        setInstance(inst, c);
        setSubInstance(subinst, c);
    }

    public void artificialWeek(){

        int week = getWeekNumber();
        week++;
        if(week >= 26) {
            week = 0;
        }
        _weekFound = week;
        setWeekNumber(week);
    }

    public int getInstance(Context c){

        SharedPreferences settings = c.getSharedPreferences(PREFS_NAME + checkerInstance._id, 0);

        return settings.getInt(INSTANCE, -1);

    }

    public int getSubInstance(Context c){

        SharedPreferences settings = c.getSharedPreferences(PREFS_NAME+checkerInstance._id, 0);
        return settings.getInt(SUB_INSTANCE, -1);

    }

    public void setSubInstance(int instance,Context c){
        SharedPreferences settings = c.getSharedPreferences(PREFS_NAME+checkerInstance._id, 0);
        settings.edit().putInt(SUB_INSTANCE, instance).commit();

    }

    public long getTimestamp(Context c){

        SharedPreferences settings = c.getSharedPreferences(PREFS_NAME+checkerInstance._id, 0);
        return settings.getLong(TIMESTAMP, -1);

    }

    public void setInstance(int instance, Context c){
        SharedPreferences settings = c.getSharedPreferences(PREFS_NAME+checkerInstance._id, 0);
        settings.edit().putInt(INSTANCE, instance).commit();

    }

    public  void setTimestamp(long time, Context c){
        SharedPreferences settings = c.getSharedPreferences(PREFS_NAME+checkerInstance._id, 0);
        settings.edit().putLong(TIMESTAMP, time).commit();

    }

    public void setNewToday(boolean isNew, Context c){

        Log.e("STATE","Changing state to:"+isNew+" for user:"+_id);

        SharedPreferences settings = c.getSharedPreferences(PREFS_NAME+checkerInstance._id, 0);
        settings.edit().putBoolean(NEW_TODAY, isNew).commit();


        if (!isNew) {

            Intent intent = new Intent(THIS_APP);
            intent.putExtra(THIS_APP_STATE, isNew);
            intent.putExtra("com.leapfrog.extra.profileId",_id);
            c.sendBroadcast(intent);

            PendingIntent pending;
            Intent laterIntent = new Intent(con,AlarmReceiver.class);
            laterIntent.setAction("com.leapfrog.profileIntent" + _id);
            laterIntent.putExtra(THIS_APP_STATE, true);
            laterIntent.putExtra("com.leapfrog.extra.profileId", _id);

            Log.e("INTENT", "Unique action: com.leapfrog.profileIntent"+_id);

            pending = PendingIntent.getBroadcast(con,0,laterIntent,0);

            fireNextWeek(pending,_id,con);

        }

        callProvider();


    }

    public void setWeekNumber(int week){
        SharedPreferences settings = con.getSharedPreferences(PREFS_NAME+checkerInstance._id, 0);
        settings.edit().putInt(WEEK_NUMBER, week).commit();
    }

    public void setNewWeek(boolean isNew){

        Log.e("STATE","Changing state to:"+isNew+" for user:"+_id);

        SharedPreferences settings = con.getSharedPreferences(PREFS_NAME+checkerInstance._id, 0);
        settings.edit().putBoolean(NEW_THIS_WEEK, isNew).commit();


        if (!isNew) {

            Intent intent = new Intent(THIS_APP);
            intent.putExtra(THIS_APP_STATE, isNew);
            intent.putExtra("com.leapfrog.extra.profileId",_id);
            con.sendBroadcast(intent);
            Log.e("INTENT", "FALSE fired on " + con);

            PendingIntent pending;
            Intent laterIntent = new Intent(con,AlarmReceiver.class);
            laterIntent.setAction("com.leapfrog.profileIntent" + _id);
            laterIntent.putExtra(THIS_APP_STATE, true);
            laterIntent.putExtra("com.leapfrog.extra.profileId",_id);

            pending = PendingIntent.getBroadcast(con,0,laterIntent,0);

            Log.e("INTENT SETUP", "THIS_APP:" + THIS_APP + " THIS_APP_STATE:" + THIS_APP_STATE + ",true for ID:" + _id);

            fireNextWeek(pending,_id,con);

        }

        callProvider();
    }


    public int getWeekNumber(){

        SharedPreferences settings = con.getSharedPreferences(PREFS_NAME+checkerInstance._id, 0);
        int week = settings.getInt(WEEK_NUMBER, -1);

        if(week == -1){
            week = 0;


            setNewWeek(true);
            setWeekNumber(week);

            setTimestamp(System.currentTimeMillis(),con);

        }



        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(getTimestamp(con));
        if (cal.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY && cal.get(Calendar.HOUR_OF_DAY) >= 3)
            cal.add(Calendar.DATE,7);
        cal.set(Calendar.DAY_OF_WEEK, Calendar.SATURDAY);
        cal.set(Calendar.HOUR, 3);
        cal.set(Calendar.HOUR_OF_DAY, 3);
        cal.set(Calendar.AM_PM, Calendar.AM);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);



        Calendar now = Calendar.getInstance();
        Log.e("CAL","current epoch time: "+now.getTimeInMillis());

        if(now.getTimeInMillis() >= cal.getTimeInMillis()){


            setNewWeek(true);

            week++;

            if (week >= 26)
                week = 0;

            setWeekNumber(week);
            setTimestamp(System.currentTimeMillis(),con);
        }

        _weekFound = week;

        return week;
    }



    private static void fireNextWeek(PendingIntent pending,int id, Context c){

        //if (manager != null) {
           // manager.cancel(pending);
        //}
        //else{

            Calendar nextCal = Calendar.getInstance();
            Log.e("CAL1", "current epoch time: " + nextCal.getTimeInMillis() + " current day of week:" + nextCal.get(Calendar.DAY_OF_WEEK));
            if (nextCal.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY && nextCal.get(Calendar.HOUR_OF_DAY) >= 3) {
                Log.e("CAL2","current epoch time: "+nextCal.getTimeInMillis() + " current day of week:" + nextCal.get(Calendar.DAY_OF_WEEK));
                Log.e("INTERVAL","interval thinks it's SATURDAY " + nextCal.get(Calendar.DAY_OF_WEEK) + "/" + Calendar.SATURDAY);
                nextCal.add(Calendar.DATE, 7);
            }
            nextCal.set(Calendar.DAY_OF_WEEK, Calendar.SATURDAY);
            nextCal.set(Calendar.HOUR, 3);
            nextCal.set(Calendar.HOUR_OF_DAY, 3);
            nextCal.set(Calendar.AM_PM, Calendar.AM);
            nextCal.set(Calendar.MINUTE, 0);
            nextCal.set(Calendar.SECOND, id);
            nextCal.set(Calendar.MILLISECOND, 0);

            long interval = nextCal.getTimeInMillis();


            Date date = new Date(interval);
            SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
            String dateFormatted = formatter.format(date);

            Log.e("NEXT","New story to appear: " + dateFormatted);

            Intent setTimerIntent = new Intent();
            setTimerIntent.setComponent(new ComponentName("com.leapfrog.narnia.timerservice", "com.leapfrog.narnia.timerservice.TimerService"));
            setTimerIntent.setAction(ACTION_SET_ALARM);
            setTimerIntent.putExtra(EXTRA_PARAM_TYPE,  AlarmManager.RTC);
            setTimerIntent.putExtra(EXTRA_PARAM_TRIGGER_AT, interval);
            setTimerIntent.putExtra(EXTRA_PARAM_PENDING_INTENT, pending);
            setTimerIntent.putExtra(EXTRA_PARAM_REQ_ID, id);
            c.startService(setTimerIntent);

            //manager.set(AlarmManager.RTC, interval, pending);
            //Toast.makeText(con, "Alarm Set", Toast.LENGTH_SHORT).show();
        //}

    }

    public boolean getNewToday(Context c){

        SharedPreferences settings = c.getSharedPreferences(PREFS_NAME+checkerInstance._id, 0);
        return settings.getBoolean(NEW_TODAY,true);

    }

    public boolean getNewWeek(Context c){

        SharedPreferences settings = c.getSharedPreferences(PREFS_NAME+checkerInstance._id, 0);
        return settings.getBoolean(NEW_THIS_WEEK,true);

    }


    public void addTokens(int tokens){
        try {
            pService.addProfileTokens(_id, tokens);
            Log.e("REWARDS","tokens added: "+tokens);
        } catch (RemoteException e){
            Log.e("REWARDS ERROR",e.getMessage());
        }
    }

    public int getCurrentID(){
        return _id;
    }

    public List<ProfileInfo> getNumProfiles(){
        try {
            return pService.getAllProfiles();
        } catch (RemoteException e){
            Log.e("ERROR",e.getMessage());
            return null;
        }
    }

    public boolean outOfSync(){
        try {

            if (_id != pService.getActiveProfile()){
                return true;
            }
        } catch (RemoteException e){
            Log.e("ERROR",e.getMessage());

        }

        SharedPreferences settings = con.getSharedPreferences(PREFS_NAME+checkerInstance._id, 0);
        int week = _weekFound;

        if(week == -1){
            return false;
        }

        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(getTimestamp(con));
        if (cal.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY && cal.get(Calendar.HOUR_OF_DAY) >= 3)
            cal.add(Calendar.DATE,7);
        cal.set(Calendar.DAY_OF_WEEK, Calendar.SATURDAY);
        cal.set(Calendar.HOUR, 3);
        cal.set(Calendar.HOUR_OF_DAY, 3);
        cal.set(Calendar.AM_PM, Calendar.AM);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);



        Calendar now = Calendar.getInstance();
        Log.e("CAL","current epoch time: "+now.getTimeInMillis());

        if(now.getTimeInMillis() >= cal.getTimeInMillis()){
            return true;
        }

        return false;
    }
    public IProfileService getService(){
        return pService;
    }

    public void callProvider(){
        ContentResolver resolver = con.getContentResolver();
        String[] projection = new String[]{"ID", "STATE"};
        Cursor cursor = resolver.query(Uri.parse("content://com.leapfrog.weeklyengagement.provider/engagement"),
                projection,
                null,
                null,
                null);
        if(cursor.moveToFirst()){
            do {
                int profileID = cursor.getInt(0);
                boolean currentState = (cursor.getInt(1)==0)?false:true;

                Log.e("CONTENT","Profile:"+profileID+" state:"+currentState);

            } while (cursor.moveToNext());
        }
    }

    public static boolean isReady(){
        if (checkerInstance != null)
            return true;
        return false;
    }

    public boolean isUK(){
        String isLocaleUK = (String) "en_US";
        try {
            isLocaleUK = pService.getSystemLocale();
        } catch (RemoteException e) {
            Log.e("ERROR",e.getMessage());
            isLocaleUK = "en_US";
            return false;
        }
        if (isLocaleUK != null && !isLocaleUK.isEmpty()) {
            //proceed as is
        } else {
            isLocaleUK = "en_US";
        }
        Log.e("CONTENT","Locale:"+isLocaleUK);
        if(!isLocaleUK.equals("en_US")) {
            return true;
        } else {
            return false;
        }
    }

    public static void setSpecificNewWeek(int profileID,Context c){
        Log.e("SETTING NEW2","setting profile "+profileID+" to new");

        SharedPreferences settings = c.getSharedPreferences(PREFS_NAME+profileID, 0);
        settings.edit().putBoolean(NEW_THIS_WEEK, true).commit();
    }

    public static void updateIfNeeded(int id, Context c){


        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(getSpecificTimestamp(c,id));
        if (cal.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY && cal.get(Calendar.HOUR_OF_DAY) >= 3)
            cal.add(Calendar.DATE,7);
        cal.set(Calendar.DAY_OF_WEEK, Calendar.SATURDAY);
        cal.set(Calendar.HOUR, 3);
        cal.set(Calendar.HOUR_OF_DAY, 3);
        cal.set(Calendar.AM_PM, Calendar.AM);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);



        Calendar now = Calendar.getInstance();
        Log.e("CAL","current epoch time: "+now.getTimeInMillis());

        if(now.getTimeInMillis() >= cal.getTimeInMillis()){


            fireSpecificIntent(c,id);
            setSpecificNewWeek(id,c);

        }
        else{

            PendingIntent pending;
            Intent laterIntent = new Intent(c,AlarmReceiver.class);
            laterIntent.setAction("com.leapfrog.profileIntent" + id);
            laterIntent.putExtra(THIS_APP_STATE, true);
            laterIntent.putExtra("com.leapfrog.extra.profileId",id);

            pending = PendingIntent.getBroadcast(c,0,laterIntent,0);

            fireNextWeek(pending,id,c);
        }
    }

    public static long getSpecificTimestamp(Context c,int id){

        SharedPreferences settings = c.getSharedPreferences(PREFS_NAME+id, 0);
        return settings.getLong(TIMESTAMP, -1);

    }

    public static void fireSpecificIntent(Context c, int id){

            Intent intent = new Intent(THIS_APP);
            intent.putExtra(THIS_APP_STATE, true);
            intent.putExtra("com.leapfrog.extra.profileId",id);
            c.sendBroadcast(intent);
            Log.e("INTENT", "FALSE fired on " + c);

    }


}


