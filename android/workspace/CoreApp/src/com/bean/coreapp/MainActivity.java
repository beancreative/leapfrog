package com.bean.coreapp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Calendar;
import java.util.Date;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.TextView;


public class MainActivity extends Activity {
	
	public static final String PREFS_NAME = "UserProfile";
	public static final String INSTANCE = "GameInstance";
	public static final String SUB_INSTANCE = "GameSubInstance";
	public static final String TIMESTAMP = "GameTimestamp";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        
        setContentView(R.layout.activity_main);
        
        
        
        
        String str = readFile("json/gamedata/0001_0002.json");
        
        try {
			JSONObject ob = new JSONObject(str);
			
			Log.e("WORD", ob.getJSONObject("data").getString("word"));
			
		} catch (JSONException e) {
			e.printStackTrace();
		}
        
//        getCurrentGame();
        
//        MediaPlayer myPlayer = new MediaPlayer();
//        
//		   
//		try {
//			myPlayer.setDataSource(this, getAudio("al_yeehaw"));
//			myPlayer.prepare();
//			myPlayer.start();
//		} catch (IllegalStateException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
        
        contentProviderTest();

    }
    
    private void contentProviderTest(){
    	
    	ContentResolver resolver = getContentResolver();
    	String[] projection = new String[]{"STATE"};
    	Cursor cursor = resolver.query(Uri.parse("content://com.leapfrog.dailyengagement.provider/engagement"),
    	        projection,
    	        null,
    	        null,
    	        null);
    	if(cursor.moveToFirst()){
    	        boolean currentState = (cursor.getInt(0)==0)?false:true;
    	        
    	        if(currentState)
    	        	Log.e("CORE", "DID IT!!!!!!!!!");
    	}
    }
    
    private void instanceExample(){
    	//INSTANCE EXAMPLE
        int inst = getInstance();
        int subinst = getSubInstance();
        
        if(inst == -1 || subinst == -1){
        	inst = 1;
        	subinst = 0;
        	
        	setInstance(inst);
        	setSubInstance(subinst);
        	
        	setTimestamp(System.currentTimeMillis());
       
        }
        
        
        
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(getTimestamp());
        cal.add(Calendar.DATE, 1);
        cal.set(Calendar.HOUR, 0);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.AM_PM, Calendar.AM);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        
        
        
        Calendar now = Calendar.getInstance();
        
        if(now.getTimeInMillis() >= cal.getTimeInMillis()){
        	
        	subinst++;
        	
        	if(subinst >= 7){
        		inst++;
        		subinst = 0;
        	}
        	
        	setInstance(inst);
        	setSubInstance(subinst);
        	
        	setTimestamp(System.currentTimeMillis());
        }
        
        int word = (inst-1)*7 + subinst+1;
        
        LinearLayout lay = new LinearLayout(this);
        lay.setOrientation(LinearLayout.VERTICAL);
        
        TextView instText = new TextView(this);
        TextView subInstText = new TextView(this);
        TextView wordText = new TextView(this);
        
        instText.setText("INSTANCE: " + inst);
        subInstText.setText("SUB INSTANCE: " + subinst);
        wordText.setText("Word Num: " + word);
        
        lay.addView(instText);
        lay.addView(subInstText);
        lay.addView(wordText);
        
        setContentView(lay);
    }
    
    @Override
    protected void onResume(){
    	super.onResume();
    	
    	instanceExample();
    }
    
    
    public String readFile(String name){
    	
    	
    	InputStream is;
    	String line = null;
    	String full = "";
		try {
			is = getAssets().open(name);
		
	    	BufferedReader br = new BufferedReader(new InputStreamReader(is));
	    	
	    	while ((line = br.readLine()) != null) {
//	    	    Log.e("wtf", line);
	    	    full += line;
	    	}
	    	br.close();
    	
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return full;
    	
    }
    
    public JSONObject getCurrentGame(){
    
    	
    	String[] files;
    	String match = getInstance() + "_";
    	String fname = null;
    	
    	setTimestamp(System.currentTimeMillis());
    	
		try {
			files = getAssets().list("json/gamedata");
			
			for(int i = 0; i < files.length; i++){
				if(files[i].contains(match)){
					fname = files[i];
					break;
				}
			}
			
			if(fname == null){
				fname = files[0];
				
				int inst = Integer.parseInt(fname.substring(0, 4));
				setInstance(inst);
				Log.d("CORE", "New Instance: " + inst);
			}
			
			return new JSONObject(readFile("json/gamedata/"+fname));
			
			
	    		
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		}
    	
		//failsafe
    	
    	return loadDefaultGame();
    	
    }
    
    private final String defaultGame = "{}";
    
    public JSONObject loadDefaultGame(){
    	try {
			return new JSONObject(defaultGame);
		} catch (JSONException e) {
			e.printStackTrace();
		}
    	
    	return new JSONObject();
    }
    
    private String getProfileLoc(){
    	return "json/profile.json";
    }
    
    public JSONObject profileObject(){
    	
    	String proff = readFile(getProfileLoc());
    	
    	
    	 try {
 			return new JSONObject(proff);
 			
 		} catch (JSONException e) {
 			e.printStackTrace();
 		}
    	 
    	 return null;
    	
    }
     
    
    public String getUsersName(){
    	try {
			return profileObject().getString("Name");
		} catch (JSONException e) {
			e.printStackTrace();
		}
    	
    	return null;
    }
    
    public int getInstance(){

    	 SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
         return settings.getInt(INSTANCE, -1);
    
    }
    
    public int getSubInstance(){

   	 SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
        return settings.getInt(SUB_INSTANCE, -1);
   
   }
    
    public void setSubInstance(int instance){
    	SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
        settings.edit().putInt(SUB_INSTANCE, instance).commit();
       
    }
    
    public long getTimestamp(){
    	
    	SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
        return settings.getLong(TIMESTAMP, -1);
        
    }
    
    public void setInstance(int instance){
    	SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
        settings.edit().putInt(INSTANCE, instance).commit();
       
    }
    
    public void setTimestamp(long time){
    	SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
        settings.edit().putLong(TIMESTAMP, time).commit();
       
    }
    
    public Drawable getImage(String name){
    	
    	int id = getResources().getIdentifier("drawable/ic_launcher", null, getPackageName());
    	if(id != 0)
    		return this.getResources().getDrawable(id);
    	return null;
    }
    
    public Uri getAudio(String name){

    	int id = getResources().getIdentifier(name, "raw", getPackageName());
    	return Uri.parse("android.resource://"+getPackageName()+"/" + id);
    }

}
